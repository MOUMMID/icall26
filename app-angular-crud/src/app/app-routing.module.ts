import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//import { ProductsComponent } from './products/products.component';
//import { ProductDetailComponent } from './products-module/product-detail/product-detail.component';
//import { ProductAddComponent } from './products-module/product-add/product-add.component';
//import { ProductEditComponent } from './products-module/product-edit/product-edit.component';
import { LoginComponent } from './login/login.component';
import { UsersComponent } from './users/users.component';
import { ProductsListComponent } from './products-module/products-list/products-list.component';


const routes: Routes = [

  {
    path: 'login',
    component: LoginComponent,
    data: { title: 'Login' }
  },
  {
    path: 'users',
    component: UsersComponent,
    data: { title: 'Login' }
  },
  /*{
    path: 'product-edit/:id/:message',
    component: ProductEditComponent,
    data: { title: 'Edit Product' }
  },*/
  { path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
