//Adding this token manually in the header for all the API requests is not a cleaner way yo do. Hence, we will be implementing a HTTPInterceptor that will intercept all the rquest and add this JWT authorization token in the header. Also, we can intercept the response and for any unauthorized request or expired token we can redirect user to login page.Also, to store this token locally, we can use sessionstorage - The sessionStorage object stores data for only one session (the data is deleted when the browser tab is closed). The interceptor will implement the HttpInterceptor interface and override intercept(). Here, we are cloning the request to set the headers we want.Following is the interceptor implementation.
import { Injectable } from '@angular/core';
import {HttpInterceptor, HttpRequest, HttpHandler, HttpSentEvent, HttpHeaderResponse, HttpProgressEvent,
  HttpResponse, HttpUserEvent, HttpErrorResponse} from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import {TokenStorageService} from './token-storage.service';
//import 'rxjs/add/operator/do';

const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class Interceptor implements HttpInterceptor {

  constructor(private token: TokenStorageService, private router: Router) { }

   intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any>   {
    let authReq = req;
    if (this.token.getToken() != null) {
      authReq = req.clone({ headers: req.headers.set(TOKEN_HEADER_KEY, 'zine ' + this.token.getToken())});
    }
    return next.handle(authReq).pipe(
      tap(  (err: any) => {
          if (err instanceof HttpErrorResponse) {

            if (err.status === 401) {
              this.router.navigate(['login']);
            }
          }
        })
      );
   }

}
