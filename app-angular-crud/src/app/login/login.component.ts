import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
//import {MatDialog} from '@angular/material';
import { AuthService } from '../auth.service';
import { TokenStorageService } from '../token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  constructor(private router: Router, private authService: AuthService, private token: TokenStorageService) {
  }

  username: string;
  password: string;

  login(): void {
  /*  if(this.username == 'admin' && this.password == 'admin'){
    this.router.navigate(["products"]);
   }else {
     alert("Invalid credentials");
   }*/
   this.authService.attemptAuth(this.username, this.password).subscribe(
      data => {
        this.token.saveToken(data.token);
        this.router.navigate(['products']);
      }
    );
  }

}
