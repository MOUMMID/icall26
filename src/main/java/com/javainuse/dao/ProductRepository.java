package com.javainuse.dao;


import org.springframework.data.jpa.repository.JpaRepository;

import com.javainuse.entities.Product;

public interface ProductRepository extends JpaRepository<Product, Integer>{

}
