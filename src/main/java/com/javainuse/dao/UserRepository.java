package com.javainuse.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.javainuse.entities.User;

public interface UserRepository extends JpaRepository<User, Long>{
	public User findByUsername(String username);

}
