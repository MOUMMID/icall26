package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the t_customers_address_localization_step database table.
 * 
 */
@Entity
@Table(name="t_customers_address_localization_step")
@NamedQuery(name="TCustomersAddressLocalizationStep.findAll", query="SELECT t FROM TCustomersAddressLocalizationStep t")
public class TCustomersAddressLocalizationStep implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="end_address_id")
	private int endAddressId;

	@Column(name="end_signature")
	private String endSignature;

	@Column(name="estimated_distance")
	private BigDecimal estimatedDistance;

	@Column(name="estimated_time")
	private int estimatedTime;

	@Column(name="start_address_id")
	private int startAddressId;

	@Column(name="start_signature")
	private String startSignature;

	public TCustomersAddressLocalizationStep() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEndAddressId() {
		return this.endAddressId;
	}

	public void setEndAddressId(int endAddressId) {
		this.endAddressId = endAddressId;
	}

	public String getEndSignature() {
		return this.endSignature;
	}

	public void setEndSignature(String endSignature) {
		this.endSignature = endSignature;
	}

	public BigDecimal getEstimatedDistance() {
		return this.estimatedDistance;
	}

	public void setEstimatedDistance(BigDecimal estimatedDistance) {
		this.estimatedDistance = estimatedDistance;
	}

	public int getEstimatedTime() {
		return this.estimatedTime;
	}

	public void setEstimatedTime(int estimatedTime) {
		this.estimatedTime = estimatedTime;
	}

	public int getStartAddressId() {
		return this.startAddressId;
	}

	public void setStartAddressId(int startAddressId) {
		this.startAddressId = startAddressId;
	}

	public String getStartSignature() {
		return this.startSignature;
	}

	public void setStartSignature(String startSignature) {
		this.startSignature = startSignature;
	}

}