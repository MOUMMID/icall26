package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_address_postcode_localization database table.
 * 
 */
@Entity
@Table(name="t_customers_address_postcode_localization")
@NamedQuery(name="TCustomersAddressPostcodeLocalization.findAll", query="SELECT t FROM TCustomersAddressPostcodeLocalization t")
public class TCustomersAddressPostcodeLocalization implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String city;

	@Column(name="created_at")
	private Timestamp createdAt;

	private BigDecimal lat;

	private BigDecimal lng;

	private String postcode;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TCustomersAddressPostcodeLocalization() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public BigDecimal getLat() {
		return this.lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return this.lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}

	public String getPostcode() {
		return this.postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}