package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_address_postcode_localization_step database table.
 * 
 */
@Entity
@Table(name="t_customers_address_postcode_localization_step")
@NamedQuery(name="TCustomersAddressPostcodeLocalizationStep.findAll", query="SELECT t FROM TCustomersAddressPostcodeLocalizationStep t")
public class TCustomersAddressPostcodeLocalizationStep implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="end_id")
	private int endId;

	@Column(name="end_postcode")
	private String endPostcode;

	@Column(name="estimated_distance")
	private BigDecimal estimatedDistance;

	@Column(name="estimated_time")
	private int estimatedTime;

	@Column(name="start_id")
	private int startId;

	@Column(name="start_postcode")
	private String startPostcode;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TCustomersAddressPostcodeLocalizationStep() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getEndId() {
		return this.endId;
	}

	public void setEndId(int endId) {
		this.endId = endId;
	}

	public String getEndPostcode() {
		return this.endPostcode;
	}

	public void setEndPostcode(String endPostcode) {
		this.endPostcode = endPostcode;
	}

	public BigDecimal getEstimatedDistance() {
		return this.estimatedDistance;
	}

	public void setEstimatedDistance(BigDecimal estimatedDistance) {
		this.estimatedDistance = estimatedDistance;
	}

	public int getEstimatedTime() {
		return this.estimatedTime;
	}

	public void setEstimatedTime(int estimatedTime) {
		this.estimatedTime = estimatedTime;
	}

	public int getStartId() {
		return this.startId;
	}

	public void setStartId(int startId) {
		this.startId = startId;
	}

	public String getStartPostcode() {
		return this.startPostcode;
	}

	public void setStartPostcode(String startPostcode) {
		this.startPostcode = startPostcode;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}