package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_contract database table.
 * 
 */
@Entity
@Table(name="t_customers_contract")
@NamedQuery(name="TCustomersContract.findAll", query="SELECT t FROM TCustomersContract t")
public class TCustomersContract implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="admin_status_id")
	private int adminStatusId;

	@Column(name="advance_payment")
	private BigDecimal advancePayment;

	@Temporal(TemporalType.DATE)
	@Column(name="apf_at")
	private Date apfAt;

	@Column(name="assistant_id")
	private int assistantId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="billing_at")
	private Date billingAt;

	@Column(name="callcenter_id")
	private int callcenterId;

	@Temporal(TemporalType.DATE)
	@Column(name="closed_at")
	private Date closedAt;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="created_by_id")
	private int createdById;

	@Column(name="customer_id")
	private int customerId;

	@Column(name="dates_is_opened")
	private String datesIsOpened;

	@Column(name="dates_is_valid")
	private String datesIsValid;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dates_opened_at")
	private Date datesOpenedAt;

	@Column(name="dates_opened_at_by")
	private int datesOpenedAtBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="doc_at")
	private Date docAt;

	@Column(name="financial_partner_id")
	private int financialPartnerId;

	@Column(name="has_tva")
	private String hasTva;

	@Column(name="install_state_id")
	private int installStateId;

	@Column(name="is_billable")
	private String isBillable;

	@Column(name="is_confirmed")
	private String isConfirmed;

	@Column(name="is_document")
	private String isDocument;

	@Column(name="is_hold")
	private String isHold;

	@Column(name="is_hold_admin")
	private String isHoldAdmin;

	@Column(name="is_hold_quote")
	private String isHoldQuote;

	@Column(name="is_photo")
	private String isPhoto;

	@Column(name="is_quality")
	private String isQuality;

	@Column(name="is_revivable")
	private String isRevivable;

	@Column(name="is_signed")
	private String isSigned;

	@Column(name="manager_id")
	private int managerId;

	@Column(name="meeting_id")
	private int meetingId;

	private BigDecimal mensuality;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="opc_at")
	private Date opcAt;

	@Column(name="opc_range_id")
	private int opcRangeId;

	@Column(name="opc_status_id")
	private int opcStatusId;

	@Temporal(TemporalType.DATE)
	@Column(name="opened_at")
	private Date openedAt;

	@Column(name="opened_at_range_id")
	private int openedAtRangeId;

	@Column(name="partner_layer_id")
	private int partnerLayerId;

	@Temporal(TemporalType.DATE)
	@Column(name="payment_at")
	private Date paymentAt;

	@Column(name="polluter_id")
	private int polluterId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="pre_meeting_at")
	private Date preMeetingAt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="quoted_at")
	private Date quotedAt;

	private String reference;

	@Lob
	private String remarks;

	@Column(name="sale_1_id")
	private int sale1Id;

	@Column(name="sale_2_id")
	private int sale2Id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="sav_at")
	private Date savAt;

	@Column(name="sav_at_range_id")
	private int savAtRangeId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="sent_at")
	private Date sentAt;

	@Column(name="state_id")
	private int stateId;

	private String status;

	@Column(name="tax_id")
	private int taxId;

	@Column(name="team_id")
	private int teamId;

	@Column(name="telepro_id")
	private int teleproId;

	@Column(name="time_state_id")
	private int timeStateId;

	@Column(name="total_price_with_taxe")
	private BigDecimal totalPriceWithTaxe;

	@Column(name="total_price_without_taxe")
	private BigDecimal totalPriceWithoutTaxe;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	@Lob
	private String variables;

	public TCustomersContract() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAdminStatusId() {
		return this.adminStatusId;
	}

	public void setAdminStatusId(int adminStatusId) {
		this.adminStatusId = adminStatusId;
	}

	public BigDecimal getAdvancePayment() {
		return this.advancePayment;
	}

	public void setAdvancePayment(BigDecimal advancePayment) {
		this.advancePayment = advancePayment;
	}

	public Date getApfAt() {
		return this.apfAt;
	}

	public void setApfAt(Date apfAt) {
		this.apfAt = apfAt;
	}

	public int getAssistantId() {
		return this.assistantId;
	}

	public void setAssistantId(int assistantId) {
		this.assistantId = assistantId;
	}

	public Date getBillingAt() {
		return this.billingAt;
	}

	public void setBillingAt(Date billingAt) {
		this.billingAt = billingAt;
	}

	public int getCallcenterId() {
		return this.callcenterId;
	}

	public void setCallcenterId(int callcenterId) {
		this.callcenterId = callcenterId;
	}

	public Date getClosedAt() {
		return this.closedAt;
	}

	public void setClosedAt(Date closedAt) {
		this.closedAt = closedAt;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getCreatedById() {
		return this.createdById;
	}

	public void setCreatedById(int createdById) {
		this.createdById = createdById;
	}

	public int getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getDatesIsOpened() {
		return this.datesIsOpened;
	}

	public void setDatesIsOpened(String datesIsOpened) {
		this.datesIsOpened = datesIsOpened;
	}

	public String getDatesIsValid() {
		return this.datesIsValid;
	}

	public void setDatesIsValid(String datesIsValid) {
		this.datesIsValid = datesIsValid;
	}

	public Date getDatesOpenedAt() {
		return this.datesOpenedAt;
	}

	public void setDatesOpenedAt(Date datesOpenedAt) {
		this.datesOpenedAt = datesOpenedAt;
	}

	public int getDatesOpenedAtBy() {
		return this.datesOpenedAtBy;
	}

	public void setDatesOpenedAtBy(int datesOpenedAtBy) {
		this.datesOpenedAtBy = datesOpenedAtBy;
	}

	public Date getDocAt() {
		return this.docAt;
	}

	public void setDocAt(Date docAt) {
		this.docAt = docAt;
	}

	public int getFinancialPartnerId() {
		return this.financialPartnerId;
	}

	public void setFinancialPartnerId(int financialPartnerId) {
		this.financialPartnerId = financialPartnerId;
	}

	public String getHasTva() {
		return this.hasTva;
	}

	public void setHasTva(String hasTva) {
		this.hasTva = hasTva;
	}

	public int getInstallStateId() {
		return this.installStateId;
	}

	public void setInstallStateId(int installStateId) {
		this.installStateId = installStateId;
	}

	public String getIsBillable() {
		return this.isBillable;
	}

	public void setIsBillable(String isBillable) {
		this.isBillable = isBillable;
	}

	public String getIsConfirmed() {
		return this.isConfirmed;
	}

	public void setIsConfirmed(String isConfirmed) {
		this.isConfirmed = isConfirmed;
	}

	public String getIsDocument() {
		return this.isDocument;
	}

	public void setIsDocument(String isDocument) {
		this.isDocument = isDocument;
	}

	public String getIsHold() {
		return this.isHold;
	}

	public void setIsHold(String isHold) {
		this.isHold = isHold;
	}

	public String getIsHoldAdmin() {
		return this.isHoldAdmin;
	}

	public void setIsHoldAdmin(String isHoldAdmin) {
		this.isHoldAdmin = isHoldAdmin;
	}

	public String getIsHoldQuote() {
		return this.isHoldQuote;
	}

	public void setIsHoldQuote(String isHoldQuote) {
		this.isHoldQuote = isHoldQuote;
	}

	public String getIsPhoto() {
		return this.isPhoto;
	}

	public void setIsPhoto(String isPhoto) {
		this.isPhoto = isPhoto;
	}

	public String getIsQuality() {
		return this.isQuality;
	}

	public void setIsQuality(String isQuality) {
		this.isQuality = isQuality;
	}

	public String getIsRevivable() {
		return this.isRevivable;
	}

	public void setIsRevivable(String isRevivable) {
		this.isRevivable = isRevivable;
	}

	public String getIsSigned() {
		return this.isSigned;
	}

	public void setIsSigned(String isSigned) {
		this.isSigned = isSigned;
	}

	public int getManagerId() {
		return this.managerId;
	}

	public void setManagerId(int managerId) {
		this.managerId = managerId;
	}

	public int getMeetingId() {
		return this.meetingId;
	}

	public void setMeetingId(int meetingId) {
		this.meetingId = meetingId;
	}

	public BigDecimal getMensuality() {
		return this.mensuality;
	}

	public void setMensuality(BigDecimal mensuality) {
		this.mensuality = mensuality;
	}

	public Date getOpcAt() {
		return this.opcAt;
	}

	public void setOpcAt(Date opcAt) {
		this.opcAt = opcAt;
	}

	public int getOpcRangeId() {
		return this.opcRangeId;
	}

	public void setOpcRangeId(int opcRangeId) {
		this.opcRangeId = opcRangeId;
	}

	public int getOpcStatusId() {
		return this.opcStatusId;
	}

	public void setOpcStatusId(int opcStatusId) {
		this.opcStatusId = opcStatusId;
	}

	public Date getOpenedAt() {
		return this.openedAt;
	}

	public void setOpenedAt(Date openedAt) {
		this.openedAt = openedAt;
	}

	public int getOpenedAtRangeId() {
		return this.openedAtRangeId;
	}

	public void setOpenedAtRangeId(int openedAtRangeId) {
		this.openedAtRangeId = openedAtRangeId;
	}

	public int getPartnerLayerId() {
		return this.partnerLayerId;
	}

	public void setPartnerLayerId(int partnerLayerId) {
		this.partnerLayerId = partnerLayerId;
	}

	public Date getPaymentAt() {
		return this.paymentAt;
	}

	public void setPaymentAt(Date paymentAt) {
		this.paymentAt = paymentAt;
	}

	public int getPolluterId() {
		return this.polluterId;
	}

	public void setPolluterId(int polluterId) {
		this.polluterId = polluterId;
	}

	public Date getPreMeetingAt() {
		return this.preMeetingAt;
	}

	public void setPreMeetingAt(Date preMeetingAt) {
		this.preMeetingAt = preMeetingAt;
	}

	public Date getQuotedAt() {
		return this.quotedAt;
	}

	public void setQuotedAt(Date quotedAt) {
		this.quotedAt = quotedAt;
	}

	public String getReference() {
		return this.reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public int getSale1Id() {
		return this.sale1Id;
	}

	public void setSale1Id(int sale1Id) {
		this.sale1Id = sale1Id;
	}

	public int getSale2Id() {
		return this.sale2Id;
	}

	public void setSale2Id(int sale2Id) {
		this.sale2Id = sale2Id;
	}

	public Date getSavAt() {
		return this.savAt;
	}

	public void setSavAt(Date savAt) {
		this.savAt = savAt;
	}

	public int getSavAtRangeId() {
		return this.savAtRangeId;
	}

	public void setSavAtRangeId(int savAtRangeId) {
		this.savAtRangeId = savAtRangeId;
	}

	public Date getSentAt() {
		return this.sentAt;
	}

	public void setSentAt(Date sentAt) {
		this.sentAt = sentAt;
	}

	public int getStateId() {
		return this.stateId;
	}

	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getTaxId() {
		return this.taxId;
	}

	public void setTaxId(int taxId) {
		this.taxId = taxId;
	}

	public int getTeamId() {
		return this.teamId;
	}

	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}

	public int getTeleproId() {
		return this.teleproId;
	}

	public void setTeleproId(int teleproId) {
		this.teleproId = teleproId;
	}

	public int getTimeStateId() {
		return this.timeStateId;
	}

	public void setTimeStateId(int timeStateId) {
		this.timeStateId = timeStateId;
	}

	public BigDecimal getTotalPriceWithTaxe() {
		return this.totalPriceWithTaxe;
	}

	public void setTotalPriceWithTaxe(BigDecimal totalPriceWithTaxe) {
		this.totalPriceWithTaxe = totalPriceWithTaxe;
	}

	public BigDecimal getTotalPriceWithoutTaxe() {
		return this.totalPriceWithoutTaxe;
	}

	public void setTotalPriceWithoutTaxe(BigDecimal totalPriceWithoutTaxe) {
		this.totalPriceWithoutTaxe = totalPriceWithoutTaxe;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getVariables() {
		return this.variables;
	}

	public void setVariables(String variables) {
		this.variables = variables;
	}

}