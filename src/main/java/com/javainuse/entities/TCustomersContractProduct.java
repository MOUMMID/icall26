package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_contract_product database table.
 * 
 */
@Entity
@Table(name="t_customers_contract_product")
@NamedQuery(name="TCustomersContractProduct.findAll", query="SELECT t FROM TCustomersContractProduct t")
public class TCustomersContractProduct implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="contract_id")
	private int contractId;

	@Column(name="created_at")
	private Timestamp createdAt;

	private String details;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ended_at")
	private Date endedAt;

	@Column(name="is_consumed")
	private String isConsumed;

	@Column(name="is_one_shoot")
	private String isOneShoot;

	@Column(name="is_prorata")
	private String isProrata;

	@Column(name="product_id")
	private int productId;

	@Column(name="purchase_price_with_tax")
	private BigDecimal purchasePriceWithTax;

	@Column(name="purchase_price_without_tax")
	private BigDecimal purchasePriceWithoutTax;

	private BigDecimal quantity;

	@Column(name="sale_price_with_tax")
	private BigDecimal salePriceWithTax;

	@Column(name="sale_price_without_tax")
	private BigDecimal salePriceWithoutTax;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="started_at")
	private Date startedAt;

	@Column(name="total_purchase_price_with_tax")
	private BigDecimal totalPurchasePriceWithTax;

	@Column(name="total_purchase_price_without_tax")
	private BigDecimal totalPurchasePriceWithoutTax;

	@Column(name="total_sale_price_with_tax")
	private BigDecimal totalSalePriceWithTax;

	@Column(name="total_sale_price_without_tax")
	private BigDecimal totalSalePriceWithoutTax;

	@Column(name="tva_id")
	private int tvaId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TCustomersContractProduct() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getContractId() {
		return this.contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getDetails() {
		return this.details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public Date getEndedAt() {
		return this.endedAt;
	}

	public void setEndedAt(Date endedAt) {
		this.endedAt = endedAt;
	}

	public String getIsConsumed() {
		return this.isConsumed;
	}

	public void setIsConsumed(String isConsumed) {
		this.isConsumed = isConsumed;
	}

	public String getIsOneShoot() {
		return this.isOneShoot;
	}

	public void setIsOneShoot(String isOneShoot) {
		this.isOneShoot = isOneShoot;
	}

	public String getIsProrata() {
		return this.isProrata;
	}

	public void setIsProrata(String isProrata) {
		this.isProrata = isProrata;
	}

	public int getProductId() {
		return this.productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public BigDecimal getPurchasePriceWithTax() {
		return this.purchasePriceWithTax;
	}

	public void setPurchasePriceWithTax(BigDecimal purchasePriceWithTax) {
		this.purchasePriceWithTax = purchasePriceWithTax;
	}

	public BigDecimal getPurchasePriceWithoutTax() {
		return this.purchasePriceWithoutTax;
	}

	public void setPurchasePriceWithoutTax(BigDecimal purchasePriceWithoutTax) {
		this.purchasePriceWithoutTax = purchasePriceWithoutTax;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getSalePriceWithTax() {
		return this.salePriceWithTax;
	}

	public void setSalePriceWithTax(BigDecimal salePriceWithTax) {
		this.salePriceWithTax = salePriceWithTax;
	}

	public BigDecimal getSalePriceWithoutTax() {
		return this.salePriceWithoutTax;
	}

	public void setSalePriceWithoutTax(BigDecimal salePriceWithoutTax) {
		this.salePriceWithoutTax = salePriceWithoutTax;
	}

	public Date getStartedAt() {
		return this.startedAt;
	}

	public void setStartedAt(Date startedAt) {
		this.startedAt = startedAt;
	}

	public BigDecimal getTotalPurchasePriceWithTax() {
		return this.totalPurchasePriceWithTax;
	}

	public void setTotalPurchasePriceWithTax(BigDecimal totalPurchasePriceWithTax) {
		this.totalPurchasePriceWithTax = totalPurchasePriceWithTax;
	}

	public BigDecimal getTotalPurchasePriceWithoutTax() {
		return this.totalPurchasePriceWithoutTax;
	}

	public void setTotalPurchasePriceWithoutTax(BigDecimal totalPurchasePriceWithoutTax) {
		this.totalPurchasePriceWithoutTax = totalPurchasePriceWithoutTax;
	}

	public BigDecimal getTotalSalePriceWithTax() {
		return this.totalSalePriceWithTax;
	}

	public void setTotalSalePriceWithTax(BigDecimal totalSalePriceWithTax) {
		this.totalSalePriceWithTax = totalSalePriceWithTax;
	}

	public BigDecimal getTotalSalePriceWithoutTax() {
		return this.totalSalePriceWithoutTax;
	}

	public void setTotalSalePriceWithoutTax(BigDecimal totalSalePriceWithoutTax) {
		this.totalSalePriceWithoutTax = totalSalePriceWithoutTax;
	}

	public int getTvaId() {
		return this.tvaId;
	}

	public void setTvaId(int tvaId) {
		this.tvaId = tvaId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}