package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;


/**
 * The persistent class for the t_customers_contracts_date_range database table.
 * 
 */
@Entity
@Table(name="t_customers_contracts_date_range")
@NamedQuery(name="TCustomersContractsDateRange.findAll", query="SELECT t FROM TCustomersContractsDateRange t")
public class TCustomersContractsDateRange implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String color;

	private Time from;

	private String name;

	private Time to;

	public TCustomersContractsDateRange() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Time getFrom() {
		return this.from;
	}

	public void setFrom(Time from) {
		this.from = from;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Time getTo() {
		return this.to;
	}

	public void setTo(Time to) {
		this.to = to;
	}

}