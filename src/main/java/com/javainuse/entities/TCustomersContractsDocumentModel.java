package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_customers_contracts_document_model database table.
 * 
 */
@Entity
@Table(name="t_customers_contracts_document_model")
@NamedQuery(name="TCustomersContractsDocumentModel.findAll", query="SELECT t FROM TCustomersContractsDocumentModel t")
public class TCustomersContractsDocumentModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String name;

	public TCustomersContractsDocumentModel() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}