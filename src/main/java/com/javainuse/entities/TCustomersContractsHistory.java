package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_contracts_history database table.
 * 
 */
@Entity
@Table(name="t_customers_contracts_history")
@NamedQuery(name="TCustomersContractsHistory.findAll", query="SELECT t FROM TCustomersContractsHistory t")
public class TCustomersContractsHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="contract_id")
	private int contractId;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Lob
	private String history;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	@Column(name="user_application")
	private String userApplication;

	@Column(name="user_id")
	private int userId;

	public TCustomersContractsHistory() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getContractId() {
		return this.contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getHistory() {
		return this.history;
	}

	public void setHistory(String history) {
		this.history = history;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUserApplication() {
		return this.userApplication;
	}

	public void setUserApplication(String userApplication) {
		this.userApplication = userApplication;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}