package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_contracts_import_errors database table.
 * 
 */
@Entity
@Table(name="t_customers_contracts_import_errors")
@NamedQuery(name="TCustomersContractsImportError.findAll", query="SELECT t FROM TCustomersContractsImportError t")
public class TCustomersContractsImportError implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Lob
	@Column(name="error_text")
	private String errorText;

	private String file;

	@Column(name="import_id")
	private int importId;

	private int line;

	private String status;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TCustomersContractsImportError() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getErrorText() {
		return this.errorText;
	}

	public void setErrorText(String errorText) {
		this.errorText = errorText;
	}

	public String getFile() {
		return this.file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public int getImportId() {
		return this.importId;
	}

	public void setImportId(int importId) {
		this.importId = importId;
	}

	public int getLine() {
		return this.line;
	}

	public void setLine(int line) {
		this.line = line;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}