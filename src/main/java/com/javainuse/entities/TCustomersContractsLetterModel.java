package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_customers_contracts_letter_model database table.
 * 
 */
@Entity
@Table(name="t_customers_contracts_letter_model")
@NamedQuery(name="TCustomersContractsLetterModel.findAll", query="SELECT t FROM TCustomersContractsLetterModel t")
public class TCustomersContractsLetterModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String extension;

	private String name;

	public TCustomersContractsLetterModel() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getExtension() {
		return this.extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}