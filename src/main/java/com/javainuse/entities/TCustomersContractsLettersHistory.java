package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_contracts_letters_history database table.
 * 
 */
@Entity
@Table(name="t_customers_contracts_letters_history")
@NamedQuery(name="TCustomersContractsLettersHistory.findAll", query="SELECT t FROM TCustomersContractsLettersHistory t")
public class TCustomersContractsLettersHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="letter_id")
	private int letterId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	@Column(name="user_application")
	private String userApplication;

	@Column(name="user_id")
	private int userId;

	public TCustomersContractsLettersHistory() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getLetterId() {
		return this.letterId;
	}

	public void setLetterId(int letterId) {
		this.letterId = letterId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUserApplication() {
		return this.userApplication;
	}

	public void setUserApplication(String userApplication) {
		this.userApplication = userApplication;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}