package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_contracts_master database table.
 * 
 */
@Entity
@Table(name="t_customers_contracts_master")
@NamedQuery(name="TCustomersContractsMaster.findAll", query="SELECT t FROM TCustomersContractsMaster t")
public class TCustomersContractsMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private int batch;

	@Column(name="contract_id")
	private int contractId;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="creator_id")
	private int creatorId;

	@Column(name="slave_contract_id")
	private int slaveContractId;

	@Column(name="slave_id")
	private int slaveId;

	@Column(name="slave_state_id")
	private int slaveStateId;

	@Column(name="slave_state_updated_at")
	private Timestamp slaveStateUpdatedAt;

	@Column(name="slave_updated_at")
	private Timestamp slaveUpdatedAt;

	private String status;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TCustomersContractsMaster() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBatch() {
		return this.batch;
	}

	public void setBatch(int batch) {
		this.batch = batch;
	}

	public int getContractId() {
		return this.contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getCreatorId() {
		return this.creatorId;
	}

	public void setCreatorId(int creatorId) {
		this.creatorId = creatorId;
	}

	public int getSlaveContractId() {
		return this.slaveContractId;
	}

	public void setSlaveContractId(int slaveContractId) {
		this.slaveContractId = slaveContractId;
	}

	public int getSlaveId() {
		return this.slaveId;
	}

	public void setSlaveId(int slaveId) {
		this.slaveId = slaveId;
	}

	public int getSlaveStateId() {
		return this.slaveStateId;
	}

	public void setSlaveStateId(int slaveStateId) {
		this.slaveStateId = slaveStateId;
	}

	public Timestamp getSlaveStateUpdatedAt() {
		return this.slaveStateUpdatedAt;
	}

	public void setSlaveStateUpdatedAt(Timestamp slaveStateUpdatedAt) {
		this.slaveStateUpdatedAt = slaveStateUpdatedAt;
	}

	public Timestamp getSlaveUpdatedAt() {
		return this.slaveUpdatedAt;
	}

	public void setSlaveUpdatedAt(Timestamp slaveUpdatedAt) {
		this.slaveUpdatedAt = slaveUpdatedAt;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}