package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_contracts_master_slave_state database table.
 * 
 */
@Entity
@Table(name="t_customers_contracts_master_slave_state")
@NamedQuery(name="TCustomersContractsMasterSlaveState.findAll", query="SELECT t FROM TCustomersContractsMasterSlaveState t")
public class TCustomersContractsMasterSlaveState implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="slave_id")
	private int slaveId;

	@Column(name="state_id")
	private int stateId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	private String value;

	public TCustomersContractsMasterSlaveState() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getSlaveId() {
		return this.slaveId;
	}

	public void setSlaveId(int slaveId) {
		this.slaveId = slaveId;
	}

	public int getStateId() {
		return this.stateId;
	}

	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}