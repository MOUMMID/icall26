package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_contracts_master_translation_state database table.
 * 
 */
@Entity
@Table(name="t_customers_contracts_master_translation_state")
@NamedQuery(name="TCustomersContractsMasterTranslationState.findAll", query="SELECT t FROM TCustomersContractsMasterTranslationState t")
public class TCustomersContractsMasterTranslationState implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="master_state_id")
	private int masterStateId;

	@Column(name="slave_id")
	private int slaveId;

	@Column(name="slave_state_id")
	private int slaveStateId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TCustomersContractsMasterTranslationState() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getMasterStateId() {
		return this.masterStateId;
	}

	public void setMasterStateId(int masterStateId) {
		this.masterStateId = masterStateId;
	}

	public int getSlaveId() {
		return this.slaveId;
	}

	public void setSlaveId(int slaveId) {
		this.slaveId = slaveId;
	}

	public int getSlaveStateId() {
		return this.slaveStateId;
	}

	public void setSlaveStateId(int slaveStateId) {
		this.slaveStateId = slaveStateId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}