package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_contracts_slave_master database table.
 * 
 */
@Entity
@Table(name="t_customers_contracts_slave_master")
@NamedQuery(name="TCustomersContractsSlaveMaster.findAll", query="SELECT t FROM TCustomersContractsSlaveMaster t")
public class TCustomersContractsSlaveMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="contract_id")
	private int contractId;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="creator_id")
	private int creatorId;

	@Column(name="master_contract_id")
	private int masterContractId;

	@Column(name="master_id")
	private int masterId;

	@Column(name="master_updated_at")
	private Timestamp masterUpdatedAt;

	private String status;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TCustomersContractsSlaveMaster() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getContractId() {
		return this.contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getCreatorId() {
		return this.creatorId;
	}

	public void setCreatorId(int creatorId) {
		this.creatorId = creatorId;
	}

	public int getMasterContractId() {
		return this.masterContractId;
	}

	public void setMasterContractId(int masterContractId) {
		this.masterContractId = masterContractId;
	}

	public int getMasterId() {
		return this.masterId;
	}

	public void setMasterId(int masterId) {
		this.masterId = masterId;
	}

	public Timestamp getMasterUpdatedAt() {
		return this.masterUpdatedAt;
	}

	public void setMasterUpdatedAt(Timestamp masterUpdatedAt) {
		this.masterUpdatedAt = masterUpdatedAt;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}