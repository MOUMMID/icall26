package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_contracts_users_permissions_teams_filter database table.
 * 
 */
@Entity
@Table(name="t_customers_contracts_users_permissions_teams_filter")
@NamedQuery(name="TCustomersContractsUsersPermissionsTeamsFilter.findAll", query="SELECT t FROM TCustomersContractsUsersPermissionsTeamsFilter t")
public class TCustomersContractsUsersPermissionsTeamsFilter implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="admin_id")
	private int adminId;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="team_id")
	private int teamId;

	private String type;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TCustomersContractsUsersPermissionsTeamsFilter() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAdminId() {
		return this.adminId;
	}

	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getTeamId() {
		return this.teamId;
	}

	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}