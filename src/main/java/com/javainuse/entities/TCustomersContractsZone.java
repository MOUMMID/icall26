package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_contracts_zone database table.
 * 
 */
@Entity
@Table(name="t_customers_contracts_zone")
@NamedQuery(name="TCustomersContractsZone.findAll", query="SELECT t FROM TCustomersContractsZone t")
public class TCustomersContractsZone implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="is_active")
	private String isActive;

	@Column(name="max_contracts")
	private int maxContracts;

	private String name;

	private String postcodes;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TCustomersContractsZone() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public int getMaxContracts() {
		return this.maxContracts;
	}

	public void setMaxContracts(int maxContracts) {
		this.maxContracts = maxContracts;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPostcodes() {
		return this.postcodes;
	}

	public void setPostcodes(String postcodes) {
		this.postcodes = postcodes;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}