package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_financial database table.
 * 
 */
@Entity
@Table(name="t_customers_financial")
@NamedQuery(name="TCustomersFinancial.findAll", query="SELECT t FROM TCustomersFinancial t")
public class TCustomersFinancial implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="credit_amount")
	private String creditAmount;

	@Column(name="credit_date")
	private String creditDate;

	@Column(name="credit_used")
	private String creditUsed;

	@Column(name="customer_id")
	private int customerId;

	@Column(name="inprogress_credit")
	private String inprogressCredit;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TCustomersFinancial() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreditAmount() {
		return this.creditAmount;
	}

	public void setCreditAmount(String creditAmount) {
		this.creditAmount = creditAmount;
	}

	public String getCreditDate() {
		return this.creditDate;
	}

	public void setCreditDate(String creditDate) {
		this.creditDate = creditDate;
	}

	public String getCreditUsed() {
		return this.creditUsed;
	}

	public void setCreditUsed(String creditUsed) {
		this.creditUsed = creditUsed;
	}

	public int getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getInprogressCredit() {
		return this.inprogressCredit;
	}

	public void setInprogressCredit(String inprogressCredit) {
		this.inprogressCredit = inprogressCredit;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}