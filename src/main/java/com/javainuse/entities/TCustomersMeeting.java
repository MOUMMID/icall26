package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_meeting database table.
 * 
 */
@Entity
@Table(name="t_customers_meeting")
@NamedQuery(name="TCustomersMeeting.findAll", query="SELECT t FROM TCustomersMeeting t")
public class TCustomersMeeting implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="assistant_id")
	private int assistantId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="callback_at")
	private Date callbackAt;

	@Column(name="callback_cancel_at")
	private Timestamp callbackCancelAt;

	@Column(name="callcenter_id")
	private int callcenterId;

	@Column(name="campaign_id")
	private int campaignId;

	@Column(name="confirmator_id")
	private int confirmatorId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="confirmed_at")
	private Date confirmedAt;

	@Column(name="confirmed_by_id")
	private int confirmedById;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="created_by_id")
	private int createdById;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="creation_at")
	private Date creationAt;

	@Column(name="customer_id")
	private int customerId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="in_at")
	private Date inAt;

	@Column(name="in_at_range_id")
	private int inAtRangeId;

	@Column(name="is_callback_cancelled")
	private String isCallbackCancelled;

	@Column(name="is_confirmed")
	private String isConfirmed;

	@Column(name="is_hold")
	private String isHold;

	@Column(name="is_locked")
	private String isLocked;

	@Column(name="is_qualified")
	private String isQualified;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="lock_time")
	private Date lockTime;

	@Column(name="lock_user_id")
	private int lockUserId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="opc_at")
	private Date opcAt;

	@Column(name="opc_range_id")
	private int opcRangeId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="out_at")
	private Date outAt;

	@Column(name="partner_layer_id")
	private int partnerLayerId;

	@Column(name="polluter_id")
	private int polluterId;

	private String registration;

	@Lob
	private String remarks;

	@Lob
	@Column(name="sale_comments")
	private String saleComments;

	@Column(name="sale2_id")
	private int sale2Id;

	@Column(name="sales_id")
	private int salesId;

	@Column(name="state_id")
	private int stateId;

	@Column(name="state_updated_at")
	private Timestamp stateUpdatedAt;

	private String status;

	@Column(name="status_call_id")
	private int statusCallId;

	@Column(name="status_lead_id")
	private int statusLeadId;

	@Column(name="telepro_id")
	private int teleproId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="treated_at")
	private Date treatedAt;

	private BigDecimal turnover;

	@Column(name="type_id")
	private int typeId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	@Lob
	private String variables;

	public TCustomersMeeting() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAssistantId() {
		return this.assistantId;
	}

	public void setAssistantId(int assistantId) {
		this.assistantId = assistantId;
	}

	public Date getCallbackAt() {
		return this.callbackAt;
	}

	public void setCallbackAt(Date callbackAt) {
		this.callbackAt = callbackAt;
	}

	public Timestamp getCallbackCancelAt() {
		return this.callbackCancelAt;
	}

	public void setCallbackCancelAt(Timestamp callbackCancelAt) {
		this.callbackCancelAt = callbackCancelAt;
	}

	public int getCallcenterId() {
		return this.callcenterId;
	}

	public void setCallcenterId(int callcenterId) {
		this.callcenterId = callcenterId;
	}

	public int getCampaignId() {
		return this.campaignId;
	}

	public void setCampaignId(int campaignId) {
		this.campaignId = campaignId;
	}

	public int getConfirmatorId() {
		return this.confirmatorId;
	}

	public void setConfirmatorId(int confirmatorId) {
		this.confirmatorId = confirmatorId;
	}

	public Date getConfirmedAt() {
		return this.confirmedAt;
	}

	public void setConfirmedAt(Date confirmedAt) {
		this.confirmedAt = confirmedAt;
	}

	public int getConfirmedById() {
		return this.confirmedById;
	}

	public void setConfirmedById(int confirmedById) {
		this.confirmedById = confirmedById;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getCreatedById() {
		return this.createdById;
	}

	public void setCreatedById(int createdById) {
		this.createdById = createdById;
	}

	public Date getCreationAt() {
		return this.creationAt;
	}

	public void setCreationAt(Date creationAt) {
		this.creationAt = creationAt;
	}

	public int getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public Date getInAt() {
		return this.inAt;
	}

	public void setInAt(Date inAt) {
		this.inAt = inAt;
	}

	public int getInAtRangeId() {
		return this.inAtRangeId;
	}

	public void setInAtRangeId(int inAtRangeId) {
		this.inAtRangeId = inAtRangeId;
	}

	public String getIsCallbackCancelled() {
		return this.isCallbackCancelled;
	}

	public void setIsCallbackCancelled(String isCallbackCancelled) {
		this.isCallbackCancelled = isCallbackCancelled;
	}

	public String getIsConfirmed() {
		return this.isConfirmed;
	}

	public void setIsConfirmed(String isConfirmed) {
		this.isConfirmed = isConfirmed;
	}

	public String getIsHold() {
		return this.isHold;
	}

	public void setIsHold(String isHold) {
		this.isHold = isHold;
	}

	public String getIsLocked() {
		return this.isLocked;
	}

	public void setIsLocked(String isLocked) {
		this.isLocked = isLocked;
	}

	public String getIsQualified() {
		return this.isQualified;
	}

	public void setIsQualified(String isQualified) {
		this.isQualified = isQualified;
	}

	public Date getLockTime() {
		return this.lockTime;
	}

	public void setLockTime(Date lockTime) {
		this.lockTime = lockTime;
	}

	public int getLockUserId() {
		return this.lockUserId;
	}

	public void setLockUserId(int lockUserId) {
		this.lockUserId = lockUserId;
	}

	public Date getOpcAt() {
		return this.opcAt;
	}

	public void setOpcAt(Date opcAt) {
		this.opcAt = opcAt;
	}

	public int getOpcRangeId() {
		return this.opcRangeId;
	}

	public void setOpcRangeId(int opcRangeId) {
		this.opcRangeId = opcRangeId;
	}

	public Date getOutAt() {
		return this.outAt;
	}

	public void setOutAt(Date outAt) {
		this.outAt = outAt;
	}

	public int getPartnerLayerId() {
		return this.partnerLayerId;
	}

	public void setPartnerLayerId(int partnerLayerId) {
		this.partnerLayerId = partnerLayerId;
	}

	public int getPolluterId() {
		return this.polluterId;
	}

	public void setPolluterId(int polluterId) {
		this.polluterId = polluterId;
	}

	public String getRegistration() {
		return this.registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getSaleComments() {
		return this.saleComments;
	}

	public void setSaleComments(String saleComments) {
		this.saleComments = saleComments;
	}

	public int getSale2Id() {
		return this.sale2Id;
	}

	public void setSale2Id(int sale2Id) {
		this.sale2Id = sale2Id;
	}

	public int getSalesId() {
		return this.salesId;
	}

	public void setSalesId(int salesId) {
		this.salesId = salesId;
	}

	public int getStateId() {
		return this.stateId;
	}

	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

	public Timestamp getStateUpdatedAt() {
		return this.stateUpdatedAt;
	}

	public void setStateUpdatedAt(Timestamp stateUpdatedAt) {
		this.stateUpdatedAt = stateUpdatedAt;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getStatusCallId() {
		return this.statusCallId;
	}

	public void setStatusCallId(int statusCallId) {
		this.statusCallId = statusCallId;
	}

	public int getStatusLeadId() {
		return this.statusLeadId;
	}

	public void setStatusLeadId(int statusLeadId) {
		this.statusLeadId = statusLeadId;
	}

	public int getTeleproId() {
		return this.teleproId;
	}

	public void setTeleproId(int teleproId) {
		this.teleproId = teleproId;
	}

	public Date getTreatedAt() {
		return this.treatedAt;
	}

	public void setTreatedAt(Date treatedAt) {
		this.treatedAt = treatedAt;
	}

	public BigDecimal getTurnover() {
		return this.turnover;
	}

	public void setTurnover(BigDecimal turnover) {
		this.turnover = turnover;
	}

	public int getTypeId() {
		return this.typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getVariables() {
		return this.variables;
	}

	public void setVariables(String variables) {
		this.variables = variables;
	}

}