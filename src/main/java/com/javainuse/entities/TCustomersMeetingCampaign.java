package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_customers_meeting_campaign database table.
 * 
 */
@Entity
@Table(name="t_customers_meeting_campaign")
@NamedQuery(name="TCustomersMeetingCampaign.findAll", query="SELECT t FROM TCustomersMeetingCampaign t")
public class TCustomersMeetingCampaign implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String name;

	public TCustomersMeetingCampaign() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}