package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_meeting_forms database table.
 * 
 */
@Entity
@Table(name="t_customers_meeting_forms")
@NamedQuery(name="TCustomersMeetingForm.findAll", query="SELECT t FROM TCustomersMeetingForm t")
public class TCustomersMeetingForm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="contract_id")
	private int contractId;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Lob
	private String data;

	@Column(name="is_exported")
	private String isExported;

	@Column(name="is_hold")
	private String isHold;

	@Column(name="is_processed")
	private String isProcessed;

	@Column(name="meeting_id")
	private int meetingId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TCustomersMeetingForm() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getContractId() {
		return this.contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getData() {
		return this.data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getIsExported() {
		return this.isExported;
	}

	public void setIsExported(String isExported) {
		this.isExported = isExported;
	}

	public String getIsHold() {
		return this.isHold;
	}

	public void setIsHold(String isHold) {
		this.isHold = isHold;
	}

	public String getIsProcessed() {
		return this.isProcessed;
	}

	public void setIsProcessed(String isProcessed) {
		this.isProcessed = isProcessed;
	}

	public int getMeetingId() {
		return this.meetingId;
	}

	public void setMeetingId(int meetingId) {
		this.meetingId = meetingId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}