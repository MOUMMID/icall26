package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_customers_meeting_form_i18n database table.
 * 
 */
@Entity
@Table(name="t_customers_meeting_form_i18n")
@NamedQuery(name="TCustomersMeetingFormI18n.findAll", query="SELECT t FROM TCustomersMeetingFormI18n t")
public class TCustomersMeetingFormI18n implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="form_id")
	private int formId;

	private String lang;

	private int position;

	private String value;

	public TCustomersMeetingFormI18n() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFormId() {
		return this.formId;
	}

	public void setFormId(int formId) {
		this.formId = formId;
	}

	public String getLang() {
		return this.lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public int getPosition() {
		return this.position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}