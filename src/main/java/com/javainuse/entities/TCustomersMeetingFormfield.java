package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_customers_meeting_formfield database table.
 * 
 */
@Entity
@Table(name="t_customers_meeting_formfield")
@NamedQuery(name="TCustomersMeetingFormfield.findAll", query="SELECT t FROM TCustomersMeetingFormfield t")
public class TCustomersMeetingFormfield implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="default")
	private String default_;

	@Column(name="form_id")
	private int formId;

	@Column(name="is_exportable")
	private String isExportable;

	@Column(name="is_visible")
	private String isVisible;

	private String name;

	private int position;

	private String type;

	private String widget;

	public TCustomersMeetingFormfield() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDefault_() {
		return this.default_;
	}

	public void setDefault_(String default_) {
		this.default_ = default_;
	}

	public int getFormId() {
		return this.formId;
	}

	public void setFormId(int formId) {
		this.formId = formId;
	}

	public String getIsExportable() {
		return this.isExportable;
	}

	public void setIsExportable(String isExportable) {
		this.isExportable = isExportable;
	}

	public String getIsVisible() {
		return this.isVisible;
	}

	public void setIsVisible(String isVisible) {
		this.isVisible = isVisible;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPosition() {
		return this.position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getWidget() {
		return this.widget;
	}

	public void setWidget(String widget) {
		this.widget = widget;
	}

}