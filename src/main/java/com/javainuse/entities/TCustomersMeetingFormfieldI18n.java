package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_meeting_formfield_i18n database table.
 * 
 */
@Entity
@Table(name="t_customers_meeting_formfield_i18n")
@NamedQuery(name="TCustomersMeetingFormfieldI18n.findAll", query="SELECT t FROM TCustomersMeetingFormfieldI18n t")
public class TCustomersMeetingFormfieldI18n implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="formfield_id")
	private int formfieldId;

	private String lang;

	private String parameters;

	private String request;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TCustomersMeetingFormfieldI18n() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getFormfieldId() {
		return this.formfieldId;
	}

	public void setFormfieldId(int formfieldId) {
		this.formfieldId = formfieldId;
	}

	public String getLang() {
		return this.lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getParameters() {
		return this.parameters;
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}

	public String getRequest() {
		return this.request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}