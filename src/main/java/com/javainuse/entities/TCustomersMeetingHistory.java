package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_meeting_history database table.
 * 
 */
@Entity
@Table(name="t_customers_meeting_history")
@NamedQuery(name="TCustomersMeetingHistory.findAll", query="SELECT t FROM TCustomersMeetingHistory t")
public class TCustomersMeetingHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Lob
	private String comment;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="customer_id")
	private int customerId;

	@Column(name="new_status_id")
	private int newStatusId;

	@Column(name="old_status_id")
	private int oldStatusId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	@Column(name="user_id")
	private int userId;

	public TCustomersMeetingHistory() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getNewStatusId() {
		return this.newStatusId;
	}

	public void setNewStatusId(int newStatusId) {
		this.newStatusId = newStatusId;
	}

	public int getOldStatusId() {
		return this.oldStatusId;
	}

	public void setOldStatusId(int oldStatusId) {
		this.oldStatusId = oldStatusId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}