package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_meeting_import_file database table.
 * 
 */
@Entity
@Table(name="t_customers_meeting_import_file")
@NamedQuery(name="TCustomersMeetingImportFile.findAll", query="SELECT t FROM TCustomersMeetingImportFile t")
public class TCustomersMeetingImportFile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String application;

	@Column(name="callcenter_id")
	private int callcenterId;

	@Column(name="campaign_id")
	private int campaignId;

	@Lob
	private String columns;

	@Column(name="created_at")
	private Timestamp createdAt;

	private String file;

	@Column(name="file_log")
	private String fileLog;

	private int filesize;

	@Column(name="format_id")
	private int formatId;

	@Column(name="has_header")
	private String hasHeader;

	@Column(name="lines_processed")
	private int linesProcessed;

	private String name;

	@Column(name="number_of_leads")
	private int numberOfLeads;

	@Column(name="number_of_lines")
	private int numberOfLines;

	private String status;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	@Column(name="user_id")
	private int userId;

	public TCustomersMeetingImportFile() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getApplication() {
		return this.application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public int getCallcenterId() {
		return this.callcenterId;
	}

	public void setCallcenterId(int callcenterId) {
		this.callcenterId = callcenterId;
	}

	public int getCampaignId() {
		return this.campaignId;
	}

	public void setCampaignId(int campaignId) {
		this.campaignId = campaignId;
	}

	public String getColumns() {
		return this.columns;
	}

	public void setColumns(String columns) {
		this.columns = columns;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getFile() {
		return this.file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getFileLog() {
		return this.fileLog;
	}

	public void setFileLog(String fileLog) {
		this.fileLog = fileLog;
	}

	public int getFilesize() {
		return this.filesize;
	}

	public void setFilesize(int filesize) {
		this.filesize = filesize;
	}

	public int getFormatId() {
		return this.formatId;
	}

	public void setFormatId(int formatId) {
		this.formatId = formatId;
	}

	public String getHasHeader() {
		return this.hasHeader;
	}

	public void setHasHeader(String hasHeader) {
		this.hasHeader = hasHeader;
	}

	public int getLinesProcessed() {
		return this.linesProcessed;
	}

	public void setLinesProcessed(int linesProcessed) {
		this.linesProcessed = linesProcessed;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumberOfLeads() {
		return this.numberOfLeads;
	}

	public void setNumberOfLeads(int numberOfLeads) {
		this.numberOfLeads = numberOfLeads;
	}

	public int getNumberOfLines() {
		return this.numberOfLines;
	}

	public void setNumberOfLines(int numberOfLines) {
		this.numberOfLines = numberOfLines;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}