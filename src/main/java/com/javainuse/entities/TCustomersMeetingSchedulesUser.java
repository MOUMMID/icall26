package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_meeting_schedules_user database table.
 * 
 */
@Entity
@Table(name="t_customers_meeting_schedules_user")
@NamedQuery(name="TCustomersMeetingSchedulesUser.findAll", query="SELECT t FROM TCustomersMeetingSchedulesUser t")
public class TCustomersMeetingSchedulesUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="address_id")
	private int addressId;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="in_at")
	private Date inAt;

	@Column(name="meeting_id")
	private int meetingId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="out_at")
	private Date outAt;

	@Column(name="schedule_user_id")
	private int scheduleUserId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="transportation_at")
	private Date transportationAt;

	@Column(name="transportation_distance")
	private int transportationDistance;

	@Column(name="transportation_duration")
	private int transportationDuration;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	@Column(name="user_id")
	private int userId;

	public TCustomersMeetingSchedulesUser() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAddressId() {
		return this.addressId;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public Date getInAt() {
		return this.inAt;
	}

	public void setInAt(Date inAt) {
		this.inAt = inAt;
	}

	public int getMeetingId() {
		return this.meetingId;
	}

	public void setMeetingId(int meetingId) {
		this.meetingId = meetingId;
	}

	public Date getOutAt() {
		return this.outAt;
	}

	public void setOutAt(Date outAt) {
		this.outAt = outAt;
	}

	public int getScheduleUserId() {
		return this.scheduleUserId;
	}

	public void setScheduleUserId(int scheduleUserId) {
		this.scheduleUserId = scheduleUserId;
	}

	public Date getTransportationAt() {
		return this.transportationAt;
	}

	public void setTransportationAt(Date transportationAt) {
		this.transportationAt = transportationAt;
	}

	public int getTransportationDistance() {
		return this.transportationDistance;
	}

	public void setTransportationDistance(int transportationDistance) {
		this.transportationDistance = transportationDistance;
	}

	public int getTransportationDuration() {
		return this.transportationDuration;
	}

	public void setTransportationDuration(int transportationDuration) {
		this.transportationDuration = transportationDuration;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}