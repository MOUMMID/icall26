package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_customers_meeting_status_call database table.
 * 
 */
@Entity
@Table(name="t_customers_meeting_status_call")
@NamedQuery(name="TCustomersMeetingStatusCall.findAll", query="SELECT t FROM TCustomersMeetingStatusCall t")
public class TCustomersMeetingStatusCall implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String color;

	private String icon;

	private String name;

	public TCustomersMeetingStatusCall() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getIcon() {
		return this.icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}