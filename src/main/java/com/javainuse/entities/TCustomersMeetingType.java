package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_customers_meeting_type database table.
 * 
 */
@Entity
@Table(name="t_customers_meeting_type")
@NamedQuery(name="TCustomersMeetingType.findAll", query="SELECT t FROM TCustomersMeetingType t")
public class TCustomersMeetingType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String name;

	public TCustomersMeetingType() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}