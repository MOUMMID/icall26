package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_meetings_forms_documents_formfield database table.
 * 
 */
@Entity
@Table(name="t_customers_meetings_forms_documents_formfield")
@NamedQuery(name="TCustomersMeetingsFormsDocumentsFormfield.findAll", query="SELECT t FROM TCustomersMeetingsFormsDocumentsFormfield t")
public class TCustomersMeetingsFormsDocumentsFormfield implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="document_id")
	private int documentId;

	@Column(name="form_id")
	private int formId;

	@Column(name="formfield_i18n_id")
	private int formfieldI18nId;

	@Column(name="formfield_id")
	private int formfieldId;

	private String operation;

	private short type;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	private String value;

	public TCustomersMeetingsFormsDocumentsFormfield() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getDocumentId() {
		return this.documentId;
	}

	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}

	public int getFormId() {
		return this.formId;
	}

	public void setFormId(int formId) {
		this.formId = formId;
	}

	public int getFormfieldI18nId() {
		return this.formfieldI18nId;
	}

	public void setFormfieldI18nId(int formfieldI18nId) {
		this.formfieldI18nId = formfieldI18nId;
	}

	public int getFormfieldId() {
		return this.formfieldId;
	}

	public void setFormfieldId(int formfieldId) {
		this.formfieldId = formfieldId;
	}

	public String getOperation() {
		return this.operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public short getType() {
		return this.type;
	}

	public void setType(short type) {
		this.type = type;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}