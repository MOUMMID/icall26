package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_meetings_master database table.
 * 
 */
@Entity
@Table(name="t_customers_meetings_master")
@NamedQuery(name="TCustomersMeetingsMaster.findAll", query="SELECT t FROM TCustomersMeetingsMaster t")
public class TCustomersMeetingsMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="creator_id")
	private int creatorId;

	@Column(name="meeting_id")
	private int meetingId;

	@Column(name="slave_id")
	private int slaveId;

	@Column(name="slave_meeting_id")
	private int slaveMeetingId;

	@Column(name="slave_updated_at")
	private Timestamp slaveUpdatedAt;

	private String status;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TCustomersMeetingsMaster() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getCreatorId() {
		return this.creatorId;
	}

	public void setCreatorId(int creatorId) {
		this.creatorId = creatorId;
	}

	public int getMeetingId() {
		return this.meetingId;
	}

	public void setMeetingId(int meetingId) {
		this.meetingId = meetingId;
	}

	public int getSlaveId() {
		return this.slaveId;
	}

	public void setSlaveId(int slaveId) {
		this.slaveId = slaveId;
	}

	public int getSlaveMeetingId() {
		return this.slaveMeetingId;
	}

	public void setSlaveMeetingId(int slaveMeetingId) {
		this.slaveMeetingId = slaveMeetingId;
	}

	public Timestamp getSlaveUpdatedAt() {
		return this.slaveUpdatedAt;
	}

	public void setSlaveUpdatedAt(Timestamp slaveUpdatedAt) {
		this.slaveUpdatedAt = slaveUpdatedAt;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}