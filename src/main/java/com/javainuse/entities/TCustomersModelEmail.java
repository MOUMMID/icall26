package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_customers_model_email database table.
 * 
 */
@Entity
@Table(name="t_customers_model_email")
@NamedQuery(name="TCustomersModelEmail.findAll", query="SELECT t FROM TCustomersModelEmail t")
public class TCustomersModelEmail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String name;

	public TCustomersModelEmail() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}