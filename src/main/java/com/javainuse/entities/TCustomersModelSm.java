package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_customers_model_sms database table.
 * 
 */
@Entity
@Table(name="t_customers_model_sms")
@NamedQuery(name="TCustomersModelSm.findAll", query="SELECT t FROM TCustomersModelSm t")
public class TCustomersModelSm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String name;

	public TCustomersModelSm() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}