package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_union_i18n database table.
 * 
 */
@Entity
@Table(name="t_customers_union_i18n")
@NamedQuery(name="TCustomersUnionI18n.findAll", query="SELECT t FROM TCustomersUnionI18n t")
public class TCustomersUnionI18n implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="created_at")
	private Timestamp createdAt;

	private String lang;

	@Column(name="union_id")
	private int unionId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	private String value;

	public TCustomersUnionI18n() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getLang() {
		return this.lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public int getUnionId() {
		return this.unionId;
	}

	public void setUnionId(int unionId) {
		this.unionId = unionId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}