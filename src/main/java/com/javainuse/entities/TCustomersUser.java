package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_user database table.
 * 
 */
@Entity
@Table(name="t_customers_user")
@NamedQuery(name="TCustomersUser.findAll", query="SELECT t FROM TCustomersUser t")
public class TCustomersUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String avatar;

	@Temporal(TemporalType.DATE)
	private Date birthday;

	@Column(name="company_id")
	private int companyId;

	@Column(name="company_user_id")
	private int companyUserId;

	@Column(name="created_at")
	private Timestamp createdAt;

	private String culture;

	@Column(name="customer_id")
	private int customerId;

	private String email;

	@Column(name="email_tosend")
	private String emailTosend;

	private String firstname;

	private String gender;

	@Column(name="is_active")
	private String isActive;

	@Column(name="is_admin")
	private String isAdmin;

	@Column(name="is_guess")
	private String isGuess;

	@Column(name="is_locked")
	private String isLocked;

	@Column(name="is_subscript")
	private String isSubscript;

	private String lang;

	@Column(name="last_password_gen")
	private Timestamp lastPasswordGen;

	@Column(name="last_update")
	private Timestamp lastUpdate;

	private Timestamp lastlogin;

	private String lastname;

	@Column(name="locked_at")
	private Timestamp lockedAt;

	private String mobile;

	@Column(name="number_of_try")
	private int numberOfTry;

	private String occupation;

	private String password;

	private String phone;

	private String status;

	@Column(name="unlocked_by")
	private int unlockedBy;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	private String username;

	public TCustomersUser() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAvatar() {
		return this.avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Date getBirthday() {
		return this.birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public int getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public int getCompanyUserId() {
		return this.companyUserId;
	}

	public void setCompanyUserId(int companyUserId) {
		this.companyUserId = companyUserId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getCulture() {
		return this.culture;
	}

	public void setCulture(String culture) {
		this.culture = culture;
	}

	public int getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailTosend() {
		return this.emailTosend;
	}

	public void setEmailTosend(String emailTosend) {
		this.emailTosend = emailTosend;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getIsAdmin() {
		return this.isAdmin;
	}

	public void setIsAdmin(String isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String getIsGuess() {
		return this.isGuess;
	}

	public void setIsGuess(String isGuess) {
		this.isGuess = isGuess;
	}

	public String getIsLocked() {
		return this.isLocked;
	}

	public void setIsLocked(String isLocked) {
		this.isLocked = isLocked;
	}

	public String getIsSubscript() {
		return this.isSubscript;
	}

	public void setIsSubscript(String isSubscript) {
		this.isSubscript = isSubscript;
	}

	public String getLang() {
		return this.lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public Timestamp getLastPasswordGen() {
		return this.lastPasswordGen;
	}

	public void setLastPasswordGen(Timestamp lastPasswordGen) {
		this.lastPasswordGen = lastPasswordGen;
	}

	public Timestamp getLastUpdate() {
		return this.lastUpdate;
	}

	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public Timestamp getLastlogin() {
		return this.lastlogin;
	}

	public void setLastlogin(Timestamp lastlogin) {
		this.lastlogin = lastlogin;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Timestamp getLockedAt() {
		return this.lockedAt;
	}

	public void setLockedAt(Timestamp lockedAt) {
		this.lockedAt = lockedAt;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public int getNumberOfTry() {
		return this.numberOfTry;
	}

	public void setNumberOfTry(int numberOfTry) {
		this.numberOfTry = numberOfTry;
	}

	public String getOccupation() {
		return this.occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getUnlockedBy() {
		return this.unlockedBy;
	}

	public void setUnlockedBy(int unlockedBy) {
		this.unlockedBy = unlockedBy;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}