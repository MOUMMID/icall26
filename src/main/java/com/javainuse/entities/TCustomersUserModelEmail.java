package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_customers_user_model_email database table.
 * 
 */
@Entity
@Table(name="t_customers_user_model_email")
@NamedQuery(name="TCustomersUserModelEmail.findAll", query="SELECT t FROM TCustomersUserModelEmail t")
public class TCustomersUserModelEmail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String name;

	public TCustomersUserModelEmail() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}