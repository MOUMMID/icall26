package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_user_sessions database table.
 * 
 */
@Entity
@Table(name="t_customers_user_sessions")
@NamedQuery(name="TCustomersUserSession.findAll", query="SELECT t FROM TCustomersUserSession t")
public class TCustomersUserSession implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="customer_user_id")
	private int customerUserId;

	private String ip;

	@Column(name="last_time")
	private Timestamp lastTime;

	private BigDecimal lat;

	private BigDecimal lng;

	private String session;

	@Column(name="start_time")
	private Timestamp startTime;

	public TCustomersUserSession() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCustomerUserId() {
		return this.customerUserId;
	}

	public void setCustomerUserId(int customerUserId) {
		this.customerUserId = customerUserId;
	}

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Timestamp getLastTime() {
		return this.lastTime;
	}

	public void setLastTime(Timestamp lastTime) {
		this.lastTime = lastTime;
	}

	public BigDecimal getLat() {
		return this.lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return this.lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}

	public String getSession() {
		return this.session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public Timestamp getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

}