package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_customers_user_token database table.
 * 
 */
@Entity
@Table(name="t_customers_user_token")
@NamedQuery(name="TCustomersUserToken.findAll", query="SELECT t FROM TCustomersUserToken t")
public class TCustomersUserToken implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="is_email_sent")
	private String isEmailSent;

	private String token;

	@Column(name="user_id")
	private int userId;

	public TCustomersUserToken() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getIsEmailSent() {
		return this.isEmailSent;
	}

	public void setIsEmailSent(String isEmailSent) {
		this.isEmailSent = isEmailSent;
	}

	public String getToken() {
		return this.token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}