package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the t_domoprime_asset database table.
 * 
 */
@Entity
@Table(name="t_domoprime_asset")
@NamedQuery(name="TDomoprimeAsset.findAll", query="SELECT t FROM TDomoprimeAsset t")
public class TDomoprimeAsset implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="billing_id")
	private int billingId;

	@Lob
	private String comments;

	@Column(name="contract_id")
	private int contractId;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="creator_id")
	private int creatorId;

	@Column(name="customer_id")
	private int customerId;

	@Column(name="dated_at")
	private Timestamp datedAt;

	private int day;

	@Column(name="meeting_id")
	private int meetingId;

	private int month;

	private String reference;

	private String status;

	@Column(name="status_id")
	private int statusId;

	@Column(name="total_asset_with_tax")
	private BigDecimal totalAssetWithTax;

	@Column(name="total_asset_without_tax")
	private BigDecimal totalAssetWithoutTax;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	private int year;

	public TDomoprimeAsset() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBillingId() {
		return this.billingId;
	}

	public void setBillingId(int billingId) {
		this.billingId = billingId;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getContractId() {
		return this.contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getCreatorId() {
		return this.creatorId;
	}

	public void setCreatorId(int creatorId) {
		this.creatorId = creatorId;
	}

	public int getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public Timestamp getDatedAt() {
		return this.datedAt;
	}

	public void setDatedAt(Timestamp datedAt) {
		this.datedAt = datedAt;
	}

	public int getDay() {
		return this.day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMeetingId() {
		return this.meetingId;
	}

	public void setMeetingId(int meetingId) {
		this.meetingId = meetingId;
	}

	public int getMonth() {
		return this.month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public String getReference() {
		return this.reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getStatusId() {
		return this.statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public BigDecimal getTotalAssetWithTax() {
		return this.totalAssetWithTax;
	}

	public void setTotalAssetWithTax(BigDecimal totalAssetWithTax) {
		this.totalAssetWithTax = totalAssetWithTax;
	}

	public BigDecimal getTotalAssetWithoutTax() {
		return this.totalAssetWithoutTax;
	}

	public void setTotalAssetWithoutTax(BigDecimal totalAssetWithoutTax) {
		this.totalAssetWithoutTax = totalAssetWithoutTax;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public int getYear() {
		return this.year;
	}

	public void setYear(int year) {
		this.year = year;
	}

}