package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_domoprime_asset_model database table.
 * 
 */
@Entity
@Table(name="t_domoprime_asset_model")
@NamedQuery(name="TDomoprimeAssetModel.findAll", query="SELECT t FROM TDomoprimeAssetModel t")
public class TDomoprimeAssetModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String name;

	public TDomoprimeAssetModel() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}