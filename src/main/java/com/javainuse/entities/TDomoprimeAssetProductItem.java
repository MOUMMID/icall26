package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the t_domoprime_asset_product_item database table.
 * 
 */
@Entity
@Table(name="t_domoprime_asset_product_item")
@NamedQuery(name="TDomoprimeAssetProductItem.findAll", query="SELECT t FROM TDomoprimeAssetProductItem t")
public class TDomoprimeAssetProductItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="asset_id")
	private int assetId;

	@Column(name="asset_product_id")
	private int assetProductId;

	@Column(name="created_at")
	private Timestamp createdAt;

	private String description;

	@Lob
	private String details;

	private String entitled;

	private int id;

	@Column(name="item_id")
	private int itemId;

	@Column(name="product_id")
	private int productId;

	@Column(name="product_item_id")
	private int productItemId;

	@Column(name="purchase_price_with_tax")
	private BigDecimal purchasePriceWithTax;

	@Column(name="purchase_price_without_tax")
	private BigDecimal purchasePriceWithoutTax;

	private BigDecimal quantity;

	@Column(name="sale_price_with_tax")
	private BigDecimal salePriceWithTax;

	@Column(name="sale_price_without_tax")
	private BigDecimal salePriceWithoutTax;

	private String status;

	private String title;

	@Column(name="total_purchase_price_with_tax")
	private BigDecimal totalPurchasePriceWithTax;

	@Column(name="total_purchase_price_without_tax")
	private BigDecimal totalPurchasePriceWithoutTax;

	@Column(name="total_sale_price_with_tax")
	private BigDecimal totalSalePriceWithTax;

	@Column(name="total_sale_price_without_tax")
	private BigDecimal totalSalePriceWithoutTax;

	@Column(name="tva_id")
	private int tvaId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TDomoprimeAssetProductItem() {
	}

	public int getAssetId() {
		return this.assetId;
	}

	public void setAssetId(int assetId) {
		this.assetId = assetId;
	}

	public int getAssetProductId() {
		return this.assetProductId;
	}

	public void setAssetProductId(int assetProductId) {
		this.assetProductId = assetProductId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDetails() {
		return this.details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getEntitled() {
		return this.entitled;
	}

	public void setEntitled(String entitled) {
		this.entitled = entitled;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getItemId() {
		return this.itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public int getProductId() {
		return this.productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getProductItemId() {
		return this.productItemId;
	}

	public void setProductItemId(int productItemId) {
		this.productItemId = productItemId;
	}

	public BigDecimal getPurchasePriceWithTax() {
		return this.purchasePriceWithTax;
	}

	public void setPurchasePriceWithTax(BigDecimal purchasePriceWithTax) {
		this.purchasePriceWithTax = purchasePriceWithTax;
	}

	public BigDecimal getPurchasePriceWithoutTax() {
		return this.purchasePriceWithoutTax;
	}

	public void setPurchasePriceWithoutTax(BigDecimal purchasePriceWithoutTax) {
		this.purchasePriceWithoutTax = purchasePriceWithoutTax;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getSalePriceWithTax() {
		return this.salePriceWithTax;
	}

	public void setSalePriceWithTax(BigDecimal salePriceWithTax) {
		this.salePriceWithTax = salePriceWithTax;
	}

	public BigDecimal getSalePriceWithoutTax() {
		return this.salePriceWithoutTax;
	}

	public void setSalePriceWithoutTax(BigDecimal salePriceWithoutTax) {
		this.salePriceWithoutTax = salePriceWithoutTax;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public BigDecimal getTotalPurchasePriceWithTax() {
		return this.totalPurchasePriceWithTax;
	}

	public void setTotalPurchasePriceWithTax(BigDecimal totalPurchasePriceWithTax) {
		this.totalPurchasePriceWithTax = totalPurchasePriceWithTax;
	}

	public BigDecimal getTotalPurchasePriceWithoutTax() {
		return this.totalPurchasePriceWithoutTax;
	}

	public void setTotalPurchasePriceWithoutTax(BigDecimal totalPurchasePriceWithoutTax) {
		this.totalPurchasePriceWithoutTax = totalPurchasePriceWithoutTax;
	}

	public BigDecimal getTotalSalePriceWithTax() {
		return this.totalSalePriceWithTax;
	}

	public void setTotalSalePriceWithTax(BigDecimal totalSalePriceWithTax) {
		this.totalSalePriceWithTax = totalSalePriceWithTax;
	}

	public BigDecimal getTotalSalePriceWithoutTax() {
		return this.totalSalePriceWithoutTax;
	}

	public void setTotalSalePriceWithoutTax(BigDecimal totalSalePriceWithoutTax) {
		this.totalSalePriceWithoutTax = totalSalePriceWithoutTax;
	}

	public int getTvaId() {
		return this.tvaId;
	}

	public void setTvaId(int tvaId) {
		this.tvaId = tvaId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}