package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the t_domoprime_billing_product database table.
 * 
 */
@Entity
@Table(name="t_domoprime_billing_product")
@NamedQuery(name="TDomoprimeBillingProduct.findAll", query="SELECT t FROM TDomoprimeBillingProduct t")
public class TDomoprimeBillingProduct implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="added_price_with_tax")
	private BigDecimal addedPriceWithTax;

	@Column(name="added_price_without_tax")
	private BigDecimal addedPriceWithoutTax;

	@Column(name="billing_id")
	private int billingId;

	@Column(name="contract_id")
	private int contractId;

	@Column(name="contract_product_id")
	private int contractProductId;

	@Column(name="created_at")
	private Timestamp createdAt;

	private String description;

	@Lob
	private String details;

	private String entitled;

	private int id;

	private BigDecimal prime;

	@Column(name="product_id")
	private int productId;

	@Column(name="purchase_price_with_tax")
	private BigDecimal purchasePriceWithTax;

	@Column(name="purchase_price_without_tax")
	private BigDecimal purchasePriceWithoutTax;

	private BigDecimal quantity;

	@Column(name="restincharge_price_with_tax")
	private BigDecimal restinchargePriceWithTax;

	@Column(name="restincharge_price_without_tax")
	private BigDecimal restinchargePriceWithoutTax;

	@Column(name="sale_discount_price_with_tax")
	private BigDecimal saleDiscountPriceWithTax;

	@Column(name="sale_discount_price_without_tax")
	private BigDecimal saleDiscountPriceWithoutTax;

	@Column(name="sale_price_with_tax")
	private BigDecimal salePriceWithTax;

	@Column(name="sale_price_without_tax")
	private BigDecimal salePriceWithoutTax;

	@Column(name="sale_standard_price_with_tax")
	private BigDecimal saleStandardPriceWithTax;

	@Column(name="sale_standard_price_without_tax")
	private BigDecimal saleStandardPriceWithoutTax;

	private String status;

	private String title;

	@Column(name="total_added_price_with_tax")
	private BigDecimal totalAddedPriceWithTax;

	@Column(name="total_added_price_without_tax")
	private BigDecimal totalAddedPriceWithoutTax;

	@Column(name="total_purchase_price_with_tax")
	private BigDecimal totalPurchasePriceWithTax;

	@Column(name="total_purchase_price_without_tax")
	private BigDecimal totalPurchasePriceWithoutTax;

	@Column(name="total_restincharge_price_with_tax")
	private BigDecimal totalRestinchargePriceWithTax;

	@Column(name="total_restincharge_price_without_tax")
	private BigDecimal totalRestinchargePriceWithoutTax;

	@Column(name="total_sale_discount_price_with_tax")
	private BigDecimal totalSaleDiscountPriceWithTax;

	@Column(name="total_sale_discount_price_without_tax")
	private BigDecimal totalSaleDiscountPriceWithoutTax;

	@Column(name="total_sale_price_with_tax")
	private BigDecimal totalSalePriceWithTax;

	@Column(name="total_sale_price_without_tax")
	private BigDecimal totalSalePriceWithoutTax;

	@Column(name="total_sale_standard_price_with_tax")
	private BigDecimal totalSaleStandardPriceWithTax;

	@Column(name="total_sale_standard_price_without_tax")
	private BigDecimal totalSaleStandardPriceWithoutTax;

	@Column(name="tva_id")
	private int tvaId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TDomoprimeBillingProduct() {
	}

	public BigDecimal getAddedPriceWithTax() {
		return this.addedPriceWithTax;
	}

	public void setAddedPriceWithTax(BigDecimal addedPriceWithTax) {
		this.addedPriceWithTax = addedPriceWithTax;
	}

	public BigDecimal getAddedPriceWithoutTax() {
		return this.addedPriceWithoutTax;
	}

	public void setAddedPriceWithoutTax(BigDecimal addedPriceWithoutTax) {
		this.addedPriceWithoutTax = addedPriceWithoutTax;
	}

	public int getBillingId() {
		return this.billingId;
	}

	public void setBillingId(int billingId) {
		this.billingId = billingId;
	}

	public int getContractId() {
		return this.contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public int getContractProductId() {
		return this.contractProductId;
	}

	public void setContractProductId(int contractProductId) {
		this.contractProductId = contractProductId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDetails() {
		return this.details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getEntitled() {
		return this.entitled;
	}

	public void setEntitled(String entitled) {
		this.entitled = entitled;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BigDecimal getPrime() {
		return this.prime;
	}

	public void setPrime(BigDecimal prime) {
		this.prime = prime;
	}

	public int getProductId() {
		return this.productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public BigDecimal getPurchasePriceWithTax() {
		return this.purchasePriceWithTax;
	}

	public void setPurchasePriceWithTax(BigDecimal purchasePriceWithTax) {
		this.purchasePriceWithTax = purchasePriceWithTax;
	}

	public BigDecimal getPurchasePriceWithoutTax() {
		return this.purchasePriceWithoutTax;
	}

	public void setPurchasePriceWithoutTax(BigDecimal purchasePriceWithoutTax) {
		this.purchasePriceWithoutTax = purchasePriceWithoutTax;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getRestinchargePriceWithTax() {
		return this.restinchargePriceWithTax;
	}

	public void setRestinchargePriceWithTax(BigDecimal restinchargePriceWithTax) {
		this.restinchargePriceWithTax = restinchargePriceWithTax;
	}

	public BigDecimal getRestinchargePriceWithoutTax() {
		return this.restinchargePriceWithoutTax;
	}

	public void setRestinchargePriceWithoutTax(BigDecimal restinchargePriceWithoutTax) {
		this.restinchargePriceWithoutTax = restinchargePriceWithoutTax;
	}

	public BigDecimal getSaleDiscountPriceWithTax() {
		return this.saleDiscountPriceWithTax;
	}

	public void setSaleDiscountPriceWithTax(BigDecimal saleDiscountPriceWithTax) {
		this.saleDiscountPriceWithTax = saleDiscountPriceWithTax;
	}

	public BigDecimal getSaleDiscountPriceWithoutTax() {
		return this.saleDiscountPriceWithoutTax;
	}

	public void setSaleDiscountPriceWithoutTax(BigDecimal saleDiscountPriceWithoutTax) {
		this.saleDiscountPriceWithoutTax = saleDiscountPriceWithoutTax;
	}

	public BigDecimal getSalePriceWithTax() {
		return this.salePriceWithTax;
	}

	public void setSalePriceWithTax(BigDecimal salePriceWithTax) {
		this.salePriceWithTax = salePriceWithTax;
	}

	public BigDecimal getSalePriceWithoutTax() {
		return this.salePriceWithoutTax;
	}

	public void setSalePriceWithoutTax(BigDecimal salePriceWithoutTax) {
		this.salePriceWithoutTax = salePriceWithoutTax;
	}

	public BigDecimal getSaleStandardPriceWithTax() {
		return this.saleStandardPriceWithTax;
	}

	public void setSaleStandardPriceWithTax(BigDecimal saleStandardPriceWithTax) {
		this.saleStandardPriceWithTax = saleStandardPriceWithTax;
	}

	public BigDecimal getSaleStandardPriceWithoutTax() {
		return this.saleStandardPriceWithoutTax;
	}

	public void setSaleStandardPriceWithoutTax(BigDecimal saleStandardPriceWithoutTax) {
		this.saleStandardPriceWithoutTax = saleStandardPriceWithoutTax;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public BigDecimal getTotalAddedPriceWithTax() {
		return this.totalAddedPriceWithTax;
	}

	public void setTotalAddedPriceWithTax(BigDecimal totalAddedPriceWithTax) {
		this.totalAddedPriceWithTax = totalAddedPriceWithTax;
	}

	public BigDecimal getTotalAddedPriceWithoutTax() {
		return this.totalAddedPriceWithoutTax;
	}

	public void setTotalAddedPriceWithoutTax(BigDecimal totalAddedPriceWithoutTax) {
		this.totalAddedPriceWithoutTax = totalAddedPriceWithoutTax;
	}

	public BigDecimal getTotalPurchasePriceWithTax() {
		return this.totalPurchasePriceWithTax;
	}

	public void setTotalPurchasePriceWithTax(BigDecimal totalPurchasePriceWithTax) {
		this.totalPurchasePriceWithTax = totalPurchasePriceWithTax;
	}

	public BigDecimal getTotalPurchasePriceWithoutTax() {
		return this.totalPurchasePriceWithoutTax;
	}

	public void setTotalPurchasePriceWithoutTax(BigDecimal totalPurchasePriceWithoutTax) {
		this.totalPurchasePriceWithoutTax = totalPurchasePriceWithoutTax;
	}

	public BigDecimal getTotalRestinchargePriceWithTax() {
		return this.totalRestinchargePriceWithTax;
	}

	public void setTotalRestinchargePriceWithTax(BigDecimal totalRestinchargePriceWithTax) {
		this.totalRestinchargePriceWithTax = totalRestinchargePriceWithTax;
	}

	public BigDecimal getTotalRestinchargePriceWithoutTax() {
		return this.totalRestinchargePriceWithoutTax;
	}

	public void setTotalRestinchargePriceWithoutTax(BigDecimal totalRestinchargePriceWithoutTax) {
		this.totalRestinchargePriceWithoutTax = totalRestinchargePriceWithoutTax;
	}

	public BigDecimal getTotalSaleDiscountPriceWithTax() {
		return this.totalSaleDiscountPriceWithTax;
	}

	public void setTotalSaleDiscountPriceWithTax(BigDecimal totalSaleDiscountPriceWithTax) {
		this.totalSaleDiscountPriceWithTax = totalSaleDiscountPriceWithTax;
	}

	public BigDecimal getTotalSaleDiscountPriceWithoutTax() {
		return this.totalSaleDiscountPriceWithoutTax;
	}

	public void setTotalSaleDiscountPriceWithoutTax(BigDecimal totalSaleDiscountPriceWithoutTax) {
		this.totalSaleDiscountPriceWithoutTax = totalSaleDiscountPriceWithoutTax;
	}

	public BigDecimal getTotalSalePriceWithTax() {
		return this.totalSalePriceWithTax;
	}

	public void setTotalSalePriceWithTax(BigDecimal totalSalePriceWithTax) {
		this.totalSalePriceWithTax = totalSalePriceWithTax;
	}

	public BigDecimal getTotalSalePriceWithoutTax() {
		return this.totalSalePriceWithoutTax;
	}

	public void setTotalSalePriceWithoutTax(BigDecimal totalSalePriceWithoutTax) {
		this.totalSalePriceWithoutTax = totalSalePriceWithoutTax;
	}

	public BigDecimal getTotalSaleStandardPriceWithTax() {
		return this.totalSaleStandardPriceWithTax;
	}

	public void setTotalSaleStandardPriceWithTax(BigDecimal totalSaleStandardPriceWithTax) {
		this.totalSaleStandardPriceWithTax = totalSaleStandardPriceWithTax;
	}

	public BigDecimal getTotalSaleStandardPriceWithoutTax() {
		return this.totalSaleStandardPriceWithoutTax;
	}

	public void setTotalSaleStandardPriceWithoutTax(BigDecimal totalSaleStandardPriceWithoutTax) {
		this.totalSaleStandardPriceWithoutTax = totalSaleStandardPriceWithoutTax;
	}

	public int getTvaId() {
		return this.tvaId;
	}

	public void setTvaId(int tvaId) {
		this.tvaId = tvaId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}