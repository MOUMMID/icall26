package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the t_domoprime_billing_product_item database table.
 * 
 */
@Entity
@Table(name="t_domoprime_billing_product_item")
@NamedQuery(name="TDomoprimeBillingProductItem.findAll", query="SELECT t FROM TDomoprimeBillingProductItem t")
public class TDomoprimeBillingProductItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="billing_id")
	private int billingId;

	@Column(name="billing_product_id")
	private int billingProductId;

	private BigDecimal coefficient;

	@Column(name="created_at")
	private Timestamp createdAt;

	private String description;

	@Lob
	private String details;

	private String entitled;

	private int id;

	@Column(name="is_mandatory")
	private String isMandatory;

	@Column(name="item_id")
	private int itemId;

	@Column(name="product_id")
	private int productId;

	@Column(name="product_item_id")
	private int productItemId;

	@Column(name="purchase_price_with_tax")
	private BigDecimal purchasePriceWithTax;

	@Column(name="purchase_price_without_tax")
	private BigDecimal purchasePriceWithoutTax;

	private BigDecimal quantity;

	@Column(name="sale_discount_price_with_tax")
	private BigDecimal saleDiscountPriceWithTax;

	@Column(name="sale_discount_price_without_tax")
	private BigDecimal saleDiscountPriceWithoutTax;

	@Column(name="sale_price_with_tax")
	private BigDecimal salePriceWithTax;

	@Column(name="sale_price_without_tax")
	private BigDecimal salePriceWithoutTax;

	private String status;

	private String title;

	@Column(name="total_purchase_price_with_tax")
	private BigDecimal totalPurchasePriceWithTax;

	@Column(name="total_purchase_price_without_tax")
	private BigDecimal totalPurchasePriceWithoutTax;

	@Column(name="total_sale_discount_price_with_tax")
	private BigDecimal totalSaleDiscountPriceWithTax;

	@Column(name="total_sale_discount_price_without_tax")
	private BigDecimal totalSaleDiscountPriceWithoutTax;

	@Column(name="total_sale_price_with_tax")
	private BigDecimal totalSalePriceWithTax;

	@Column(name="total_sale_price_without_tax")
	private BigDecimal totalSalePriceWithoutTax;

	@Column(name="total_tax")
	private BigDecimal totalTax;

	@Column(name="tva_id")
	private int tvaId;

	private String unit;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TDomoprimeBillingProductItem() {
	}

	public int getBillingId() {
		return this.billingId;
	}

	public void setBillingId(int billingId) {
		this.billingId = billingId;
	}

	public int getBillingProductId() {
		return this.billingProductId;
	}

	public void setBillingProductId(int billingProductId) {
		this.billingProductId = billingProductId;
	}

	public BigDecimal getCoefficient() {
		return this.coefficient;
	}

	public void setCoefficient(BigDecimal coefficient) {
		this.coefficient = coefficient;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDetails() {
		return this.details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getEntitled() {
		return this.entitled;
	}

	public void setEntitled(String entitled) {
		this.entitled = entitled;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsMandatory() {
		return this.isMandatory;
	}

	public void setIsMandatory(String isMandatory) {
		this.isMandatory = isMandatory;
	}

	public int getItemId() {
		return this.itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public int getProductId() {
		return this.productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getProductItemId() {
		return this.productItemId;
	}

	public void setProductItemId(int productItemId) {
		this.productItemId = productItemId;
	}

	public BigDecimal getPurchasePriceWithTax() {
		return this.purchasePriceWithTax;
	}

	public void setPurchasePriceWithTax(BigDecimal purchasePriceWithTax) {
		this.purchasePriceWithTax = purchasePriceWithTax;
	}

	public BigDecimal getPurchasePriceWithoutTax() {
		return this.purchasePriceWithoutTax;
	}

	public void setPurchasePriceWithoutTax(BigDecimal purchasePriceWithoutTax) {
		this.purchasePriceWithoutTax = purchasePriceWithoutTax;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getSaleDiscountPriceWithTax() {
		return this.saleDiscountPriceWithTax;
	}

	public void setSaleDiscountPriceWithTax(BigDecimal saleDiscountPriceWithTax) {
		this.saleDiscountPriceWithTax = saleDiscountPriceWithTax;
	}

	public BigDecimal getSaleDiscountPriceWithoutTax() {
		return this.saleDiscountPriceWithoutTax;
	}

	public void setSaleDiscountPriceWithoutTax(BigDecimal saleDiscountPriceWithoutTax) {
		this.saleDiscountPriceWithoutTax = saleDiscountPriceWithoutTax;
	}

	public BigDecimal getSalePriceWithTax() {
		return this.salePriceWithTax;
	}

	public void setSalePriceWithTax(BigDecimal salePriceWithTax) {
		this.salePriceWithTax = salePriceWithTax;
	}

	public BigDecimal getSalePriceWithoutTax() {
		return this.salePriceWithoutTax;
	}

	public void setSalePriceWithoutTax(BigDecimal salePriceWithoutTax) {
		this.salePriceWithoutTax = salePriceWithoutTax;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public BigDecimal getTotalPurchasePriceWithTax() {
		return this.totalPurchasePriceWithTax;
	}

	public void setTotalPurchasePriceWithTax(BigDecimal totalPurchasePriceWithTax) {
		this.totalPurchasePriceWithTax = totalPurchasePriceWithTax;
	}

	public BigDecimal getTotalPurchasePriceWithoutTax() {
		return this.totalPurchasePriceWithoutTax;
	}

	public void setTotalPurchasePriceWithoutTax(BigDecimal totalPurchasePriceWithoutTax) {
		this.totalPurchasePriceWithoutTax = totalPurchasePriceWithoutTax;
	}

	public BigDecimal getTotalSaleDiscountPriceWithTax() {
		return this.totalSaleDiscountPriceWithTax;
	}

	public void setTotalSaleDiscountPriceWithTax(BigDecimal totalSaleDiscountPriceWithTax) {
		this.totalSaleDiscountPriceWithTax = totalSaleDiscountPriceWithTax;
	}

	public BigDecimal getTotalSaleDiscountPriceWithoutTax() {
		return this.totalSaleDiscountPriceWithoutTax;
	}

	public void setTotalSaleDiscountPriceWithoutTax(BigDecimal totalSaleDiscountPriceWithoutTax) {
		this.totalSaleDiscountPriceWithoutTax = totalSaleDiscountPriceWithoutTax;
	}

	public BigDecimal getTotalSalePriceWithTax() {
		return this.totalSalePriceWithTax;
	}

	public void setTotalSalePriceWithTax(BigDecimal totalSalePriceWithTax) {
		this.totalSalePriceWithTax = totalSalePriceWithTax;
	}

	public BigDecimal getTotalSalePriceWithoutTax() {
		return this.totalSalePriceWithoutTax;
	}

	public void setTotalSalePriceWithoutTax(BigDecimal totalSalePriceWithoutTax) {
		this.totalSalePriceWithoutTax = totalSalePriceWithoutTax;
	}

	public BigDecimal getTotalTax() {
		return this.totalTax;
	}

	public void setTotalTax(BigDecimal totalTax) {
		this.totalTax = totalTax;
	}

	public int getTvaId() {
		return this.tvaId;
	}

	public void setTvaId(int tvaId) {
		this.tvaId = tvaId;
	}

	public String getUnit() {
		return this.unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}