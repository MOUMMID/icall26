package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the t_domoprime_calculation database table.
 * 
 */
@Entity
@Table(name="t_domoprime_calculation")
@NamedQuery(name="TDomoprimeCalculation.findAll", query="SELECT t FROM TDomoprimeCalculation t")
public class TDomoprimeCalculation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="accepted_by_id")
	private int acceptedById;

	private String causes;

	@Column(name="class_id")
	private int classId;

	@Column(name="contract_id")
	private int contractId;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="customer_id")
	private int customerId;

	@Column(name="energy_id")
	private int energyId;

	private int id;

	private String isLast;

	@Column(name="margin_price")
	private BigDecimal marginPrice;

	@Column(name="meeting_id")
	private int meetingId;

	@Column(name="number_of_people")
	private int numberOfPeople;

	@Column(name="polluter_id")
	private int polluterId;

	@Column(name="purchasing_price")
	private BigDecimal purchasingPrice;

	private BigDecimal qmac;

	@Column(name="qmac_value")
	private BigDecimal qmacValue;

	@Column(name="region_id")
	private int regionId;

	private BigDecimal revenue;

	@Column(name="sector_id")
	private int sectorId;

	private String status;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	@Column(name="user_id")
	private int userId;

	@Column(name="zone_id")
	private int zoneId;

	public TDomoprimeCalculation() {
	}

	public int getAcceptedById() {
		return this.acceptedById;
	}

	public void setAcceptedById(int acceptedById) {
		this.acceptedById = acceptedById;
	}

	public String getCauses() {
		return this.causes;
	}

	public void setCauses(String causes) {
		this.causes = causes;
	}

	public int getClassId() {
		return this.classId;
	}

	public void setClassId(int classId) {
		this.classId = classId;
	}

	public int getContractId() {
		return this.contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getEnergyId() {
		return this.energyId;
	}

	public void setEnergyId(int energyId) {
		this.energyId = energyId;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsLast() {
		return this.isLast;
	}

	public void setIsLast(String isLast) {
		this.isLast = isLast;
	}

	public BigDecimal getMarginPrice() {
		return this.marginPrice;
	}

	public void setMarginPrice(BigDecimal marginPrice) {
		this.marginPrice = marginPrice;
	}

	public int getMeetingId() {
		return this.meetingId;
	}

	public void setMeetingId(int meetingId) {
		this.meetingId = meetingId;
	}

	public int getNumberOfPeople() {
		return this.numberOfPeople;
	}

	public void setNumberOfPeople(int numberOfPeople) {
		this.numberOfPeople = numberOfPeople;
	}

	public int getPolluterId() {
		return this.polluterId;
	}

	public void setPolluterId(int polluterId) {
		this.polluterId = polluterId;
	}

	public BigDecimal getPurchasingPrice() {
		return this.purchasingPrice;
	}

	public void setPurchasingPrice(BigDecimal purchasingPrice) {
		this.purchasingPrice = purchasingPrice;
	}

	public BigDecimal getQmac() {
		return this.qmac;
	}

	public void setQmac(BigDecimal qmac) {
		this.qmac = qmac;
	}

	public BigDecimal getQmacValue() {
		return this.qmacValue;
	}

	public void setQmacValue(BigDecimal qmacValue) {
		this.qmacValue = qmacValue;
	}

	public int getRegionId() {
		return this.regionId;
	}

	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}

	public BigDecimal getRevenue() {
		return this.revenue;
	}

	public void setRevenue(BigDecimal revenue) {
		this.revenue = revenue;
	}

	public int getSectorId() {
		return this.sectorId;
	}

	public void setSectorId(int sectorId) {
		this.sectorId = sectorId;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getZoneId() {
		return this.zoneId;
	}

	public void setZoneId(int zoneId) {
		this.zoneId = zoneId;
	}

}