package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the t_domoprime_class database table.
 * 
 */
@Entity
@Table(name="t_domoprime_class")
@NamedQuery(name="TDomoprimeClass.findAll", query="SELECT t FROM TDomoprimeClass t")
public class TDomoprimeClass implements Serializable {
	private static final long serialVersionUID = 1L;

	private BigDecimal coef;

	private int id;

	private BigDecimal multiple;

	@Column(name="multiple_floor")
	private BigDecimal multipleFloor;

	@Column(name="multiple_top")
	private BigDecimal multipleTop;

	@Column(name="multiple_wall")
	private BigDecimal multipleWall;

	private String name;

	public TDomoprimeClass() {
	}

	public BigDecimal getCoef() {
		return this.coef;
	}

	public void setCoef(BigDecimal coef) {
		this.coef = coef;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BigDecimal getMultiple() {
		return this.multiple;
	}

	public void setMultiple(BigDecimal multiple) {
		this.multiple = multiple;
	}

	public BigDecimal getMultipleFloor() {
		return this.multipleFloor;
	}

	public void setMultipleFloor(BigDecimal multipleFloor) {
		this.multipleFloor = multipleFloor;
	}

	public BigDecimal getMultipleTop() {
		return this.multipleTop;
	}

	public void setMultipleTop(BigDecimal multipleTop) {
		this.multipleTop = multipleTop;
	}

	public BigDecimal getMultipleWall() {
		return this.multipleWall;
	}

	public void setMultipleWall(BigDecimal multipleWall) {
		this.multipleWall = multipleWall;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}