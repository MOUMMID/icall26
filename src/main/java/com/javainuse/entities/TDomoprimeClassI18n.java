package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_domoprime_class_i18n database table.
 * 
 */
@Entity
@Table(name="t_domoprime_class_i18n")
@NamedQuery(name="TDomoprimeClassI18n.findAll", query="SELECT t FROM TDomoprimeClassI18n t")
public class TDomoprimeClassI18n implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="class_id")
	private int classId;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	private String lang;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	private String value;

	public TDomoprimeClassI18n() {
	}

	public int getClassId() {
		return this.classId;
	}

	public void setClassId(int classId) {
		this.classId = classId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLang() {
		return this.lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}