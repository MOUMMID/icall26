package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the t_domoprime_class_region_price database table.
 * 
 */
@Entity
@Table(name="t_domoprime_class_region_price")
@NamedQuery(name="TDomoprimeClassRegionPrice.findAll", query="SELECT t FROM TDomoprimeClassRegionPrice t")
public class TDomoprimeClassRegionPrice implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="class_id")
	private int classId;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	@Column(name="number_of_people")
	private int numberOfPeople;

	private BigDecimal price;

	@Column(name="region_id")
	private int regionId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TDomoprimeClassRegionPrice() {
	}

	public int getClassId() {
		return this.classId;
	}

	public void setClassId(int classId) {
		this.classId = classId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNumberOfPeople() {
		return this.numberOfPeople;
	}

	public void setNumberOfPeople(int numberOfPeople) {
		this.numberOfPeople = numberOfPeople;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public int getRegionId() {
		return this.regionId;
	}

	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}