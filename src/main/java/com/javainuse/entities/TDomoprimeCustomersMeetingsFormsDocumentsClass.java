package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_domoprime_customers_meetings_forms_documents_class database table.
 * 
 */
@Entity
@Table(name="t_domoprime_customers_meetings_forms_documents_class")
@NamedQuery(name="TDomoprimeCustomersMeetingsFormsDocumentsClass.findAll", query="SELECT t FROM TDomoprimeCustomersMeetingsFormsDocumentsClass t")
public class TDomoprimeCustomersMeetingsFormsDocumentsClass implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="class_id")
	private int classId;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="form_document_id")
	private int formDocumentId;

	private int id;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TDomoprimeCustomersMeetingsFormsDocumentsClass() {
	}

	public int getClassId() {
		return this.classId;
	}

	public void setClassId(int classId) {
		this.classId = classId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getFormDocumentId() {
		return this.formDocumentId;
	}

	public void setFormDocumentId(int formDocumentId) {
		this.formDocumentId = formDocumentId;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}