package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_domoprime_energy database table.
 * 
 */
@Entity
@Table(name="t_domoprime_energy")
@NamedQuery(name="TDomoprimeEnergy.findAll", query="SELECT t FROM TDomoprimeEnergy t")
public class TDomoprimeEnergy implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private String name;

	public TDomoprimeEnergy() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}