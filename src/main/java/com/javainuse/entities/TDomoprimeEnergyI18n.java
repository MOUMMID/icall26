package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_domoprime_energy_i18n database table.
 * 
 */
@Entity
@Table(name="t_domoprime_energy_i18n")
@NamedQuery(name="TDomoprimeEnergyI18n.findAll", query="SELECT t FROM TDomoprimeEnergyI18n t")
public class TDomoprimeEnergyI18n implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="energy_id")
	private int energyId;

	private int id;

	private String lang;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	private String value;

	public TDomoprimeEnergyI18n() {
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getEnergyId() {
		return this.energyId;
	}

	public void setEnergyId(int energyId) {
		this.energyId = energyId;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLang() {
		return this.lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}