package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the t_domoprime_polluter_class database table.
 * 
 */
@Entity
@Table(name="t_domoprime_polluter_class")
@NamedQuery(name="TDomoprimePolluterClass.findAll", query="SELECT t FROM TDomoprimePolluterClass t")
public class TDomoprimePolluterClass implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="class_id")
	private int classId;

	private BigDecimal coef;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	private BigDecimal multiple;

	@Column(name="multiple_floor")
	private BigDecimal multipleFloor;

	@Column(name="multiple_top")
	private BigDecimal multipleTop;

	@Column(name="multiple_wall")
	private BigDecimal multipleWall;

	@Column(name="polluter_id")
	private int polluterId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TDomoprimePolluterClass() {
	}

	public int getClassId() {
		return this.classId;
	}

	public void setClassId(int classId) {
		this.classId = classId;
	}

	public BigDecimal getCoef() {
		return this.coef;
	}

	public void setCoef(BigDecimal coef) {
		this.coef = coef;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BigDecimal getMultiple() {
		return this.multiple;
	}

	public void setMultiple(BigDecimal multiple) {
		this.multiple = multiple;
	}

	public BigDecimal getMultipleFloor() {
		return this.multipleFloor;
	}

	public void setMultipleFloor(BigDecimal multipleFloor) {
		this.multipleFloor = multipleFloor;
	}

	public BigDecimal getMultipleTop() {
		return this.multipleTop;
	}

	public void setMultipleTop(BigDecimal multipleTop) {
		this.multipleTop = multipleTop;
	}

	public BigDecimal getMultipleWall() {
		return this.multipleWall;
	}

	public void setMultipleWall(BigDecimal multipleWall) {
		this.multipleWall = multipleWall;
	}

	public int getPolluterId() {
		return this.polluterId;
	}

	public void setPolluterId(int polluterId) {
		this.polluterId = polluterId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}