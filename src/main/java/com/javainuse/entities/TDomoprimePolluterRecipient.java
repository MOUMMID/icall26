package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_domoprime_polluter_recipient database table.
 * 
 */
@Entity
@Table(name="t_domoprime_polluter_recipient")
@NamedQuery(name="TDomoprimePolluterRecipient.findAll", query="SELECT t FROM TDomoprimePolluterRecipient t")
public class TDomoprimePolluterRecipient implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	@Column(name="polluter_id")
	private int polluterId;

	@Column(name="recipient_id")
	private int recipientId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TDomoprimePolluterRecipient() {
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPolluterId() {
		return this.polluterId;
	}

	public void setPolluterId(int polluterId) {
		this.polluterId = polluterId;
	}

	public int getRecipientId() {
		return this.recipientId;
	}

	public void setRecipientId(int recipientId) {
		this.recipientId = recipientId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}