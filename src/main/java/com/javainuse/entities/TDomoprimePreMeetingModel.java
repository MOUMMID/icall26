package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_domoprime_pre_meeting_model database table.
 * 
 */
@Entity
@Table(name="t_domoprime_pre_meeting_model")
@NamedQuery(name="TDomoprimePreMeetingModel.findAll", query="SELECT t FROM TDomoprimePreMeetingModel t")
public class TDomoprimePreMeetingModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private String name;

	public TDomoprimePreMeetingModel() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}