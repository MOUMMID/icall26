package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_domoprime_pre_meeting_model_i18n database table.
 * 
 */
@Entity
@Table(name="t_domoprime_pre_meeting_model_i18n")
@NamedQuery(name="TDomoprimePreMeetingModelI18n.findAll", query="SELECT t FROM TDomoprimePreMeetingModelI18n t")
public class TDomoprimePreMeetingModelI18n implements Serializable {
	private static final long serialVersionUID = 1L;

	@Lob
	private String content;

	@Column(name="created_at")
	private Timestamp createdAt;

	private String file;

	private int id;

	private String lang;

	@Column(name="model_id")
	private int modelId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	private String value;

	@Lob
	private String variables;

	public TDomoprimePreMeetingModelI18n() {
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getFile() {
		return this.file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLang() {
		return this.lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public int getModelId() {
		return this.modelId;
	}

	public void setModelId(int modelId) {
		this.modelId = modelId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getVariables() {
		return this.variables;
	}

	public void setVariables(String variables) {
		this.variables = variables;
	}

}