package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the t_domoprime_product_calculation database table.
 * 
 */
@Entity
@Table(name="t_domoprime_product_calculation")
@NamedQuery(name="TDomoprimeProductCalculation.findAll", query="SELECT t FROM TDomoprimeProductCalculation t")
public class TDomoprimeProductCalculation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="calculation_id")
	private int calculationId;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	private BigDecimal margin;

	@Column(name="product_id")
	private int productId;

	@Column(name="purchasing_price")
	private BigDecimal purchasingPrice;

	private BigDecimal qmac;

	@Column(name="qmac_value")
	private BigDecimal qmacValue;

	private BigDecimal surface;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TDomoprimeProductCalculation() {
	}

	public int getCalculationId() {
		return this.calculationId;
	}

	public void setCalculationId(int calculationId) {
		this.calculationId = calculationId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BigDecimal getMargin() {
		return this.margin;
	}

	public void setMargin(BigDecimal margin) {
		this.margin = margin;
	}

	public int getProductId() {
		return this.productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public BigDecimal getPurchasingPrice() {
		return this.purchasingPrice;
	}

	public void setPurchasingPrice(BigDecimal purchasingPrice) {
		this.purchasingPrice = purchasingPrice;
	}

	public BigDecimal getQmac() {
		return this.qmac;
	}

	public void setQmac(BigDecimal qmac) {
		this.qmac = qmac;
	}

	public BigDecimal getQmacValue() {
		return this.qmacValue;
	}

	public void setQmacValue(BigDecimal qmacValue) {
		this.qmacValue = qmacValue;
	}

	public BigDecimal getSurface() {
		return this.surface;
	}

	public void setSurface(BigDecimal surface) {
		this.surface = surface;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}