package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the t_domoprime_product_sector_energy database table.
 * 
 */
@Entity
@Table(name="t_domoprime_product_sector_energy")
@NamedQuery(name="TDomoprimeProductSectorEnergy.findAll", query="SELECT t FROM TDomoprimeProductSectorEnergy t")
public class TDomoprimeProductSectorEnergy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="energy_id")
	private int energyId;

	private int id;

	private BigDecimal price;

	@Column(name="product_id")
	private int productId;

	@Column(name="sector_id")
	private int sectorId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TDomoprimeProductSectorEnergy() {
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getEnergyId() {
		return this.energyId;
	}

	public void setEnergyId(int energyId) {
		this.energyId = energyId;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public int getProductId() {
		return this.productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getSectorId() {
		return this.sectorId;
	}

	public void setSectorId(int sectorId) {
		this.sectorId = sectorId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}