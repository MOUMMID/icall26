package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the t_domoprime_quotation database table.
 * 
 */
@Entity
@Table(name="t_domoprime_quotation")
@NamedQuery(name="TDomoprimeQuotation.findAll", query="SELECT t FROM TDomoprimeQuotation t")
public class TDomoprimeQuotation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Lob
	private String comments;

	@Column(name="contract_id")
	private int contractId;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="creator_id")
	private int creatorId;

	@Column(name="customer_id")
	private int customerId;

	@Column(name="dated_at")
	private Timestamp datedAt;

	private String engine;

	@Column(name="fee_file")
	private BigDecimal feeFile;

	@Column(name="fixed_prime")
	private BigDecimal fixedPrime;

	@Lob
	private String footer;

	private String header;

	private int id;

	@Column(name="is_last")
	private String isLast;

	@Column(name="is_signed")
	private String isSigned;

	@Column(name="meeting_id")
	private int meetingId;

	private int month;

	@Column(name="number_of_children")
	private BigDecimal numberOfChildren;

	@Column(name="number_of_people")
	private BigDecimal numberOfPeople;

	@Column(name="one_euro")
	private String oneEuro;

	private BigDecimal prime;

	@Column(name="qmac_value")
	private BigDecimal qmacValue;

	private String reference;

	@Lob
	private String remarks;

	@Column(name="rest_in_charge")
	private BigDecimal restInCharge;

	@Column(name="rest_in_charge_after_credit")
	private BigDecimal restInChargeAfterCredit;

	@Column(name="signed_at")
	private Timestamp signedAt;

	private String status;

	@Column(name="status_id")
	private int statusId;

	@Column(name="tax_credit")
	private BigDecimal taxCredit;

	@Column(name="tax_credit_available")
	private BigDecimal taxCreditAvailable;

	@Column(name="tax_credit_limit")
	private BigDecimal taxCreditLimit;

	@Column(name="tax_credit_used")
	private BigDecimal taxCreditUsed;

	@Column(name="total_added_with_tax_floor")
	private BigDecimal totalAddedWithTaxFloor;

	@Column(name="total_added_with_tax_top")
	private BigDecimal totalAddedWithTaxTop;

	@Column(name="total_added_with_tax_wall")
	private BigDecimal totalAddedWithTaxWall;

	@Column(name="total_added_without_tax_floor")
	private BigDecimal totalAddedWithoutTaxFloor;

	@Column(name="total_added_without_tax_top")
	private BigDecimal totalAddedWithoutTaxTop;

	@Column(name="total_added_without_tax_wall")
	private BigDecimal totalAddedWithoutTaxWall;

	@Column(name="total_purchase_with_tax")
	private BigDecimal totalPurchaseWithTax;

	@Column(name="total_purchase_without_tax")
	private BigDecimal totalPurchaseWithoutTax;

	@Column(name="total_restincharge_with_tax_floor")
	private BigDecimal totalRestinchargeWithTaxFloor;

	@Column(name="total_restincharge_with_tax_top")
	private BigDecimal totalRestinchargeWithTaxTop;

	@Column(name="total_restincharge_with_tax_wall")
	private BigDecimal totalRestinchargeWithTaxWall;

	@Column(name="total_restincharge_without_tax_floor")
	private BigDecimal totalRestinchargeWithoutTaxFloor;

	@Column(name="total_restincharge_without_tax_top")
	private BigDecimal totalRestinchargeWithoutTaxTop;

	@Column(name="total_restincharge_without_tax_wall")
	private BigDecimal totalRestinchargeWithoutTaxWall;

	@Column(name="total_sale_101_with_tax")
	private BigDecimal totalSale101WithTax;

	@Column(name="total_sale_101_without_tax")
	private BigDecimal totalSale101WithoutTax;

	@Column(name="total_sale_102_with_tax")
	private BigDecimal totalSale102WithTax;

	@Column(name="total_sale_102_without_tax")
	private BigDecimal totalSale102WithoutTax;

	@Column(name="total_sale_103_with_tax")
	private BigDecimal totalSale103WithTax;

	@Column(name="total_sale_103_without_tax")
	private BigDecimal totalSale103WithoutTax;

	@Column(name="total_sale_discount_with_tax")
	private BigDecimal totalSaleDiscountWithTax;

	@Column(name="total_sale_discount_without_tax")
	private BigDecimal totalSaleDiscountWithoutTax;

	@Column(name="total_sale_with_tax")
	private BigDecimal totalSaleWithTax;

	@Column(name="total_sale_without_tax")
	private BigDecimal totalSaleWithoutTax;

	@Column(name="total_tax")
	private BigDecimal totalTax;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	private int year;

	public TDomoprimeQuotation() {
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getContractId() {
		return this.contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getCreatorId() {
		return this.creatorId;
	}

	public void setCreatorId(int creatorId) {
		this.creatorId = creatorId;
	}

	public int getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public Timestamp getDatedAt() {
		return this.datedAt;
	}

	public void setDatedAt(Timestamp datedAt) {
		this.datedAt = datedAt;
	}

	public String getEngine() {
		return this.engine;
	}

	public void setEngine(String engine) {
		this.engine = engine;
	}

	public BigDecimal getFeeFile() {
		return this.feeFile;
	}

	public void setFeeFile(BigDecimal feeFile) {
		this.feeFile = feeFile;
	}

	public BigDecimal getFixedPrime() {
		return this.fixedPrime;
	}

	public void setFixedPrime(BigDecimal fixedPrime) {
		this.fixedPrime = fixedPrime;
	}

	public String getFooter() {
		return this.footer;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}

	public String getHeader() {
		return this.header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsLast() {
		return this.isLast;
	}

	public void setIsLast(String isLast) {
		this.isLast = isLast;
	}

	public String getIsSigned() {
		return this.isSigned;
	}

	public void setIsSigned(String isSigned) {
		this.isSigned = isSigned;
	}

	public int getMeetingId() {
		return this.meetingId;
	}

	public void setMeetingId(int meetingId) {
		this.meetingId = meetingId;
	}

	public int getMonth() {
		return this.month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public BigDecimal getNumberOfChildren() {
		return this.numberOfChildren;
	}

	public void setNumberOfChildren(BigDecimal numberOfChildren) {
		this.numberOfChildren = numberOfChildren;
	}

	public BigDecimal getNumberOfPeople() {
		return this.numberOfPeople;
	}

	public void setNumberOfPeople(BigDecimal numberOfPeople) {
		this.numberOfPeople = numberOfPeople;
	}

	public String getOneEuro() {
		return this.oneEuro;
	}

	public void setOneEuro(String oneEuro) {
		this.oneEuro = oneEuro;
	}

	public BigDecimal getPrime() {
		return this.prime;
	}

	public void setPrime(BigDecimal prime) {
		this.prime = prime;
	}

	public BigDecimal getQmacValue() {
		return this.qmacValue;
	}

	public void setQmacValue(BigDecimal qmacValue) {
		this.qmacValue = qmacValue;
	}

	public String getReference() {
		return this.reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getRestInCharge() {
		return this.restInCharge;
	}

	public void setRestInCharge(BigDecimal restInCharge) {
		this.restInCharge = restInCharge;
	}

	public BigDecimal getRestInChargeAfterCredit() {
		return this.restInChargeAfterCredit;
	}

	public void setRestInChargeAfterCredit(BigDecimal restInChargeAfterCredit) {
		this.restInChargeAfterCredit = restInChargeAfterCredit;
	}

	public Timestamp getSignedAt() {
		return this.signedAt;
	}

	public void setSignedAt(Timestamp signedAt) {
		this.signedAt = signedAt;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getStatusId() {
		return this.statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public BigDecimal getTaxCredit() {
		return this.taxCredit;
	}

	public void setTaxCredit(BigDecimal taxCredit) {
		this.taxCredit = taxCredit;
	}

	public BigDecimal getTaxCreditAvailable() {
		return this.taxCreditAvailable;
	}

	public void setTaxCreditAvailable(BigDecimal taxCreditAvailable) {
		this.taxCreditAvailable = taxCreditAvailable;
	}

	public BigDecimal getTaxCreditLimit() {
		return this.taxCreditLimit;
	}

	public void setTaxCreditLimit(BigDecimal taxCreditLimit) {
		this.taxCreditLimit = taxCreditLimit;
	}

	public BigDecimal getTaxCreditUsed() {
		return this.taxCreditUsed;
	}

	public void setTaxCreditUsed(BigDecimal taxCreditUsed) {
		this.taxCreditUsed = taxCreditUsed;
	}

	public BigDecimal getTotalAddedWithTaxFloor() {
		return this.totalAddedWithTaxFloor;
	}

	public void setTotalAddedWithTaxFloor(BigDecimal totalAddedWithTaxFloor) {
		this.totalAddedWithTaxFloor = totalAddedWithTaxFloor;
	}

	public BigDecimal getTotalAddedWithTaxTop() {
		return this.totalAddedWithTaxTop;
	}

	public void setTotalAddedWithTaxTop(BigDecimal totalAddedWithTaxTop) {
		this.totalAddedWithTaxTop = totalAddedWithTaxTop;
	}

	public BigDecimal getTotalAddedWithTaxWall() {
		return this.totalAddedWithTaxWall;
	}

	public void setTotalAddedWithTaxWall(BigDecimal totalAddedWithTaxWall) {
		this.totalAddedWithTaxWall = totalAddedWithTaxWall;
	}

	public BigDecimal getTotalAddedWithoutTaxFloor() {
		return this.totalAddedWithoutTaxFloor;
	}

	public void setTotalAddedWithoutTaxFloor(BigDecimal totalAddedWithoutTaxFloor) {
		this.totalAddedWithoutTaxFloor = totalAddedWithoutTaxFloor;
	}

	public BigDecimal getTotalAddedWithoutTaxTop() {
		return this.totalAddedWithoutTaxTop;
	}

	public void setTotalAddedWithoutTaxTop(BigDecimal totalAddedWithoutTaxTop) {
		this.totalAddedWithoutTaxTop = totalAddedWithoutTaxTop;
	}

	public BigDecimal getTotalAddedWithoutTaxWall() {
		return this.totalAddedWithoutTaxWall;
	}

	public void setTotalAddedWithoutTaxWall(BigDecimal totalAddedWithoutTaxWall) {
		this.totalAddedWithoutTaxWall = totalAddedWithoutTaxWall;
	}

	public BigDecimal getTotalPurchaseWithTax() {
		return this.totalPurchaseWithTax;
	}

	public void setTotalPurchaseWithTax(BigDecimal totalPurchaseWithTax) {
		this.totalPurchaseWithTax = totalPurchaseWithTax;
	}

	public BigDecimal getTotalPurchaseWithoutTax() {
		return this.totalPurchaseWithoutTax;
	}

	public void setTotalPurchaseWithoutTax(BigDecimal totalPurchaseWithoutTax) {
		this.totalPurchaseWithoutTax = totalPurchaseWithoutTax;
	}

	public BigDecimal getTotalRestinchargeWithTaxFloor() {
		return this.totalRestinchargeWithTaxFloor;
	}

	public void setTotalRestinchargeWithTaxFloor(BigDecimal totalRestinchargeWithTaxFloor) {
		this.totalRestinchargeWithTaxFloor = totalRestinchargeWithTaxFloor;
	}

	public BigDecimal getTotalRestinchargeWithTaxTop() {
		return this.totalRestinchargeWithTaxTop;
	}

	public void setTotalRestinchargeWithTaxTop(BigDecimal totalRestinchargeWithTaxTop) {
		this.totalRestinchargeWithTaxTop = totalRestinchargeWithTaxTop;
	}

	public BigDecimal getTotalRestinchargeWithTaxWall() {
		return this.totalRestinchargeWithTaxWall;
	}

	public void setTotalRestinchargeWithTaxWall(BigDecimal totalRestinchargeWithTaxWall) {
		this.totalRestinchargeWithTaxWall = totalRestinchargeWithTaxWall;
	}

	public BigDecimal getTotalRestinchargeWithoutTaxFloor() {
		return this.totalRestinchargeWithoutTaxFloor;
	}

	public void setTotalRestinchargeWithoutTaxFloor(BigDecimal totalRestinchargeWithoutTaxFloor) {
		this.totalRestinchargeWithoutTaxFloor = totalRestinchargeWithoutTaxFloor;
	}

	public BigDecimal getTotalRestinchargeWithoutTaxTop() {
		return this.totalRestinchargeWithoutTaxTop;
	}

	public void setTotalRestinchargeWithoutTaxTop(BigDecimal totalRestinchargeWithoutTaxTop) {
		this.totalRestinchargeWithoutTaxTop = totalRestinchargeWithoutTaxTop;
	}

	public BigDecimal getTotalRestinchargeWithoutTaxWall() {
		return this.totalRestinchargeWithoutTaxWall;
	}

	public void setTotalRestinchargeWithoutTaxWall(BigDecimal totalRestinchargeWithoutTaxWall) {
		this.totalRestinchargeWithoutTaxWall = totalRestinchargeWithoutTaxWall;
	}

	public BigDecimal getTotalSale101WithTax() {
		return this.totalSale101WithTax;
	}

	public void setTotalSale101WithTax(BigDecimal totalSale101WithTax) {
		this.totalSale101WithTax = totalSale101WithTax;
	}

	public BigDecimal getTotalSale101WithoutTax() {
		return this.totalSale101WithoutTax;
	}

	public void setTotalSale101WithoutTax(BigDecimal totalSale101WithoutTax) {
		this.totalSale101WithoutTax = totalSale101WithoutTax;
	}

	public BigDecimal getTotalSale102WithTax() {
		return this.totalSale102WithTax;
	}

	public void setTotalSale102WithTax(BigDecimal totalSale102WithTax) {
		this.totalSale102WithTax = totalSale102WithTax;
	}

	public BigDecimal getTotalSale102WithoutTax() {
		return this.totalSale102WithoutTax;
	}

	public void setTotalSale102WithoutTax(BigDecimal totalSale102WithoutTax) {
		this.totalSale102WithoutTax = totalSale102WithoutTax;
	}

	public BigDecimal getTotalSale103WithTax() {
		return this.totalSale103WithTax;
	}

	public void setTotalSale103WithTax(BigDecimal totalSale103WithTax) {
		this.totalSale103WithTax = totalSale103WithTax;
	}

	public BigDecimal getTotalSale103WithoutTax() {
		return this.totalSale103WithoutTax;
	}

	public void setTotalSale103WithoutTax(BigDecimal totalSale103WithoutTax) {
		this.totalSale103WithoutTax = totalSale103WithoutTax;
	}

	public BigDecimal getTotalSaleDiscountWithTax() {
		return this.totalSaleDiscountWithTax;
	}

	public void setTotalSaleDiscountWithTax(BigDecimal totalSaleDiscountWithTax) {
		this.totalSaleDiscountWithTax = totalSaleDiscountWithTax;
	}

	public BigDecimal getTotalSaleDiscountWithoutTax() {
		return this.totalSaleDiscountWithoutTax;
	}

	public void setTotalSaleDiscountWithoutTax(BigDecimal totalSaleDiscountWithoutTax) {
		this.totalSaleDiscountWithoutTax = totalSaleDiscountWithoutTax;
	}

	public BigDecimal getTotalSaleWithTax() {
		return this.totalSaleWithTax;
	}

	public void setTotalSaleWithTax(BigDecimal totalSaleWithTax) {
		this.totalSaleWithTax = totalSaleWithTax;
	}

	public BigDecimal getTotalSaleWithoutTax() {
		return this.totalSaleWithoutTax;
	}

	public void setTotalSaleWithoutTax(BigDecimal totalSaleWithoutTax) {
		this.totalSaleWithoutTax = totalSaleWithoutTax;
	}

	public BigDecimal getTotalTax() {
		return this.totalTax;
	}

	public void setTotalTax(BigDecimal totalTax) {
		this.totalTax = totalTax;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public int getYear() {
		return this.year;
	}

	public void setYear(int year) {
		this.year = year;
	}

}