package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_domoprime_quotation_model database table.
 * 
 */
@Entity
@Table(name="t_domoprime_quotation_model")
@NamedQuery(name="TDomoprimeQuotationModel.findAll", query="SELECT t FROM TDomoprimeQuotationModel t")
public class TDomoprimeQuotationModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private String name;

	public TDomoprimeQuotationModel() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}