package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_domoprime_quotation_model_i18n database table.
 * 
 */
@Entity
@Table(name="t_domoprime_quotation_model_i18n")
@NamedQuery(name="TDomoprimeQuotationModelI18n.findAll", query="SELECT t FROM TDomoprimeQuotationModelI18n t")
public class TDomoprimeQuotationModelI18n implements Serializable {
	private static final long serialVersionUID = 1L;

	@Lob
	private String body;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	private String lang;

	@Column(name="model_id")
	private int modelId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	private String value;

	public TDomoprimeQuotationModelI18n() {
	}

	public String getBody() {
		return this.body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLang() {
		return this.lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public int getModelId() {
		return this.modelId;
	}

	public void setModelId(int modelId) {
		this.modelId = modelId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}