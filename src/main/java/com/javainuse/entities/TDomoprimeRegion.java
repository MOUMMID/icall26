package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_domoprime_region database table.
 * 
 */
@Entity
@Table(name="t_domoprime_region")
@NamedQuery(name="TDomoprimeRegion.findAll", query="SELECT t FROM TDomoprimeRegion t")
public class TDomoprimeRegion implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private String name;

	public TDomoprimeRegion() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}