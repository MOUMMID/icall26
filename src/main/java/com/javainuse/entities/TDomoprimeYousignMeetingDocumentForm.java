package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_domoprime_yousign_meeting_document_form database table.
 * 
 */
@Entity
@Table(name="t_domoprime_yousign_meeting_document_form")
@NamedQuery(name="TDomoprimeYousignMeetingDocumentForm.findAll", query="SELECT t FROM TDomoprimeYousignMeetingDocumentForm t")
public class TDomoprimeYousignMeetingDocumentForm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="contract_id")
	private int contractId;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="creator_id")
	private int creatorId;

	@Column(name="document_id")
	private int documentId;

	private int id;

	@Column(name="is_last")
	private String isLast;

	@Column(name="is_signed")
	private String isSigned;

	@Column(name="meeting_id")
	private int meetingId;

	@Column(name="sign_id")
	private int signId;

	@Column(name="signed_at")
	private Timestamp signedAt;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TDomoprimeYousignMeetingDocumentForm() {
	}

	public int getContractId() {
		return this.contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getCreatorId() {
		return this.creatorId;
	}

	public void setCreatorId(int creatorId) {
		this.creatorId = creatorId;
	}

	public int getDocumentId() {
		return this.documentId;
	}

	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsLast() {
		return this.isLast;
	}

	public void setIsLast(String isLast) {
		this.isLast = isLast;
	}

	public String getIsSigned() {
		return this.isSigned;
	}

	public void setIsSigned(String isSigned) {
		this.isSigned = isSigned;
	}

	public int getMeetingId() {
		return this.meetingId;
	}

	public void setMeetingId(int meetingId) {
		this.meetingId = meetingId;
	}

	public int getSignId() {
		return this.signId;
	}

	public void setSignId(int signId) {
		this.signId = signId;
	}

	public Timestamp getSignedAt() {
		return this.signedAt;
	}

	public void setSignedAt(Timestamp signedAt) {
		this.signedAt = signedAt;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}