package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_domoprime_yousign_quotation database table.
 * 
 */
@Entity
@Table(name="t_domoprime_yousign_quotation")
@NamedQuery(name="TDomoprimeYousignQuotation.findAll", query="SELECT t FROM TDomoprimeYousignQuotation t")
public class TDomoprimeYousignQuotation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="contract_id")
	private int contractId;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="creator_id")
	private int creatorId;

	private int id;

	@Column(name="is_last")
	private String isLast;

	@Column(name="meeting_id")
	private int meetingId;

	@Column(name="quotation_id")
	private int quotationId;

	@Column(name="sign_id")
	private int signId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TDomoprimeYousignQuotation() {
	}

	public int getContractId() {
		return this.contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getCreatorId() {
		return this.creatorId;
	}

	public void setCreatorId(int creatorId) {
		this.creatorId = creatorId;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsLast() {
		return this.isLast;
	}

	public void setIsLast(String isLast) {
		this.isLast = isLast;
	}

	public int getMeetingId() {
		return this.meetingId;
	}

	public void setMeetingId(int meetingId) {
		this.meetingId = meetingId;
	}

	public int getQuotationId() {
		return this.quotationId;
	}

	public void setQuotationId(int quotationId) {
		this.quotationId = quotationId;
	}

	public int getSignId() {
		return this.signId;
	}

	public void setSignId(int signId) {
		this.signId = signId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}