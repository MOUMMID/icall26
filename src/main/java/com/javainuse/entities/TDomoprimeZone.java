package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_domoprime_zone database table.
 * 
 */
@Entity
@Table(name="t_domoprime_zone")
@NamedQuery(name="TDomoprimeZone.findAll", query="SELECT t FROM TDomoprimeZone t")
public class TDomoprimeZone implements Serializable {
	private static final long serialVersionUID = 1L;

	private String code;

	@Column(name="created_at")
	private Timestamp createdAt;

	private String dept;

	private int id;

	@Column(name="region_id")
	private int regionId;

	private String sector;

	@Column(name="sector_id")
	private int sectorId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TDomoprimeZone() {
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getDept() {
		return this.dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRegionId() {
		return this.regionId;
	}

	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}

	public String getSector() {
		return this.sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public int getSectorId() {
		return this.sectorId;
	}

	public void setSectorId(int sectorId) {
		this.sectorId = sectorId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}