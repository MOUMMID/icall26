package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_forgot_password database table.
 * 
 */
@Entity
@Table(name="t_forgot_password")
@NamedQuery(name="TForgotPassword.findAll", query="SELECT t FROM TForgotPassword t")
public class TForgotPassword implements Serializable {
	private static final long serialVersionUID = 1L;

	private String key;

	private String password;

	private Timestamp time;

	@Column(name="user_id")
	private int userId;

	public TForgotPassword() {
	}

	public String getKey() {
		return this.key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}