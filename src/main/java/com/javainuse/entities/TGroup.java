package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_groups database table.
 * 
 */
@Entity
@Table(name="t_groups")
@NamedQuery(name="TGroup.findAll", query="SELECT t FROM TGroup t")
public class TGroup implements Serializable {
	private static final long serialVersionUID = 1L;

	private String application;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	@Column(name="is_active")
	private String isActive;

	private String name;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TGroup() {
	}

	public String getApplication() {
		return this.application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}