package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_group_permission database table.
 * 
 */
@Entity
@Table(name="t_group_permission")
@NamedQuery(name="TGroupPermission.findAll", query="SELECT t FROM TGroupPermission t")
public class TGroupPermission implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="group_id")
	private int groupId;

	private int id;

	@Column(name="permission_id")
	private int permissionId;

	public TGroupPermission() {
	}

	public int getGroupId() {
		return this.groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPermissionId() {
		return this.permissionId;
	}

	public void setPermissionId(int permissionId) {
		this.permissionId = permissionId;
	}

}