package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the t_icall26_map_api_address database table.
 * 
 */
@Entity
@Table(name="t_icall26_map_api_address")
@NamedQuery(name="TIcall26MapApiAddress.findAll", query="SELECT t FROM TIcall26MapApiAddress t")
public class TIcall26MapApiAddress implements Serializable {
	private static final long serialVersionUID = 1L;

	private String address;

	@Column(name="created_at")
	private Timestamp createdAt;

	private String error;

	private int id;

	private BigDecimal lat;

	private BigDecimal lng;

	private String signature;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TIcall26MapApiAddress() {
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getError() {
		return this.error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BigDecimal getLat() {
		return this.lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return this.lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}

	public String getSignature() {
		return this.signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}