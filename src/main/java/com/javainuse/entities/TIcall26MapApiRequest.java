package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_icall26_map_api_request database table.
 * 
 */
@Entity
@Table(name="t_icall26_map_api_request")
@NamedQuery(name="TIcall26MapApiRequest.findAll", query="SELECT t FROM TIcall26MapApiRequest t")
public class TIcall26MapApiRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="api_address_id")
	private int apiAddressId;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	private String key;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TIcall26MapApiRequest() {
	}

	public int getApiAddressId() {
		return this.apiAddressId;
	}

	public void setApiAddressId(int apiAddressId) {
		this.apiAddressId = apiAddressId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKey() {
		return this.key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}