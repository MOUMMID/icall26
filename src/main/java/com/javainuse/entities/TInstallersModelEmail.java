package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_installers_model_email database table.
 * 
 */
@Entity
@Table(name="t_installers_model_email")
@NamedQuery(name="TInstallersModelEmail.findAll", query="SELECT t FROM TInstallersModelEmail t")
public class TInstallersModelEmail implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private String name;

	public TInstallersModelEmail() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}