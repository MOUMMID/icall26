package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_languages database table.
 * 
 */
@Entity
@Table(name="t_languages")
@NamedQuery(name="TLanguage.findAll", query="SELECT t FROM TLanguage t")
public class TLanguage implements Serializable {
	private static final long serialVersionUID = 1L;

	private String application;

	private String code;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	@Column(name="is_active")
	private String isActive;

	private short position;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TLanguage() {
	}

	public String getApplication() {
		return this.application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public short getPosition() {
		return this.position;
	}

	public void setPosition(short position) {
		this.position = position;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}