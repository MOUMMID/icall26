package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the t_marketing_leads_wp_forms database table.
 * 
 */
@Entity
@Table(name="t_marketing_leads_wp_forms")
@NamedQuery(name="TMarketingLeadsWpForm.findAll", query="SELECT t FROM TMarketingLeadsWpForm t")
public class TMarketingLeadsWpForm implements Serializable {
	private static final long serialVersionUID = 1L;

	private String address;

	private String city;

	private String country;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="duplicate_wpf")
	private String duplicateWpf;

	private String email;

	private String energy;

	private String firstname;

	private int id;

	@Column(name="id_wp")
	private int idWp;

	private BigDecimal income;

	@Column(name="is_active")
	private String isActive;

	@Column(name="is_duplicate")
	private String isDuplicate;

	private String lastname;

	@Column(name="number_of_people")
	private byte numberOfPeople;

	private String owner;

	private String phone;

	@Column(name="phone_status")
	private String phoneStatus;

	private int postcode;

	private String referrer;

	@Column(name="site_id")
	private int siteId;

	private String state;

	@Column(name="state_id")
	private int stateId;

	private String status;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	@Column(name="utm_campaign")
	private String utmCampaign;

	@Column(name="utm_medium")
	private String utmMedium;

	@Column(name="utm_source")
	private String utmSource;

	@Column(name="wp_created_at")
	private Timestamp wpCreatedAt;

	private String zone;

	public TMarketingLeadsWpForm() {
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getDuplicateWpf() {
		return this.duplicateWpf;
	}

	public void setDuplicateWpf(String duplicateWpf) {
		this.duplicateWpf = duplicateWpf;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEnergy() {
		return this.energy;
	}

	public void setEnergy(String energy) {
		this.energy = energy;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdWp() {
		return this.idWp;
	}

	public void setIdWp(int idWp) {
		this.idWp = idWp;
	}

	public BigDecimal getIncome() {
		return this.income;
	}

	public void setIncome(BigDecimal income) {
		this.income = income;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getIsDuplicate() {
		return this.isDuplicate;
	}

	public void setIsDuplicate(String isDuplicate) {
		this.isDuplicate = isDuplicate;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public byte getNumberOfPeople() {
		return this.numberOfPeople;
	}

	public void setNumberOfPeople(byte numberOfPeople) {
		this.numberOfPeople = numberOfPeople;
	}

	public String getOwner() {
		return this.owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhoneStatus() {
		return this.phoneStatus;
	}

	public void setPhoneStatus(String phoneStatus) {
		this.phoneStatus = phoneStatus;
	}

	public int getPostcode() {
		return this.postcode;
	}

	public void setPostcode(int postcode) {
		this.postcode = postcode;
	}

	public String getReferrer() {
		return this.referrer;
	}

	public void setReferrer(String referrer) {
		this.referrer = referrer;
	}

	public int getSiteId() {
		return this.siteId;
	}

	public void setSiteId(int siteId) {
		this.siteId = siteId;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getStateId() {
		return this.stateId;
	}

	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUtmCampaign() {
		return this.utmCampaign;
	}

	public void setUtmCampaign(String utmCampaign) {
		this.utmCampaign = utmCampaign;
	}

	public String getUtmMedium() {
		return this.utmMedium;
	}

	public void setUtmMedium(String utmMedium) {
		this.utmMedium = utmMedium;
	}

	public String getUtmSource() {
		return this.utmSource;
	}

	public void setUtmSource(String utmSource) {
		this.utmSource = utmSource;
	}

	public Timestamp getWpCreatedAt() {
		return this.wpCreatedAt;
	}

	public void setWpCreatedAt(Timestamp wpCreatedAt) {
		this.wpCreatedAt = wpCreatedAt;
	}

	public String getZone() {
		return this.zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

}