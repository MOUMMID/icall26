package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_marketing_leads_wp_forms_status database table.
 * 
 */
@Entity
@Table(name="t_marketing_leads_wp_forms_status")
@NamedQuery(name="TMarketingLeadsWpFormsStatus.findAll", query="SELECT t FROM TMarketingLeadsWpFormsStatus t")
public class TMarketingLeadsWpFormsStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	private String color;

	private String icon;

	private int id;

	private String name;

	public TMarketingLeadsWpFormsStatus() {
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getIcon() {
		return this.icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}