package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_marketing_leads_wp_landing_page_site database table.
 * 
 */
@Entity
@Table(name="t_marketing_leads_wp_landing_page_site")
@NamedQuery(name="TMarketingLeadsWpLandingPageSite.findAll", query="SELECT t FROM TMarketingLeadsWpLandingPageSite t")
public class TMarketingLeadsWpLandingPageSite implements Serializable {
	private static final long serialVersionUID = 1L;

	private String campaign;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="host_db")
	private String hostDb;

	@Column(name="host_site")
	private String hostSite;

	private int id;

	@Column(name="is_active")
	private String isActive;

	@Column(name="name_db")
	private String nameDb;

	@Column(name="password_db")
	private String passwordDb;

	private String status;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	@Column(name="user_db")
	private String userDb;

	public TMarketingLeadsWpLandingPageSite() {
	}

	public String getCampaign() {
		return this.campaign;
	}

	public void setCampaign(String campaign) {
		this.campaign = campaign;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getHostDb() {
		return this.hostDb;
	}

	public void setHostDb(String hostDb) {
		this.hostDb = hostDb;
	}

	public String getHostSite() {
		return this.hostSite;
	}

	public void setHostSite(String hostSite) {
		this.hostSite = hostSite;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getNameDb() {
		return this.nameDb;
	}

	public void setNameDb(String nameDb) {
		this.nameDb = nameDb;
	}

	public String getPasswordDb() {
		return this.passwordDb;
	}

	public void setPasswordDb(String passwordDb) {
		this.passwordDb = passwordDb;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUserDb() {
		return this.userDb;
	}

	public void setUserDb(String userDb) {
		this.userDb = userDb;
	}

}