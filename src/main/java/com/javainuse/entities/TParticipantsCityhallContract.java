package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_participants_cityhall_contract database table.
 * 
 */
@Entity
@Table(name="t_participants_cityhall_contract")
@NamedQuery(name="TParticipantsCityhallContract.findAll", query="SELECT t FROM TParticipantsCityhallContract t")
public class TParticipantsCityhallContract implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="ack_at")
	private Timestamp ackAt;

	@Column(name="contract_id")
	private int contractId;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	@Lob
	private String remarks;

	@Column(name="resend_at")
	private Timestamp resendAt;

	@Column(name="send_at")
	private Timestamp sendAt;

	@Column(name="state_at")
	private Timestamp stateAt;

	@Column(name="status_id")
	private int statusId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TParticipantsCityhallContract() {
	}

	public Timestamp getAckAt() {
		return this.ackAt;
	}

	public void setAckAt(Timestamp ackAt) {
		this.ackAt = ackAt;
	}

	public int getContractId() {
		return this.contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Timestamp getResendAt() {
		return this.resendAt;
	}

	public void setResendAt(Timestamp resendAt) {
		this.resendAt = resendAt;
	}

	public Timestamp getSendAt() {
		return this.sendAt;
	}

	public void setSendAt(Timestamp sendAt) {
		this.sendAt = sendAt;
	}

	public Timestamp getStateAt() {
		return this.stateAt;
	}

	public void setStateAt(Timestamp stateAt) {
		this.stateAt = stateAt;
	}

	public int getStatusId() {
		return this.statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}