package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_participants_cityhall_status database table.
 * 
 */
@Entity
@Table(name="t_participants_cityhall_status")
@NamedQuery(name="TParticipantsCityhallStatus.findAll", query="SELECT t FROM TParticipantsCityhallStatus t")
public class TParticipantsCityhallStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private String name;

	public TParticipantsCityhallStatus() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}