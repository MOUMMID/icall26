package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_participants_consuel_contract database table.
 * 
 */
@Entity
@Table(name="t_participants_consuel_contract")
@NamedQuery(name="TParticipantsConsuelContract.findAll", query="SELECT t FROM TParticipantsConsuelContract t")
public class TParticipantsConsuelContract implements Serializable {
	private static final long serialVersionUID = 1L;

	@Lob
	private String conformity;

	@Column(name="contract_id")
	private int contractId;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	@Column(name="installer_id")
	private int installerId;

	@Column(name="modified_at")
	private Timestamp modifiedAt;

	@Lob
	private String remarks;

	@Column(name="revisited_at")
	private Timestamp revisitedAt;

	@Column(name="send_at")
	private Timestamp sendAt;

	@Column(name="status_id")
	private int statusId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	@Column(name="visited_at")
	private Timestamp visitedAt;

	@Column(name="work_before")
	private String workBefore;

	public TParticipantsConsuelContract() {
	}

	public String getConformity() {
		return this.conformity;
	}

	public void setConformity(String conformity) {
		this.conformity = conformity;
	}

	public int getContractId() {
		return this.contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getInstallerId() {
		return this.installerId;
	}

	public void setInstallerId(int installerId) {
		this.installerId = installerId;
	}

	public Timestamp getModifiedAt() {
		return this.modifiedAt;
	}

	public void setModifiedAt(Timestamp modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Timestamp getRevisitedAt() {
		return this.revisitedAt;
	}

	public void setRevisitedAt(Timestamp revisitedAt) {
		this.revisitedAt = revisitedAt;
	}

	public Timestamp getSendAt() {
		return this.sendAt;
	}

	public void setSendAt(Timestamp sendAt) {
		this.sendAt = sendAt;
	}

	public int getStatusId() {
		return this.statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Timestamp getVisitedAt() {
		return this.visitedAt;
	}

	public void setVisitedAt(Timestamp visitedAt) {
		this.visitedAt = visitedAt;
	}

	public String getWorkBefore() {
		return this.workBefore;
	}

	public void setWorkBefore(String workBefore) {
		this.workBefore = workBefore;
	}

}