package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_participants_consuel_model database table.
 * 
 */
@Entity
@Table(name="t_participants_consuel_model")
@NamedQuery(name="TParticipantsConsuelModel.findAll", query="SELECT t FROM TParticipantsConsuelModel t")
public class TParticipantsConsuelModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private String extension;

	private int id;

	private String name;

	public TParticipantsConsuelModel() {
	}

	public String getExtension() {
		return this.extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}