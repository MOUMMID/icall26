package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_participants_consuel_model_i18n database table.
 * 
 */
@Entity
@Table(name="t_participants_consuel_model_i18n")
@NamedQuery(name="TParticipantsConsuelModelI18n.findAll", query="SELECT t FROM TParticipantsConsuelModelI18n t")
public class TParticipantsConsuelModelI18n implements Serializable {
	private static final long serialVersionUID = 1L;

	@Lob
	private String content;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	private String lang;

	@Column(name="model_id")
	private int modelId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	private String value;

	public TParticipantsConsuelModelI18n() {
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLang() {
		return this.lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public int getModelId() {
		return this.modelId;
	}

	public void setModelId(int modelId) {
		this.modelId = modelId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}