package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_participants_consuel_status database table.
 * 
 */
@Entity
@Table(name="t_participants_consuel_status")
@NamedQuery(name="TParticipantsConsuelStatus.findAll", query="SELECT t FROM TParticipantsConsuelStatus t")
public class TParticipantsConsuelStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private String name;

	public TParticipantsConsuelStatus() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}