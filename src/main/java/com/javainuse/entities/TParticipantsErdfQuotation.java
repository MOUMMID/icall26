package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the t_participants_erdf_quotation database table.
 * 
 */
@Entity
@Table(name="t_participants_erdf_quotation")
@NamedQuery(name="TParticipantsErdfQuotation.findAll", query="SELECT t FROM TParticipantsErdfQuotation t")
public class TParticipantsErdfQuotation implements Serializable {
	private static final long serialVersionUID = 1L;

	private BigDecimal amount;

	@Column(name="check_amount")
	private BigDecimal checkAmount;

	@Column(name="check_at")
	private Timestamp checkAt;

	@Column(name="contract_id")
	private int contractId;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	@Column(name="opened_at")
	private Timestamp openedAt;

	@Column(name="received_at")
	private Timestamp receivedAt;

	@Lob
	private String remarks;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TParticipantsErdfQuotation() {
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getCheckAmount() {
		return this.checkAmount;
	}

	public void setCheckAmount(BigDecimal checkAmount) {
		this.checkAmount = checkAmount;
	}

	public Timestamp getCheckAt() {
		return this.checkAt;
	}

	public void setCheckAt(Timestamp checkAt) {
		this.checkAt = checkAt;
	}

	public int getContractId() {
		return this.contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getOpenedAt() {
		return this.openedAt;
	}

	public void setOpenedAt(Timestamp openedAt) {
		this.openedAt = openedAt;
	}

	public Timestamp getReceivedAt() {
		return this.receivedAt;
	}

	public void setReceivedAt(Timestamp receivedAt) {
		this.receivedAt = receivedAt;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}