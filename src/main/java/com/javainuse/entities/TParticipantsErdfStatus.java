package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_participants_erdf_status database table.
 * 
 */
@Entity
@Table(name="t_participants_erdf_status")
@NamedQuery(name="TParticipantsErdfStatus.findAll", query="SELECT t FROM TParticipantsErdfStatus t")
public class TParticipantsErdfStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private String name;

	public TParticipantsErdfStatus() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}