package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_participants_installation_contract database table.
 * 
 */
@Entity
@Table(name="t_participants_installation_contract")
@NamedQuery(name="TParticipantsInstallationContract.findAll", query="SELECT t FROM TParticipantsInstallationContract t")
public class TParticipantsInstallationContract implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="contract_id")
	private int contractId;

	@Column(name="counter_at")
	private Timestamp counterAt;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	@Column(name="installer_id")
	private int installerId;

	@Column(name="linked_at")
	private Timestamp linkedAt;

	private String type;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	@Column(name="worked_at")
	private Timestamp workedAt;

	public TParticipantsInstallationContract() {
	}

	public int getContractId() {
		return this.contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public Timestamp getCounterAt() {
		return this.counterAt;
	}

	public void setCounterAt(Timestamp counterAt) {
		this.counterAt = counterAt;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getInstallerId() {
		return this.installerId;
	}

	public void setInstallerId(int installerId) {
		this.installerId = installerId;
	}

	public Timestamp getLinkedAt() {
		return this.linkedAt;
	}

	public void setLinkedAt(Timestamp linkedAt) {
		this.linkedAt = linkedAt;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Timestamp getWorkedAt() {
		return this.workedAt;
	}

	public void setWorkedAt(Timestamp workedAt) {
		this.workedAt = workedAt;
	}

}