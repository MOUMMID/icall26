package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_partner_polluter_document database table.
 * 
 */
@Entity
@Table(name="t_partner_polluter_document")
@NamedQuery(name="TPartnerPolluterDocument.findAll", query="SELECT t FROM TPartnerPolluterDocument t")
public class TPartnerPolluterDocument implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="document_id")
	private int documentId;

	private int id;

	@Column(name="model_id")
	private int modelId;

	@Column(name="polluter_id")
	private int polluterId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TPartnerPolluterDocument() {
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getDocumentId() {
		return this.documentId;
	}

	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getModelId() {
		return this.modelId;
	}

	public void setModelId(int modelId) {
		this.modelId = modelId;
	}

	public int getPolluterId() {
		return this.polluterId;
	}

	public void setPolluterId(int polluterId) {
		this.polluterId = polluterId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}