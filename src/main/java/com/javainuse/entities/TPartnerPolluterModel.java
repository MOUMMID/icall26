package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_partner_polluter_model database table.
 * 
 */
@Entity
@Table(name="t_partner_polluter_model")
@NamedQuery(name="TPartnerPolluterModel.findAll", query="SELECT t FROM TPartnerPolluterModel t")
public class TPartnerPolluterModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="created_at")
	private Timestamp createdAt;

	private String extension;

	private int id;

	private String name;

	@Column(name="polluter_id")
	private int polluterId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TPartnerPolluterModel() {
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getExtension() {
		return this.extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPolluterId() {
		return this.polluterId;
	}

	public void setPolluterId(int polluterId) {
		this.polluterId = polluterId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}