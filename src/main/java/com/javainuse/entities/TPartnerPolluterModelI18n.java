package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_partner_polluter_model_i18n database table.
 * 
 */
@Entity
@Table(name="t_partner_polluter_model_i18n")
@NamedQuery(name="TPartnerPolluterModelI18n.findAll", query="SELECT t FROM TPartnerPolluterModelI18n t")
public class TPartnerPolluterModelI18n implements Serializable {
	private static final long serialVersionUID = 1L;

	@Lob
	private String comments;

	@Lob
	private String content;

	@Column(name="created_at")
	private Timestamp createdAt;

	private String file;

	private int id;

	@Column(name="initiator_signature")
	private String initiatorSignature;

	private String lang;

	@Column(name="model_id")
	private int modelId;

	private String signature;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	private String value;

	@Lob
	private String variables;

	public TPartnerPolluterModelI18n() {
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getFile() {
		return this.file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getInitiatorSignature() {
		return this.initiatorSignature;
	}

	public void setInitiatorSignature(String initiatorSignature) {
		this.initiatorSignature = initiatorSignature;
	}

	public String getLang() {
		return this.lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public int getModelId() {
		return this.modelId;
	}

	public void setModelId(int modelId) {
		this.modelId = modelId;
	}

	public String getSignature() {
		return this.signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getVariables() {
		return this.variables;
	}

	public void setVariables(String variables) {
		this.variables = variables;
	}

}