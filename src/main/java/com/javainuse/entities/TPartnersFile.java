package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_partners_files database table.
 * 
 */
@Entity
@Table(name="t_partners_files")
@NamedQuery(name="TPartnersFile.findAll", query="SELECT t FROM TPartnersFile t")
public class TPartnersFile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="company_id")
	private int companyId;

	@Column(name="created_at")
	private Timestamp createdAt;

	private String file;

	private int id;

	private String status;

	private String title;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TPartnersFile() {
	}

	public int getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getFile() {
		return this.file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}