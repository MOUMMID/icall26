package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_partners_model_email database table.
 * 
 */
@Entity
@Table(name="t_partners_model_email")
@NamedQuery(name="TPartnersModelEmail.findAll", query="SELECT t FROM TPartnersModelEmail t")
public class TPartnersModelEmail implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private String name;

	public TPartnersModelEmail() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}