package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_permissions database table.
 * 
 */
@Entity
@Table(name="t_permissions")
@NamedQuery(name="TPermission.findAll", query="SELECT t FROM TPermission t")
public class TPermission implements Serializable {
	private static final long serialVersionUID = 1L;

	private String application;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="group_id")
	private int groupId;

	private int id;

	private String name;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TPermission() {
	}

	public String getApplication() {
		return this.application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getGroupId() {
		return this.groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}