package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_permission_group database table.
 * 
 */
@Entity
@Table(name="t_permission_group")
@NamedQuery(name="TPermissionGroup.findAll", query="SELECT t FROM TPermissionGroup t")
public class TPermissionGroup implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private String name;

	public TPermissionGroup() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}