package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the t_products database table.
 * 
 */
@Entity
@Table(name="t_products")
@NamedQuery(name="TProduct.findAll", query="SELECT t FROM TProduct t")
public class TProduct implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="action_id")
	private int actionId;

	@Lob
	private String content;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="discount_price")
	private BigDecimal discountPrice;

	private String icon;

	private int id;

	@Column(name="is_active")
	private String isActive;

	@Column(name="is_billable")
	private String isBillable;

	@Column(name="is_consomable")
	private String isConsomable;

	@Column(name="is_monthly")
	private String isMonthly;

	@Column(name="meta_description")
	private String metaDescription;

	@Column(name="meta_keywords")
	private String metaKeywords;

	@Column(name="meta_robots")
	private String metaRobots;

	@Column(name="meta_title")
	private String metaTitle;

	private String picture;

	private BigDecimal price;

	@Column(name="prime_price")
	private BigDecimal primePrice;

	@Column(name="purchasing_price")
	private BigDecimal purchasingPrice;

	private String reference;

	@Column(name="standard_price")
	private BigDecimal standardPrice;

	private String status;

	@Column(name="tva_id")
	private int tvaId;

	private String unit;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	private String url;

	public TProduct() {
	}

	public int getActionId() {
		return this.actionId;
	}

	public void setActionId(int actionId) {
		this.actionId = actionId;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public BigDecimal getDiscountPrice() {
		return this.discountPrice;
	}

	public void setDiscountPrice(BigDecimal discountPrice) {
		this.discountPrice = discountPrice;
	}

	public String getIcon() {
		return this.icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getIsBillable() {
		return this.isBillable;
	}

	public void setIsBillable(String isBillable) {
		this.isBillable = isBillable;
	}

	public String getIsConsomable() {
		return this.isConsomable;
	}

	public void setIsConsomable(String isConsomable) {
		this.isConsomable = isConsomable;
	}

	public String getIsMonthly() {
		return this.isMonthly;
	}

	public void setIsMonthly(String isMonthly) {
		this.isMonthly = isMonthly;
	}

	public String getMetaDescription() {
		return this.metaDescription;
	}

	public void setMetaDescription(String metaDescription) {
		this.metaDescription = metaDescription;
	}

	public String getMetaKeywords() {
		return this.metaKeywords;
	}

	public void setMetaKeywords(String metaKeywords) {
		this.metaKeywords = metaKeywords;
	}

	public String getMetaRobots() {
		return this.metaRobots;
	}

	public void setMetaRobots(String metaRobots) {
		this.metaRobots = metaRobots;
	}

	public String getMetaTitle() {
		return this.metaTitle;
	}

	public void setMetaTitle(String metaTitle) {
		this.metaTitle = metaTitle;
	}

	public String getPicture() {
		return this.picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getPrimePrice() {
		return this.primePrice;
	}

	public void setPrimePrice(BigDecimal primePrice) {
		this.primePrice = primePrice;
	}

	public BigDecimal getPurchasingPrice() {
		return this.purchasingPrice;
	}

	public void setPurchasingPrice(BigDecimal purchasingPrice) {
		this.purchasingPrice = purchasingPrice;
	}

	public String getReference() {
		return this.reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public BigDecimal getStandardPrice() {
		return this.standardPrice;
	}

	public void setStandardPrice(BigDecimal standardPrice) {
		this.standardPrice = standardPrice;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getTvaId() {
		return this.tvaId;
	}

	public void setTvaId(int tvaId) {
		this.tvaId = tvaId;
	}

	public String getUnit() {
		return this.unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}