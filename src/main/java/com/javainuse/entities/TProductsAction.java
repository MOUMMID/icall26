package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_products_action database table.
 * 
 */
@Entity
@Table(name="t_products_action")
@NamedQuery(name="TProductsAction.findAll", query="SELECT t FROM TProductsAction t")
public class TProductsAction implements Serializable {
	private static final long serialVersionUID = 1L;

	private String action;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	private String title;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TProductsAction() {
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}