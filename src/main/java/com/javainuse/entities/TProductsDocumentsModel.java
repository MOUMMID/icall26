package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_products_documents_model database table.
 * 
 */
@Entity
@Table(name="t_products_documents_model")
@NamedQuery(name="TProductsDocumentsModel.findAll", query="SELECT t FROM TProductsDocumentsModel t")
public class TProductsDocumentsModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private String extension;

	private int id;

	private String name;

	public TProductsDocumentsModel() {
	}

	public String getExtension() {
		return this.extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}