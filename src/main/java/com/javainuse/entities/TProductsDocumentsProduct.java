package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_products_documents_product database table.
 * 
 */
@Entity
@Table(name="t_products_documents_product")
@NamedQuery(name="TProductsDocumentsProduct.findAll", query="SELECT t FROM TProductsDocumentsProduct t")
public class TProductsDocumentsProduct implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	@Column(name="model_id")
	private int modelId;

	@Column(name="product_id")
	private int productId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TProductsDocumentsProduct() {
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getModelId() {
		return this.modelId;
	}

	public void setModelId(int modelId) {
		this.modelId = modelId;
	}

	public int getProductId() {
		return this.productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}