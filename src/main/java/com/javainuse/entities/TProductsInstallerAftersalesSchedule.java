package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the t_products_installer_aftersales_schedule database table.
 * 
 */
@Entity
@Table(name="t_products_installer_aftersales_schedule")
@NamedQuery(name="TProductsInstallerAftersalesSchedule.findAll", query="SELECT t FROM TProductsInstallerAftersalesSchedule t")
public class TProductsInstallerAftersalesSchedule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Lob
	private String details;

	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="in_at")
	private Date inAt;

	@Column(name="installation_id")
	private int installationId;

	@Column(name="installer_id")
	private int installerId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="out_at")
	private Date outAt;

	@Column(name="state_id")
	private int stateId;

	private String status;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TProductsInstallerAftersalesSchedule() {
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getDetails() {
		return this.details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getInAt() {
		return this.inAt;
	}

	public void setInAt(Date inAt) {
		this.inAt = inAt;
	}

	public int getInstallationId() {
		return this.installationId;
	}

	public void setInstallationId(int installationId) {
		this.installationId = installationId;
	}

	public int getInstallerId() {
		return this.installerId;
	}

	public void setInstallerId(int installerId) {
		this.installerId = installerId;
	}

	public Date getOutAt() {
		return this.outAt;
	}

	public void setOutAt(Date outAt) {
		this.outAt = outAt;
	}

	public int getStateId() {
		return this.stateId;
	}

	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}