package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_products_installer_aftersales_schedule_comments database table.
 * 
 */
@Entity
@Table(name="t_products_installer_aftersales_schedule_comments")
@NamedQuery(name="TProductsInstallerAftersalesScheduleComment.findAll", query="SELECT t FROM TProductsInstallerAftersalesScheduleComment t")
public class TProductsInstallerAftersalesScheduleComment implements Serializable {
	private static final long serialVersionUID = 1L;

	private String comment;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	@Column(name="schedule_id")
	private int scheduleId;

	private String status;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TProductsInstallerAftersalesScheduleComment() {
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getScheduleId() {
		return this.scheduleId;
	}

	public void setScheduleId(int scheduleId) {
		this.scheduleId = scheduleId;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}