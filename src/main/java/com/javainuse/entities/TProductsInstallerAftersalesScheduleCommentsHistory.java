package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_products_installer_aftersales_schedule_comments_history database table.
 * 
 */
@Entity
@Table(name="t_products_installer_aftersales_schedule_comments_history")
@NamedQuery(name="TProductsInstallerAftersalesScheduleCommentsHistory.findAll", query="SELECT t FROM TProductsInstallerAftersalesScheduleCommentsHistory t")
public class TProductsInstallerAftersalesScheduleCommentsHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="comment_id")
	private int commentId;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	@Column(name="user_application")
	private String userApplication;

	@Column(name="user_id")
	private int userId;

	public TProductsInstallerAftersalesScheduleCommentsHistory() {
	}

	public int getCommentId() {
		return this.commentId;
	}

	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUserApplication() {
		return this.userApplication;
	}

	public void setUserApplication(String userApplication) {
		this.userApplication = userApplication;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}