package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_products_installer_contact_user database table.
 * 
 */
@Entity
@Table(name="t_products_installer_contact_user")
@NamedQuery(name="TProductsInstallerContactUser.findAll", query="SELECT t FROM TProductsInstallerContactUser t")
public class TProductsInstallerContactUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	@Column(name="installer_contact_id")
	private int installerContactId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	@Column(name="user_id")
	private int userId;

	public TProductsInstallerContactUser() {
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getInstallerContactId() {
		return this.installerContactId;
	}

	public void setInstallerContactId(int installerContactId) {
		this.installerContactId = installerContactId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}