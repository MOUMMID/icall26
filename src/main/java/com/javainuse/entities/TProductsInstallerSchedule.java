package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the t_products_installer_schedule database table.
 * 
 */
@Entity
@Table(name="t_products_installer_schedule")
@NamedQuery(name="TProductsInstallerSchedule.findAll", query="SELECT t FROM TProductsInstallerSchedule t")
public class TProductsInstallerSchedule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="contract_id")
	private int contractId;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Lob
	private String details;

	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="in_at")
	private Date inAt;

	@Column(name="installer_id")
	private int installerId;

	@Column(name="item_id")
	private int itemId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="out_at")
	private Date outAt;

	@Column(name="product_id")
	private int productId;

	@Column(name="range_id")
	private int rangeId;

	private String status;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TProductsInstallerSchedule() {
	}

	public int getContractId() {
		return this.contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getDetails() {
		return this.details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getInAt() {
		return this.inAt;
	}

	public void setInAt(Date inAt) {
		this.inAt = inAt;
	}

	public int getInstallerId() {
		return this.installerId;
	}

	public void setInstallerId(int installerId) {
		this.installerId = installerId;
	}

	public int getItemId() {
		return this.itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public Date getOutAt() {
		return this.outAt;
	}

	public void setOutAt(Date outAt) {
		this.outAt = outAt;
	}

	public int getProductId() {
		return this.productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getRangeId() {
		return this.rangeId;
	}

	public void setRangeId(int rangeId) {
		this.rangeId = rangeId;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}