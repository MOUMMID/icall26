package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_products_installer_schedule_document_model database table.
 * 
 */
@Entity
@Table(name="t_products_installer_schedule_document_model")
@NamedQuery(name="TProductsInstallerScheduleDocumentModel.findAll", query="SELECT t FROM TProductsInstallerScheduleDocumentModel t")
public class TProductsInstallerScheduleDocumentModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private String name;

	public TProductsInstallerScheduleDocumentModel() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}