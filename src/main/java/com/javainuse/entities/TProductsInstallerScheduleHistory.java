package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_products_installer_schedule_history database table.
 * 
 */
@Entity
@Table(name="t_products_installer_schedule_history")
@NamedQuery(name="TProductsInstallerScheduleHistory.findAll", query="SELECT t FROM TProductsInstallerScheduleHistory t")
public class TProductsInstallerScheduleHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="created_at")
	private Timestamp createdAt;

	private String history;

	private int id;

	@Column(name="schedule_id")
	private int scheduleId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	@Column(name="user_application")
	private String userApplication;

	@Column(name="user_id")
	private int userId;

	public TProductsInstallerScheduleHistory() {
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getHistory() {
		return this.history;
	}

	public void setHistory(String history) {
		this.history = history;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getScheduleId() {
		return this.scheduleId;
	}

	public void setScheduleId(int scheduleId) {
		this.scheduleId = scheduleId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUserApplication() {
		return this.userApplication;
	}

	public void setUserApplication(String userApplication) {
		this.userApplication = userApplication;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}