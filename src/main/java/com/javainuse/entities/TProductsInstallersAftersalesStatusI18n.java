package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_products_installers_aftersales_status_i18n database table.
 * 
 */
@Entity
@Table(name="t_products_installers_aftersales_status_i18n")
@NamedQuery(name="TProductsInstallersAftersalesStatusI18n.findAll", query="SELECT t FROM TProductsInstallersAftersalesStatusI18n t")
public class TProductsInstallersAftersalesStatusI18n implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	private String lang;

	@Column(name="status_id")
	private int statusId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	private String value;

	public TProductsInstallersAftersalesStatusI18n() {
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLang() {
		return this.lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public int getStatusId() {
		return this.statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}