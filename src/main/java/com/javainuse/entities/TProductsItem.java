package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the t_products_item database table.
 * 
 */
@Entity
@Table(name="t_products_item")
@NamedQuery(name="TProductsItem.findAll", query="SELECT t FROM TProductsItem t")
public class TProductsItem implements Serializable {
	private static final long serialVersionUID = 1L;

	private BigDecimal coefficient;

	@Lob
	private String content;

	@Column(name="created_at")
	private Timestamp createdAt;

	private String description;

	@Lob
	private String details;

	@Column(name="discount_price")
	private BigDecimal discountPrice;

	private String icon;

	private int id;

	private String input1;

	private String input2;

	private String input3;

	@Column(name="is_active")
	private String isActive;

	@Column(name="is_mandatory")
	private String isMandatory;

	private String mark;

	private String picture;

	@Column(name="product_id")
	private int productId;

	@Column(name="purchasing_price")
	private BigDecimal purchasingPrice;

	private String reference;

	@Column(name="sale_price")
	private BigDecimal salePrice;

	private String status;

	private BigDecimal thickness;

	@Column(name="tva_id")
	private int tvaId;

	private String unit;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TProductsItem() {
	}

	public BigDecimal getCoefficient() {
		return this.coefficient;
	}

	public void setCoefficient(BigDecimal coefficient) {
		this.coefficient = coefficient;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDetails() {
		return this.details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public BigDecimal getDiscountPrice() {
		return this.discountPrice;
	}

	public void setDiscountPrice(BigDecimal discountPrice) {
		this.discountPrice = discountPrice;
	}

	public String getIcon() {
		return this.icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getInput1() {
		return this.input1;
	}

	public void setInput1(String input1) {
		this.input1 = input1;
	}

	public String getInput2() {
		return this.input2;
	}

	public void setInput2(String input2) {
		this.input2 = input2;
	}

	public String getInput3() {
		return this.input3;
	}

	public void setInput3(String input3) {
		this.input3 = input3;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getIsMandatory() {
		return this.isMandatory;
	}

	public void setIsMandatory(String isMandatory) {
		this.isMandatory = isMandatory;
	}

	public String getMark() {
		return this.mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public String getPicture() {
		return this.picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public int getProductId() {
		return this.productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public BigDecimal getPurchasingPrice() {
		return this.purchasingPrice;
	}

	public void setPurchasingPrice(BigDecimal purchasingPrice) {
		this.purchasingPrice = purchasingPrice;
	}

	public String getReference() {
		return this.reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public BigDecimal getSalePrice() {
		return this.salePrice;
	}

	public void setSalePrice(BigDecimal salePrice) {
		this.salePrice = salePrice;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getThickness() {
		return this.thickness;
	}

	public void setThickness(BigDecimal thickness) {
		this.thickness = thickness;
	}

	public int getTvaId() {
		return this.tvaId;
	}

	public void setTvaId(int tvaId) {
		this.tvaId = tvaId;
	}

	public String getUnit() {
		return this.unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}