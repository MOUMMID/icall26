package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_server_messages_users database table.
 * 
 */
@Entity
@Table(name="t_server_messages_users")
@NamedQuery(name="TServerMessagesUser.findAll", query="SELECT t FROM TServerMessagesUser t")
public class TServerMessagesUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	@Column(name="is_seen")
	private String isSeen;

	@Column(name="is_shown")
	private String isShown;

	@Column(name="message_number")
	private int messageNumber;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	@Column(name="user_id")
	private int userId;

	public TServerMessagesUser() {
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsSeen() {
		return this.isSeen;
	}

	public void setIsSeen(String isSeen) {
		this.isSeen = isSeen;
	}

	public String getIsShown() {
		return this.isShown;
	}

	public void setIsShown(String isShown) {
		this.isShown = isShown;
	}

	public int getMessageNumber() {
		return this.messageNumber;
	}

	public void setMessageNumber(int messageNumber) {
		this.messageNumber = messageNumber;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}