package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the t_service_impot_income database table.
 * 
 */
@Entity
@Table(name="t_service_impot_income")
@NamedQuery(name="TServiceImpotIncome.findAll", query="SELECT t FROM TServiceImpotIncome t")
public class TServiceImpotIncome implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="class_id")
	private int classId;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	@Column(name="income_french_island")
	private BigDecimal incomeFrenchIsland;

	@Column(name="income_other")
	private BigDecimal incomeOther;

	@Column(name="is_active")
	private String isActive;

	@Column(name="number_of_people")
	private int numberOfPeople;

	private String status;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TServiceImpotIncome() {
	}

	public int getClassId() {
		return this.classId;
	}

	public void setClassId(int classId) {
		this.classId = classId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BigDecimal getIncomeFrenchIsland() {
		return this.incomeFrenchIsland;
	}

	public void setIncomeFrenchIsland(BigDecimal incomeFrenchIsland) {
		this.incomeFrenchIsland = incomeFrenchIsland;
	}

	public BigDecimal getIncomeOther() {
		return this.incomeOther;
	}

	public void setIncomeOther(BigDecimal incomeOther) {
		this.incomeOther = incomeOther;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public int getNumberOfPeople() {
		return this.numberOfPeople;
	}

	public void setNumberOfPeople(int numberOfPeople) {
		this.numberOfPeople = numberOfPeople;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}