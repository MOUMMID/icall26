package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_service_impot_income_class database table.
 * 
 */
@Entity
@Table(name="t_service_impot_income_class")
@NamedQuery(name="TServiceImpotIncomeClass.findAll", query="SELECT t FROM TServiceImpotIncomeClass t")
public class TServiceImpotIncomeClass implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	@Column(name="is_active")
	private String isActive;

	@Lob
	private String name;

	private String status;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TServiceImpotIncomeClass() {
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}