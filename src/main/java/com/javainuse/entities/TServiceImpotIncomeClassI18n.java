package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_service_impot_income_class_i18n database table.
 * 
 */
@Entity
@Table(name="t_service_impot_income_class_i18n")
@NamedQuery(name="TServiceImpotIncomeClassI18n.findAll", query="SELECT t FROM TServiceImpotIncomeClassI18n t")
public class TServiceImpotIncomeClassI18n implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="class_id")
	private int classId;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	@Column(name="is_active")
	private String isActive;

	private String lang;

	private String status;

	@Lob
	private String text;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TServiceImpotIncomeClassI18n() {
	}

	public int getClassId() {
		return this.classId;
	}

	public void setClassId(int classId) {
		this.classId = classId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getLang() {
		return this.lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}