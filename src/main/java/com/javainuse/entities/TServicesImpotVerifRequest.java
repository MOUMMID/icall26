package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the t_services_impot_verif_request database table.
 * 
 */
@Entity
@Table(name="t_services_impot_verif_request")
@NamedQuery(name="TServicesImpotVerifRequest.findAll", query="SELECT t FROM TServicesImpotVerifRequest t")
public class TServicesImpotVerifRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="brut_revenue")
	private BigDecimal brutRevenue;

	@Column(name="created_at")
	private Timestamp createdAt;

	private String date;

	@Column(name="date_recover")
	private String dateRecover;

	@Column(name="declarant1_address1")
	private String declarant1Address1;

	@Column(name="declarant1_address2")
	private String declarant1Address2;

	@Column(name="declarant1_address3")
	private String declarant1Address3;

	@Column(name="declarant1_born_at")
	private String declarant1BornAt;

	@Column(name="declarant1_born_name")
	private String declarant1BornName;

	@Column(name="declarant1_city")
	private String declarant1City;

	@Column(name="declarant1_firstname")
	private String declarant1Firstname;

	@Column(name="declarant1_lastname")
	private String declarant1Lastname;

	@Column(name="declarant1_postcode")
	private String declarant1Postcode;

	@Column(name="declarant2_address1")
	private String declarant2Address1;

	@Column(name="declarant2_address2")
	private String declarant2Address2;

	@Column(name="declarant2_address3")
	private String declarant2Address3;

	@Column(name="declarant2_born_at")
	private String declarant2BornAt;

	@Column(name="declarant2_born_name")
	private String declarant2BornName;

	@Column(name="declarant2_city")
	private String declarant2City;

	@Column(name="declarant2_firstname")
	private String declarant2Firstname;

	@Column(name="declarant2_lastname")
	private String declarant2Lastname;

	@Column(name="declarant2_postcode")
	private String declarant2Postcode;

	@Column(name="family_situation")
	private String familySituation;

	private String file;

	@Column(name="fiscal_revenue_reference")
	private BigDecimal fiscalRevenueReference;

	@Column(name="has_error")
	private String hasError;

	private int id;

	@Column(name="income_amount")
	private BigDecimal incomeAmount;

	@Column(name="income_before_corrections")
	private BigDecimal incomeBeforeCorrections;

	@Column(name="is_loaded")
	private String isLoaded;

	private String number;

	@Column(name="number_of_part")
	private BigDecimal numberOfPart;

	@Column(name="number_of_people_incharge")
	private BigDecimal numberOfPeopleIncharge;

	private String picture;

	private String reference;

	private BigDecimal revenue;

	private String signature;

	private String status;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TServicesImpotVerifRequest() {
	}

	public BigDecimal getBrutRevenue() {
		return this.brutRevenue;
	}

	public void setBrutRevenue(BigDecimal brutRevenue) {
		this.brutRevenue = brutRevenue;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDateRecover() {
		return this.dateRecover;
	}

	public void setDateRecover(String dateRecover) {
		this.dateRecover = dateRecover;
	}

	public String getDeclarant1Address1() {
		return this.declarant1Address1;
	}

	public void setDeclarant1Address1(String declarant1Address1) {
		this.declarant1Address1 = declarant1Address1;
	}

	public String getDeclarant1Address2() {
		return this.declarant1Address2;
	}

	public void setDeclarant1Address2(String declarant1Address2) {
		this.declarant1Address2 = declarant1Address2;
	}

	public String getDeclarant1Address3() {
		return this.declarant1Address3;
	}

	public void setDeclarant1Address3(String declarant1Address3) {
		this.declarant1Address3 = declarant1Address3;
	}

	public String getDeclarant1BornAt() {
		return this.declarant1BornAt;
	}

	public void setDeclarant1BornAt(String declarant1BornAt) {
		this.declarant1BornAt = declarant1BornAt;
	}

	public String getDeclarant1BornName() {
		return this.declarant1BornName;
	}

	public void setDeclarant1BornName(String declarant1BornName) {
		this.declarant1BornName = declarant1BornName;
	}

	public String getDeclarant1City() {
		return this.declarant1City;
	}

	public void setDeclarant1City(String declarant1City) {
		this.declarant1City = declarant1City;
	}

	public String getDeclarant1Firstname() {
		return this.declarant1Firstname;
	}

	public void setDeclarant1Firstname(String declarant1Firstname) {
		this.declarant1Firstname = declarant1Firstname;
	}

	public String getDeclarant1Lastname() {
		return this.declarant1Lastname;
	}

	public void setDeclarant1Lastname(String declarant1Lastname) {
		this.declarant1Lastname = declarant1Lastname;
	}

	public String getDeclarant1Postcode() {
		return this.declarant1Postcode;
	}

	public void setDeclarant1Postcode(String declarant1Postcode) {
		this.declarant1Postcode = declarant1Postcode;
	}

	public String getDeclarant2Address1() {
		return this.declarant2Address1;
	}

	public void setDeclarant2Address1(String declarant2Address1) {
		this.declarant2Address1 = declarant2Address1;
	}

	public String getDeclarant2Address2() {
		return this.declarant2Address2;
	}

	public void setDeclarant2Address2(String declarant2Address2) {
		this.declarant2Address2 = declarant2Address2;
	}

	public String getDeclarant2Address3() {
		return this.declarant2Address3;
	}

	public void setDeclarant2Address3(String declarant2Address3) {
		this.declarant2Address3 = declarant2Address3;
	}

	public String getDeclarant2BornAt() {
		return this.declarant2BornAt;
	}

	public void setDeclarant2BornAt(String declarant2BornAt) {
		this.declarant2BornAt = declarant2BornAt;
	}

	public String getDeclarant2BornName() {
		return this.declarant2BornName;
	}

	public void setDeclarant2BornName(String declarant2BornName) {
		this.declarant2BornName = declarant2BornName;
	}

	public String getDeclarant2City() {
		return this.declarant2City;
	}

	public void setDeclarant2City(String declarant2City) {
		this.declarant2City = declarant2City;
	}

	public String getDeclarant2Firstname() {
		return this.declarant2Firstname;
	}

	public void setDeclarant2Firstname(String declarant2Firstname) {
		this.declarant2Firstname = declarant2Firstname;
	}

	public String getDeclarant2Lastname() {
		return this.declarant2Lastname;
	}

	public void setDeclarant2Lastname(String declarant2Lastname) {
		this.declarant2Lastname = declarant2Lastname;
	}

	public String getDeclarant2Postcode() {
		return this.declarant2Postcode;
	}

	public void setDeclarant2Postcode(String declarant2Postcode) {
		this.declarant2Postcode = declarant2Postcode;
	}

	public String getFamilySituation() {
		return this.familySituation;
	}

	public void setFamilySituation(String familySituation) {
		this.familySituation = familySituation;
	}

	public String getFile() {
		return this.file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public BigDecimal getFiscalRevenueReference() {
		return this.fiscalRevenueReference;
	}

	public void setFiscalRevenueReference(BigDecimal fiscalRevenueReference) {
		this.fiscalRevenueReference = fiscalRevenueReference;
	}

	public String getHasError() {
		return this.hasError;
	}

	public void setHasError(String hasError) {
		this.hasError = hasError;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BigDecimal getIncomeAmount() {
		return this.incomeAmount;
	}

	public void setIncomeAmount(BigDecimal incomeAmount) {
		this.incomeAmount = incomeAmount;
	}

	public BigDecimal getIncomeBeforeCorrections() {
		return this.incomeBeforeCorrections;
	}

	public void setIncomeBeforeCorrections(BigDecimal incomeBeforeCorrections) {
		this.incomeBeforeCorrections = incomeBeforeCorrections;
	}

	public String getIsLoaded() {
		return this.isLoaded;
	}

	public void setIsLoaded(String isLoaded) {
		this.isLoaded = isLoaded;
	}

	public String getNumber() {
		return this.number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public BigDecimal getNumberOfPart() {
		return this.numberOfPart;
	}

	public void setNumberOfPart(BigDecimal numberOfPart) {
		this.numberOfPart = numberOfPart;
	}

	public BigDecimal getNumberOfPeopleIncharge() {
		return this.numberOfPeopleIncharge;
	}

	public void setNumberOfPeopleIncharge(BigDecimal numberOfPeopleIncharge) {
		this.numberOfPeopleIncharge = numberOfPeopleIncharge;
	}

	public String getPicture() {
		return this.picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getReference() {
		return this.reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public BigDecimal getRevenue() {
		return this.revenue;
	}

	public void setRevenue(BigDecimal revenue) {
		this.revenue = revenue;
	}

	public String getSignature() {
		return this.signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}