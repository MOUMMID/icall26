package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_services_sms_box_sent database table.
 * 
 */
@Entity
@Table(name="t_services_sms_box_sent")
@NamedQuery(name="TServicesSmsBoxSent.findAll", query="SELECT t FROM TServicesSmsBoxSent t")
public class TServicesSmsBoxSent implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Lob
	@Column(name="error_api")
	private String errorApi;

	@Lob
	@Column(name="error_callback")
	private String errorCallback;

	@Lob
	private String errors;

	private int id;

	@Column(name="is_received")
	private String isReceived;

	@Column(name="is_sent")
	private String isSent;

	@Lob
	private String message;

	private String mobile;

	@Lob
	private String parameters;

	private String reference;

	@Column(name="sent_at")
	private Timestamp sentAt;

	public TServicesSmsBoxSent() {
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getErrorApi() {
		return this.errorApi;
	}

	public void setErrorApi(String errorApi) {
		this.errorApi = errorApi;
	}

	public String getErrorCallback() {
		return this.errorCallback;
	}

	public void setErrorCallback(String errorCallback) {
		this.errorCallback = errorCallback;
	}

	public String getErrors() {
		return this.errors;
	}

	public void setErrors(String errors) {
		this.errors = errors;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsReceived() {
		return this.isReceived;
	}

	public void setIsReceived(String isReceived) {
		this.isReceived = isReceived;
	}

	public String getIsSent() {
		return this.isSent;
	}

	public void setIsSent(String isSent) {
		this.isSent = isSent;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getParameters() {
		return this.parameters;
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}

	public String getReference() {
		return this.reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Timestamp getSentAt() {
		return this.sentAt;
	}

	public void setSentAt(Timestamp sentAt) {
		this.sentAt = sentAt;
	}

}