package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_services_yousign_file database table.
 * 
 */
@Entity
@Table(name="t_services_yousign_file")
@NamedQuery(name="TServicesYousignFile.findAll", query="SELECT t FROM TServicesYousignFile t")
public class TServicesYousignFile implements Serializable {
	private static final long serialVersionUID = 1L;

	private int batch;

	private String crc;

	@Column(name="created_at")
	private Timestamp createdAt;

	private String email;

	private String errors;

	private String filename;

	private String firstname;

	private int id;

	@Column(name="id_file")
	private int idFile;

	@Column(name="id_request")
	private int idRequest;

	@Column(name="is_loaded")
	private String isLoaded;

	@Column(name="is_signed")
	private String isSigned;

	private String lastname;

	private String phone;

	@Column(name="signed_at")
	private Timestamp signedAt;

	private String state;

	private String status;

	private String token;

	private String type;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TServicesYousignFile() {
	}

	public int getBatch() {
		return this.batch;
	}

	public void setBatch(int batch) {
		this.batch = batch;
	}

	public String getCrc() {
		return this.crc;
	}

	public void setCrc(String crc) {
		this.crc = crc;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getErrors() {
		return this.errors;
	}

	public void setErrors(String errors) {
		this.errors = errors;
	}

	public String getFilename() {
		return this.filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdFile() {
		return this.idFile;
	}

	public void setIdFile(int idFile) {
		this.idFile = idFile;
	}

	public int getIdRequest() {
		return this.idRequest;
	}

	public void setIdRequest(int idRequest) {
		this.idRequest = idRequest;
	}

	public String getIsLoaded() {
		return this.isLoaded;
	}

	public void setIsLoaded(String isLoaded) {
		this.isLoaded = isLoaded;
	}

	public String getIsSigned() {
		return this.isSigned;
	}

	public void setIsSigned(String isSigned) {
		this.isSigned = isSigned;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Timestamp getSignedAt() {
		return this.signedAt;
	}

	public void setSignedAt(Timestamp signedAt) {
		this.signedAt = signedAt;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getToken() {
		return this.token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}