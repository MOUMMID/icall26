package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_sessions database table.
 * 
 */
@Entity
@Table(name="t_sessions")
@NamedQuery(name="TSession.findAll", query="SELECT t FROM TSession t")
public class TSession implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private String ip;

	@Column(name="is_last")
	private String isLast;

	@Column(name="last_time")
	private Timestamp lastTime;

	private String session;

	@Column(name="start_time")
	private Timestamp startTime;

	@Column(name="user_id")
	private int userId;

	public TSession() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getIsLast() {
		return this.isLast;
	}

	public void setIsLast(String isLast) {
		this.isLast = isLast;
	}

	public Timestamp getLastTime() {
		return this.lastTime;
	}

	public void setLastTime(Timestamp lastTime) {
		this.lastTime = lastTime;
	}

	public String getSession() {
		return this.session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public Timestamp getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}