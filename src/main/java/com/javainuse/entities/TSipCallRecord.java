package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_sip_call_records database table.
 * 
 */
@Entity
@Table(name="t_sip_call_records")
@NamedQuery(name="TSipCallRecord.findAll", query="SELECT t FROM TSipCallRecord t")
public class TSipCallRecord implements Serializable {
	private static final long serialVersionUID = 1L;

	private String account;

	@Column(name="account_id")
	private int accountId;

	private String agent;

	private String cld;

	private String cli;

	@Column(name="connect_time")
	private int connectTime;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="customer_id")
	private int customerId;

	@Column(name="disconnect_time")
	private int disconnectTime;

	private int duration;

	private String file;

	private int id;

	@Column(name="is_downloaded")
	private String isDownloaded;

	private String module;

	private String name;

	private String operator;

	@Column(name="phone_in")
	private String phoneIn;

	@Column(name="phone_out")
	private String phoneOut;

	private String record;

	private String recorder;

	private String server;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	private String url;

	@Column(name="xdr_id")
	private int xdrId;

	public TSipCallRecord() {
	}

	public String getAccount() {
		return this.account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public int getAccountId() {
		return this.accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public String getAgent() {
		return this.agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getCld() {
		return this.cld;
	}

	public void setCld(String cld) {
		this.cld = cld;
	}

	public String getCli() {
		return this.cli;
	}

	public void setCli(String cli) {
		this.cli = cli;
	}

	public int getConnectTime() {
		return this.connectTime;
	}

	public void setConnectTime(int connectTime) {
		this.connectTime = connectTime;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getDisconnectTime() {
		return this.disconnectTime;
	}

	public void setDisconnectTime(int disconnectTime) {
		this.disconnectTime = disconnectTime;
	}

	public int getDuration() {
		return this.duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getFile() {
		return this.file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsDownloaded() {
		return this.isDownloaded;
	}

	public void setIsDownloaded(String isDownloaded) {
		this.isDownloaded = isDownloaded;
	}

	public String getModule() {
		return this.module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOperator() {
		return this.operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getPhoneIn() {
		return this.phoneIn;
	}

	public void setPhoneIn(String phoneIn) {
		this.phoneIn = phoneIn;
	}

	public String getPhoneOut() {
		return this.phoneOut;
	}

	public void setPhoneOut(String phoneOut) {
		this.phoneOut = phoneOut;
	}

	public String getRecord() {
		return this.record;
	}

	public void setRecord(String record) {
		this.record = record;
	}

	public String getRecorder() {
		return this.recorder;
	}

	public void setRecorder(String recorder) {
		this.recorder = recorder;
	}

	public String getServer() {
		return this.server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getXdrId() {
		return this.xdrId;
	}

	public void setXdrId(int xdrId) {
		this.xdrId = xdrId;
	}

}