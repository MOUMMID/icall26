package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_sip_goautodialk26_server database table.
 * 
 */
@Entity
@Table(name="t_sip_goautodialk26_server")
@NamedQuery(name="TSipGoautodialk26Server.findAll", query="SELECT t FROM TSipGoautodialk26Server t")
public class TSipGoautodialk26Server implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="created_at")
	private Timestamp createdAt;

	private String host;

	private int id;

	@Column(name="is_active")
	private String isActive;

	private String login;

	private String name;

	private String password;

	private String ping;

	private String recorder;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TSipGoautodialk26Server() {
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getHost() {
		return this.host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPing() {
		return this.ping;
	}

	public void setPing(String ping) {
		this.ping = ping;
	}

	public String getRecorder() {
		return this.recorder;
	}

	public void setRecorder(String recorder) {
		this.recorder = recorder;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}