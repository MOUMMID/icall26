package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the t_site_archive_checks database table.
 * 
 */
@Entity
@Table(name="t_site_archive_checks")
@NamedQuery(name="TSiteArchiveCheck.findAll", query="SELECT t FROM TSiteArchiveCheck t")
public class TSiteArchiveCheck implements Serializable {
	private static final long serialVersionUID = 1L;

	private BigDecimal amount;

	private String bank;

	private String billed;

	@Column(name="created_at")
	private Timestamp createdAt;

	private Timestamp date;

	private int id;

	private String number;

	private String reference;

	@Lob
	private String remarks;

	private String report;

	private String status;

	private String type;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	private String user;

	@Column(name="user_id")
	private int userId;

	public TSiteArchiveCheck() {
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getBank() {
		return this.bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getBilled() {
		return this.billed;
	}

	public void setBilled(String billed) {
		this.billed = billed;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public Timestamp getDate() {
		return this.date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNumber() {
		return this.number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getReference() {
		return this.reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getReport() {
		return this.report;
	}

	public void setReport(String report) {
		this.report = report;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUser() {
		return this.user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}