package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_site_ip_blacklist database table.
 * 
 */
@Entity
@Table(name="t_site_ip_blacklist")
@NamedQuery(name="TSiteIpBlacklist.findAll", query="SELECT t FROM TSiteIpBlacklist t")
public class TSiteIpBlacklist implements Serializable {
	private static final long serialVersionUID = 1L;

	private String application;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="deleted_at")
	private Timestamp deletedAt;

	@Column(name="deleted_by")
	private int deletedBy;

	private int id;

	private String ip;

	@Column(name="number_of_fails")
	private int numberOfFails;

	private String status;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TSiteIpBlacklist() {
	}

	public String getApplication() {
		return this.application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public Timestamp getDeletedAt() {
		return this.deletedAt;
	}

	public void setDeletedAt(Timestamp deletedAt) {
		this.deletedAt = deletedAt;
	}

	public int getDeletedBy() {
		return this.deletedBy;
	}

	public void setDeletedBy(int deletedBy) {
		this.deletedBy = deletedBy;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getNumberOfFails() {
		return this.numberOfFails;
	}

	public void setNumberOfFails(int numberOfFails) {
		this.numberOfFails = numberOfFails;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}