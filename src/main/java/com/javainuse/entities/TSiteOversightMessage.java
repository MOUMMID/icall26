package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_site_oversight_message database table.
 * 
 */
@Entity
@Table(name="t_site_oversight_message")
@NamedQuery(name="TSiteOversightMessage.findAll", query="SELECT t FROM TSiteOversightMessage t")
public class TSiteOversightMessage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="created_at")
	private Timestamp createdAt;

	private byte criticity;

	private String header;

	private int id;

	@Column(name="is_sent")
	private String isSent;

	private String message;

	private String module;

	@Column(name="number_of_items")
	private int numberOfItems;

	private String parameters;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	@Column(name="user_id")
	private int userId;

	public TSiteOversightMessage() {
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public byte getCriticity() {
		return this.criticity;
	}

	public void setCriticity(byte criticity) {
		this.criticity = criticity;
	}

	public String getHeader() {
		return this.header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsSent() {
		return this.isSent;
	}

	public void setIsSent(String isSent) {
		this.isSent = isSent;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getModule() {
		return this.module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public int getNumberOfItems() {
		return this.numberOfItems;
	}

	public void setNumberOfItems(int numberOfItems) {
		this.numberOfItems = numberOfItems;
	}

	public String getParameters() {
		return this.parameters;
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}