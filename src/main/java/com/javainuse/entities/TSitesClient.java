package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_sites_clients database table.
 * 
 */
@Entity
@Table(name="t_sites_clients")
@NamedQuery(name="TSitesClient.findAll", query="SELECT t FROM TSitesClient t")
public class TSitesClient implements Serializable {
	private static final long serialVersionUID = 1L;

	private String application;

	private String banner;

	@Column(name="created_at")
	private Timestamp createdAt;

	private String design;

	private String favicon;

	@Column(name="favicon_ico")
	private String faviconIco;

	private int id;

	@Column(name="id_company")
	private int idCompany;

	@Column(name="is_active")
	private String isActive;

	@Column(name="is_default")
	private String isDefault;

	@Column(name="is_mobile")
	private String isMobile;

	@Column(name="is_tablet")
	private String isTablet;

	private String lang;

	private String name;

	private String params;

	private String style;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TSitesClient() {
	}

	public String getApplication() {
		return this.application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public String getBanner() {
		return this.banner;
	}

	public void setBanner(String banner) {
		this.banner = banner;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getDesign() {
		return this.design;
	}

	public void setDesign(String design) {
		this.design = design;
	}

	public String getFavicon() {
		return this.favicon;
	}

	public void setFavicon(String favicon) {
		this.favicon = favicon;
	}

	public String getFaviconIco() {
		return this.faviconIco;
	}

	public void setFaviconIco(String faviconIco) {
		this.faviconIco = faviconIco;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdCompany() {
		return this.idCompany;
	}

	public void setIdCompany(int idCompany) {
		this.idCompany = idCompany;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getIsDefault() {
		return this.isDefault;
	}

	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}

	public String getIsMobile() {
		return this.isMobile;
	}

	public void setIsMobile(String isMobile) {
		this.isMobile = isMobile;
	}

	public String getIsTablet() {
		return this.isTablet;
	}

	public void setIsTablet(String isTablet) {
		this.isTablet = isTablet;
	}

	public String getLang() {
		return this.lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParams() {
		return this.params;
	}

	public void setParams(String params) {
		this.params = params;
	}

	public String getStyle() {
		return this.style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}