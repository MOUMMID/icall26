package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_system_monitor_record_directory database table.
 * 
 */
@Entity
@Table(name="t_system_monitor_record_directory")
@NamedQuery(name="TSystemMonitorRecordDirectory.findAll", query="SELECT t FROM TSystemMonitorRecordDirectory t")
public class TSystemMonitorRecordDirectory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="created_at")
	private Timestamp createdAt;

	private String directory;

	private int id;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	private int used;

	public TSystemMonitorRecordDirectory() {
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getDirectory() {
		return this.directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public int getUsed() {
		return this.used;
	}

	public void setUsed(int used) {
		this.used = used;
	}

}