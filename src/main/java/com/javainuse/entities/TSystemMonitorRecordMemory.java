package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_system_monitor_record_memory database table.
 * 
 */
@Entity
@Table(name="t_system_monitor_record_memory")
@NamedQuery(name="TSystemMonitorRecordMemory.findAll", query="SELECT t FROM TSystemMonitorRecordMemory t")
public class TSystemMonitorRecordMemory implements Serializable {
	private static final long serialVersionUID = 1L;

	private int available;

	private int buffers;

	private int cache;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int free;

	private int id;

	private int shared;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	private int used;

	public TSystemMonitorRecordMemory() {
	}

	public int getAvailable() {
		return this.available;
	}

	public void setAvailable(int available) {
		this.available = available;
	}

	public int getBuffers() {
		return this.buffers;
	}

	public void setBuffers(int buffers) {
		this.buffers = buffers;
	}

	public int getCache() {
		return this.cache;
	}

	public void setCache(int cache) {
		this.cache = cache;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getFree() {
		return this.free;
	}

	public void setFree(int free) {
		this.free = free;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getShared() {
		return this.shared;
	}

	public void setShared(int shared) {
		this.shared = shared;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public int getUsed() {
		return this.used;
	}

	public void setUsed(int used) {
		this.used = used;
	}

}