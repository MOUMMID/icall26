package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_system_monitor_record_partition database table.
 * 
 */
@Entity
@Table(name="t_system_monitor_record_partition")
@NamedQuery(name="TSystemMonitorRecordPartition.findAll", query="SELECT t FROM TSystemMonitorRecordPartition t")
public class TSystemMonitorRecordPartition implements Serializable {
	private static final long serialVersionUID = 1L;

	private int available;

	@Column(name="created_at")
	private Timestamp createdAt;

	private String directory;

	private int free;

	private int id;

	private String name;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	private int used;

	public TSystemMonitorRecordPartition() {
	}

	public int getAvailable() {
		return this.available;
	}

	public void setAvailable(int available) {
		this.available = available;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getDirectory() {
		return this.directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public int getFree() {
		return this.free;
	}

	public void setFree(int free) {
		this.free = free;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public int getUsed() {
		return this.used;
	}

	public void setUsed(int used) {
		this.used = used;
	}

}