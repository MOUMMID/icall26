package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_system_versions_file database table.
 * 
 */
@Entity
@Table(name="t_system_versions_file")
@NamedQuery(name="TSystemVersionsFile.findAll", query="SELECT t FROM TSystemVersionsFile t")
public class TSystemVersionsFile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Lob
	private String changes;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="dated_at")
	private Timestamp datedAt;

	private int id;

	private String lang;

	private String module;

	private String status;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	private String version;

	public TSystemVersionsFile() {
	}

	public String getChanges() {
		return this.changes;
	}

	public void setChanges(String changes) {
		this.changes = changes;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public Timestamp getDatedAt() {
		return this.datedAt;
	}

	public void setDatedAt(Timestamp datedAt) {
		this.datedAt = datedAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLang() {
		return this.lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getModule() {
		return this.module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}