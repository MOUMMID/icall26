package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the t_users database table.
 * 
 */
@Entity
@Table(name="t_users")
@NamedQuery(name="TUser.findAll", query="SELECT t FROM TUser t")
public class TUser implements Serializable {
	private static final long serialVersionUID = 1L;

	private String application;

	@Temporal(TemporalType.DATE)
	private Date birthday;

	@Column(name="callcenter_id")
	private int callcenterId;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="creator_id")
	private int creatorId;

	private String email;

	@Column(name="email_tosend")
	private String emailTosend;

	private String firstname;

	private int id;

	@Column(name="is_active")
	private String isActive;

	@Column(name="is_guess")
	private String isGuess;

	@Column(name="is_locked")
	private String isLocked;

	@Column(name="last_password_gen")
	private Timestamp lastPasswordGen;

	private Timestamp lastlogin;

	private String lastname;

	@Column(name="locked_at")
	private Timestamp lockedAt;

	private String mobile;

	@Column(name="number_of_try")
	private int numberOfTry;

	private String password;

	private String phone;

	private String picture;

	private String sex;

	private String status;

	@Column(name="unlocked_by")
	private int unlockedBy;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	private String username;

	public TUser() {
	}

	public String getApplication() {
		return this.application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public Date getBirthday() {
		return this.birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public int getCallcenterId() {
		return this.callcenterId;
	}

	public void setCallcenterId(int callcenterId) {
		this.callcenterId = callcenterId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getCreatorId() {
		return this.creatorId;
	}

	public void setCreatorId(int creatorId) {
		this.creatorId = creatorId;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailTosend() {
		return this.emailTosend;
	}

	public void setEmailTosend(String emailTosend) {
		this.emailTosend = emailTosend;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getIsGuess() {
		return this.isGuess;
	}

	public void setIsGuess(String isGuess) {
		this.isGuess = isGuess;
	}

	public String getIsLocked() {
		return this.isLocked;
	}

	public void setIsLocked(String isLocked) {
		this.isLocked = isLocked;
	}

	public Timestamp getLastPasswordGen() {
		return this.lastPasswordGen;
	}

	public void setLastPasswordGen(Timestamp lastPasswordGen) {
		this.lastPasswordGen = lastPasswordGen;
	}

	public Timestamp getLastlogin() {
		return this.lastlogin;
	}

	public void setLastlogin(Timestamp lastlogin) {
		this.lastlogin = lastlogin;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Timestamp getLockedAt() {
		return this.lockedAt;
	}

	public void setLockedAt(Timestamp lockedAt) {
		this.lockedAt = lockedAt;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public int getNumberOfTry() {
		return this.numberOfTry;
	}

	public void setNumberOfTry(int numberOfTry) {
		this.numberOfTry = numberOfTry;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPicture() {
		return this.picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getSex() {
		return this.sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getUnlockedBy() {
		return this.unlockedBy;
	}

	public void setUnlockedBy(int unlockedBy) {
		this.unlockedBy = unlockedBy;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}