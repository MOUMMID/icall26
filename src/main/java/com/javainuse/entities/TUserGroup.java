package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_user_group database table.
 * 
 */
@Entity
@Table(name="t_user_group")
@NamedQuery(name="TUserGroup.findAll", query="SELECT t FROM TUserGroup t")
public class TUserGroup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="group_id")
	private int groupId;

	private int id;

	@Column(name="is_active")
	private String isActive;

	@Column(name="user_id")
	private int userId;

	public TUserGroup() {
	}

	public int getGroupId() {
		return this.groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}