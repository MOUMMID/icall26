package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_user_permission database table.
 * 
 */
@Entity
@Table(name="t_user_permission")
@NamedQuery(name="TUserPermission.findAll", query="SELECT t FROM TUserPermission t")
public class TUserPermission implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	@Column(name="permission_id")
	private int permissionId;

	@Column(name="user_id")
	private int userId;

	public TUserPermission() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPermissionId() {
		return this.permissionId;
	}

	public void setPermissionId(int permissionId) {
		this.permissionId = permissionId;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}