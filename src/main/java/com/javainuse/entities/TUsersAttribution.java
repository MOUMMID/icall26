package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_users_attributions database table.
 * 
 */
@Entity
@Table(name="t_users_attributions")
@NamedQuery(name="TUsersAttribution.findAll", query="SELECT t FROM TUsersAttribution t")
public class TUsersAttribution implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="attribution_id")
	private int attributionId;

	private int id;

	@Column(name="user_id")
	private int userId;

	public TUsersAttribution() {
	}

	public int getAttributionId() {
		return this.attributionId;
	}

	public void setAttributionId(int attributionId) {
		this.attributionId = attributionId;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}