package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_users_chat database table.
 * 
 */
@Entity
@Table(name="t_users_chat")
@NamedQuery(name="TUsersChat.findAll", query="SELECT t FROM TUsersChat t")
public class TUsersChat implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	@Column(name="is_active")
	private String isActive;

	@Column(name="user_id")
	private int userId;

	public TUsersChat() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}