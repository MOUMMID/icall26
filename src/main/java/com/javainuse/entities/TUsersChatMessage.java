package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_users_chat_messages database table.
 * 
 */
@Entity
@Table(name="t_users_chat_messages")
@NamedQuery(name="TUsersChatMessage.findAll", query="SELECT t FROM TUsersChatMessage t")
public class TUsersChatMessage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="from_user_id")
	private int fromUserId;

	private int id;

	@Column(name="is_seen")
	private String isSeen;

	private String message;

	@Column(name="to_user_id")
	private int toUserId;

	public TUsersChatMessage() {
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getFromUserId() {
		return this.fromUserId;
	}

	public void setFromUserId(int fromUserId) {
		this.fromUserId = fromUserId;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsSeen() {
		return this.isSeen;
	}

	public void setIsSeen(String isSeen) {
		this.isSeen = isSeen;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getToUserId() {
		return this.toUserId;
	}

	public void setToUserId(int toUserId) {
		this.toUserId = toUserId;
	}

}