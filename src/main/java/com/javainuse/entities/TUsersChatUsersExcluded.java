package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_users_chat_users_excluded database table.
 * 
 */
@Entity
@Table(name="t_users_chat_users_excluded")
@NamedQuery(name="TUsersChatUsersExcluded.findAll", query="SELECT t FROM TUsersChatUsersExcluded t")
public class TUsersChatUsersExcluded implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="chat_team_id")
	private int chatTeamId;

	private int id;

	@Column(name="user_id")
	private int userId;

	public TUsersChatUsersExcluded() {
	}

	public int getChatTeamId() {
		return this.chatTeamId;
	}

	public void setChatTeamId(int chatTeamId) {
		this.chatTeamId = chatTeamId;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}