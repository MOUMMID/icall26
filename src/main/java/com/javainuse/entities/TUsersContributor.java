package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_users_contributors database table.
 * 
 */
@Entity
@Table(name="t_users_contributors")
@NamedQuery(name="TUsersContributor.findAll", query="SELECT t FROM TUsersContributor t")
public class TUsersContributor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="attribution_id")
	private int attributionId;

	@Column(name="contributor_id")
	private int contributorId;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	private String status;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	@Column(name="user_id")
	private int userId;

	public TUsersContributor() {
	}

	public int getAttributionId() {
		return this.attributionId;
	}

	public void setAttributionId(int attributionId) {
		this.attributionId = attributionId;
	}

	public int getContributorId() {
		return this.contributorId;
	}

	public void setContributorId(int contributorId) {
		this.contributorId = contributorId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}