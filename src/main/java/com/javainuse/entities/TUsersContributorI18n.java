package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_users_contributor_i18n database table.
 * 
 */
@Entity
@Table(name="t_users_contributor_i18n")
@NamedQuery(name="TUsersContributorI18n.findAll", query="SELECT t FROM TUsersContributorI18n t")
public class TUsersContributorI18n implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="contributor_id")
	private int contributorId;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	private String lang;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	private String value;

	public TUsersContributorI18n() {
	}

	public int getContributorId() {
		return this.contributorId;
	}

	public void setContributorId(int contributorId) {
		this.contributorId = contributorId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLang() {
		return this.lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}