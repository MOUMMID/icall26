package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_users_email_sent database table.
 * 
 */
@Entity
@Table(name="t_users_email_sent")
@NamedQuery(name="TUsersEmailSent.findAll", query="SELECT t FROM TUsersEmailSent t")
public class TUsersEmailSent implements Serializable {
	private static final long serialVersionUID = 1L;

	@Lob
	private String body;

	@Column(name="contract_id")
	private int contractId;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="customer_id")
	private int customerId;

	private String email;

	private int id;

	@Column(name="is_sent")
	private String isSent;

	@Column(name="meeting_id")
	private int meetingId;

	@Column(name="model_id")
	private int modelId;

	@Column(name="sent_at")
	private Timestamp sentAt;

	private String subject;

	@Column(name="user_id")
	private int userId;

	public TUsersEmailSent() {
	}

	public String getBody() {
		return this.body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public int getContractId() {
		return this.contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsSent() {
		return this.isSent;
	}

	public void setIsSent(String isSent) {
		this.isSent = isSent;
	}

	public int getMeetingId() {
		return this.meetingId;
	}

	public void setMeetingId(int meetingId) {
		this.meetingId = meetingId;
	}

	public int getModelId() {
		return this.modelId;
	}

	public void setModelId(int modelId) {
		this.modelId = modelId;
	}

	public Timestamp getSentAt() {
		return this.sentAt;
	}

	public void setSentAt(Timestamp sentAt) {
		this.sentAt = sentAt;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}