package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_users_function database table.
 * 
 */
@Entity
@Table(name="t_users_function")
@NamedQuery(name="TUsersFunction.findAll", query="SELECT t FROM TUsersFunction t")
public class TUsersFunction implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private String name;

	public TUsersFunction() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}