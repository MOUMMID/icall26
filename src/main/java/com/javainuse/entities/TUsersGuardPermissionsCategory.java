package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_users_guard_permissions_category database table.
 * 
 */
@Entity
@Table(name="t_users_guard_permissions_category")
@NamedQuery(name="TUsersGuardPermissionsCategory.findAll", query="SELECT t FROM TUsersGuardPermissionsCategory t")
public class TUsersGuardPermissionsCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private int lb;

	private byte level;

	private String name;

	private int rb;

	public TUsersGuardPermissionsCategory() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getLb() {
		return this.lb;
	}

	public void setLb(int lb) {
		this.lb = lb;
	}

	public byte getLevel() {
		return this.level;
	}

	public void setLevel(byte level) {
		this.level = level;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRb() {
		return this.rb;
	}

	public void setRb(int rb) {
		this.rb = rb;
	}

}