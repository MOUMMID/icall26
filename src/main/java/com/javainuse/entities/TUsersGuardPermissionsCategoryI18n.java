package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_users_guard_permissions_category_i18n database table.
 * 
 */
@Entity
@Table(name="t_users_guard_permissions_category_i18n")
@NamedQuery(name="TUsersGuardPermissionsCategoryI18n.findAll", query="SELECT t FROM TUsersGuardPermissionsCategoryI18n t")
public class TUsersGuardPermissionsCategoryI18n implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="category_id")
	private int categoryId;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	@Column(name="is_active")
	private String isActive;

	private String lang;

	private String title;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TUsersGuardPermissionsCategoryI18n() {
	}

	public int getCategoryId() {
		return this.categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getLang() {
		return this.lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}