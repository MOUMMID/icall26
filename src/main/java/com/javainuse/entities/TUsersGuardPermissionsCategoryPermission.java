package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_users_guard_permissions_category_permission database table.
 * 
 */
@Entity
@Table(name="t_users_guard_permissions_category_permission")
@NamedQuery(name="TUsersGuardPermissionsCategoryPermission.findAll", query="SELECT t FROM TUsersGuardPermissionsCategoryPermission t")
public class TUsersGuardPermissionsCategoryPermission implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="category_id")
	private int categoryId;

	private int id;

	@Column(name="is_active")
	private String isActive;

	@Column(name="permission_id")
	private int permissionId;

	private short position;

	public TUsersGuardPermissionsCategoryPermission() {
	}

	public int getCategoryId() {
		return this.categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public int getPermissionId() {
		return this.permissionId;
	}

	public void setPermissionId(int permissionId) {
		this.permissionId = permissionId;
	}

	public short getPosition() {
		return this.position;
	}

	public void setPosition(short position) {
		this.position = position;
	}

}