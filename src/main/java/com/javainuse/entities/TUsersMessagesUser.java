package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_users_messages_users database table.
 * 
 */
@Entity
@Table(name="t_users_messages_users")
@NamedQuery(name="TUsersMessagesUser.findAll", query="SELECT t FROM TUsersMessagesUser t")
public class TUsersMessagesUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	@Column(name="is_seen")
	private String isSeen;

	@Column(name="is_shown")
	private String isShown;

	@Column(name="message_id")
	private int messageId;

	@Column(name="sender_id")
	private int senderId;

	@Column(name="team_id")
	private int teamId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	@Column(name="user_id")
	private int userId;

	public TUsersMessagesUser() {
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsSeen() {
		return this.isSeen;
	}

	public void setIsSeen(String isSeen) {
		this.isSeen = isSeen;
	}

	public String getIsShown() {
		return this.isShown;
	}

	public void setIsShown(String isShown) {
		this.isShown = isShown;
	}

	public int getMessageId() {
		return this.messageId;
	}

	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	public int getSenderId() {
		return this.senderId;
	}

	public void setSenderId(int senderId) {
		this.senderId = senderId;
	}

	public int getTeamId() {
		return this.teamId;
	}

	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}