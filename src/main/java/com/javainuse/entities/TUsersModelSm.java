package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_users_model_sms database table.
 * 
 */
@Entity
@Table(name="t_users_model_sms")
@NamedQuery(name="TUsersModelSm.findAll", query="SELECT t FROM TUsersModelSm t")
public class TUsersModelSm implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private String name;

	public TUsersModelSm() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}