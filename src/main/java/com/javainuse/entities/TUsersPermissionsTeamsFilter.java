package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_users_permissions_teams_filter database table.
 * 
 */
@Entity
@Table(name="t_users_permissions_teams_filter")
@NamedQuery(name="TUsersPermissionsTeamsFilter.findAll", query="SELECT t FROM TUsersPermissionsTeamsFilter t")
public class TUsersPermissionsTeamsFilter implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="admin_id")
	private int adminId;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	@Column(name="team_id")
	private int teamId;

	private String type;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TUsersPermissionsTeamsFilter() {
	}

	public int getAdminId() {
		return this.adminId;
	}

	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTeamId() {
		return this.teamId;
	}

	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}