package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the t_users_tasks database table.
 * 
 */
@Entity
@Table(name="t_users_tasks")
@NamedQuery(name="TUsersTask.findAll", query="SELECT t FROM TUsersTask t")
public class TUsersTask implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="contract_id")
	private int contractId;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Lob
	private String description;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="done_at")
	private Date doneAt;

	@Column(name="from_user_application")
	private String fromUserApplication;

	@Column(name="from_user_id")
	private int fromUserId;

	private int id;

	@Column(name="is_seen")
	private String isSeen;

	@Column(name="linked_with_id")
	private int linkedWithId;

	@Column(name="meeting_id")
	private int meetingId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="planned_at")
	private Date plannedAt;

	private int priority;

	@Column(name="state_id")
	private int stateId;

	private String status;

	private String title;

	@Column(name="to_user_application")
	private String toUserApplication;

	@Column(name="to_user_id")
	private int toUserId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TUsersTask() {
	}

	public int getContractId() {
		return this.contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDoneAt() {
		return this.doneAt;
	}

	public void setDoneAt(Date doneAt) {
		this.doneAt = doneAt;
	}

	public String getFromUserApplication() {
		return this.fromUserApplication;
	}

	public void setFromUserApplication(String fromUserApplication) {
		this.fromUserApplication = fromUserApplication;
	}

	public int getFromUserId() {
		return this.fromUserId;
	}

	public void setFromUserId(int fromUserId) {
		this.fromUserId = fromUserId;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsSeen() {
		return this.isSeen;
	}

	public void setIsSeen(String isSeen) {
		this.isSeen = isSeen;
	}

	public int getLinkedWithId() {
		return this.linkedWithId;
	}

	public void setLinkedWithId(int linkedWithId) {
		this.linkedWithId = linkedWithId;
	}

	public int getMeetingId() {
		return this.meetingId;
	}

	public void setMeetingId(int meetingId) {
		this.meetingId = meetingId;
	}

	public Date getPlannedAt() {
		return this.plannedAt;
	}

	public void setPlannedAt(Date plannedAt) {
		this.plannedAt = plannedAt;
	}

	public int getPriority() {
		return this.priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public int getStateId() {
		return this.stateId;
	}

	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getToUserApplication() {
		return this.toUserApplication;
	}

	public void setToUserApplication(String toUserApplication) {
		this.toUserApplication = toUserApplication;
	}

	public int getToUserId() {
		return this.toUserId;
	}

	public void setToUserId(int toUserId) {
		this.toUserId = toUserId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}