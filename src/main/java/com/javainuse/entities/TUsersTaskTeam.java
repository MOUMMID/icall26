package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_users_task_teams database table.
 * 
 */
@Entity
@Table(name="t_users_task_teams")
@NamedQuery(name="TUsersTaskTeam.findAll", query="SELECT t FROM TUsersTaskTeam t")
public class TUsersTaskTeam implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	@Column(name="is_active")
	private String isActive;

	@Column(name="team_id")
	private int teamId;

	public TUsersTaskTeam() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public int getTeamId() {
		return this.teamId;
	}

	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}

}