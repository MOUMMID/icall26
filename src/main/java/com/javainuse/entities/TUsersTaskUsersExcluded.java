package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_users_task_users_excluded database table.
 * 
 */
@Entity
@Table(name="t_users_task_users_excluded")
@NamedQuery(name="TUsersTaskUsersExcluded.findAll", query="SELECT t FROM TUsersTaskUsersExcluded t")
public class TUsersTaskUsersExcluded implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	@Column(name="task_team_id")
	private int taskTeamId;

	@Column(name="user_id")
	private int userId;

	public TUsersTaskUsersExcluded() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTaskTeamId() {
		return this.taskTeamId;
	}

	public void setTaskTeamId(int taskTeamId) {
		this.taskTeamId = taskTeamId;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}