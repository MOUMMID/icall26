package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_users_team database table.
 * 
 */
@Entity
@Table(name="t_users_team")
@NamedQuery(name="TUsersTeam.findAll", query="SELECT t FROM TUsersTeam t")
public class TUsersTeam implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int id;

	@Column(name="manager_id")
	private int managerId;

	@Column(name="manager2_id")
	private int manager2Id;

	private String name;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TUsersTeam() {
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getManagerId() {
		return this.managerId;
	}

	public void setManagerId(int managerId) {
		this.managerId = managerId;
	}

	public int getManager2Id() {
		return this.manager2Id;
	}

	public void setManager2Id(int manager2Id) {
		this.manager2Id = manager2Id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}