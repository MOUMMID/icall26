package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_users_team_users database table.
 * 
 */
@Entity
@Table(name="t_users_team_users")
@NamedQuery(name="TUsersTeamUser.findAll", query="SELECT t FROM TUsersTeamUser t")
public class TUsersTeamUser implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	@Column(name="team_id")
	private int teamId;

	@Column(name="user_id")
	private int userId;

	public TUsersTeamUser() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTeamId() {
		return this.teamId;
	}

	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}