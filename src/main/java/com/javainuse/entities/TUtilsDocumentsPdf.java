package com.javainuse.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the t_utils_documents_pdf database table.
 * 
 */
@Entity
@Table(name="t_utils_documents_pdf")
@NamedQuery(name="TUtilsDocumentsPdf.findAll", query="SELECT t FROM TUtilsDocumentsPdf t")
public class TUtilsDocumentsPdf implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="class")
	private String class_;

	@Column(name="created_at")
	private Timestamp createdAt;

	private int current;

	@Column(name="ended_at")
	private Timestamp endedAt;

	@Lob
	private String errors;

	private String file;

	@Column(name="has_error")
	private String hasError;

	private int id;

	private int idx;

	@Column(name="in_progress")
	private String inProgress;

	@Column(name="is_completed")
	private String isCompleted;

	private String method;

	private String module;

	private String name;

	@Column(name="number_of_documents")
	private int numberOfDocuments;

	@Lob
	private String parameters;

	@Column(name="pdf_generated")
	private int pdfGenerated;

	private int priority;

	private int progression;

	private String signature;

	@Column(name="started_at")
	private Timestamp startedAt;

	private String status;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	@Column(name="user_id")
	private int userId;

	public TUtilsDocumentsPdf() {
	}

	public String getClass_() {
		return this.class_;
	}

	public void setClass_(String class_) {
		this.class_ = class_;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getCurrent() {
		return this.current;
	}

	public void setCurrent(int current) {
		this.current = current;
	}

	public Timestamp getEndedAt() {
		return this.endedAt;
	}

	public void setEndedAt(Timestamp endedAt) {
		this.endedAt = endedAt;
	}

	public String getErrors() {
		return this.errors;
	}

	public void setErrors(String errors) {
		this.errors = errors;
	}

	public String getFile() {
		return this.file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getHasError() {
		return this.hasError;
	}

	public void setHasError(String hasError) {
		this.hasError = hasError;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdx() {
		return this.idx;
	}

	public void setIdx(int idx) {
		this.idx = idx;
	}

	public String getInProgress() {
		return this.inProgress;
	}

	public void setInProgress(String inProgress) {
		this.inProgress = inProgress;
	}

	public String getIsCompleted() {
		return this.isCompleted;
	}

	public void setIsCompleted(String isCompleted) {
		this.isCompleted = isCompleted;
	}

	public String getMethod() {
		return this.method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getModule() {
		return this.module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumberOfDocuments() {
		return this.numberOfDocuments;
	}

	public void setNumberOfDocuments(int numberOfDocuments) {
		this.numberOfDocuments = numberOfDocuments;
	}

	public String getParameters() {
		return this.parameters;
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}

	public int getPdfGenerated() {
		return this.pdfGenerated;
	}

	public void setPdfGenerated(int pdfGenerated) {
		this.pdfGenerated = pdfGenerated;
	}

	public int getPriority() {
		return this.priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public int getProgression() {
		return this.progression;
	}

	public void setProgression(int progression) {
		this.progression = progression;
	}

	public String getSignature() {
		return this.signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public Timestamp getStartedAt() {
		return this.startedAt;
	}

	public void setStartedAt(Timestamp startedAt) {
		this.startedAt = startedAt;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}