package com.javainuse.metier;

import java.util.List;

import com.javainuse.entities.Product;


public interface ProductMetier {
	
	public List<Product> listProduct();
	public Product getProductById(int id);
	public Product saveProduct(Product p);
	public Product updateProduct(Product p,int id);
	public void deleteProduct(int id);
}
