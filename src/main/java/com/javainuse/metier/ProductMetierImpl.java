package com.javainuse.metier;



import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javainuse.dao.ProductRepository;
import com.javainuse.entities.Product;

@Service
public class ProductMetierImpl implements ProductMetier{
	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public Product updateProduct(Product p,int id) {
		// TODO Auto-generated method stub
		Optional<Product> productOptional = productRepository.findById(id);

		if (!productOptional.isPresent())
			throw new RuntimeException("id-not found" + id);

		p.setId(id);
		p.setUpdated_at(new Date());
		productRepository.save(p);
		return p;
	}


	@Override
	public List<Product> listProduct() {
		// TODO Auto-generated method stub
		return productRepository.findAll();
	}

	@Override
	public Product getProductById(int id) {
		// TODO Auto-generated method stub
		Optional<Product> product = productRepository.findById(id);

		if (!product.isPresent())
			throw new RuntimeException("id-not found" + id);

		return product.get();

	}

	@Override
	public Product saveProduct(Product p) {
		// TODO Auto-generated method stub
		p.setCreated_at(new Date());
		p.setUpdated_at(new Date());
		return productRepository.save(p);
	}

	@Override
	public void deleteProduct(int id) {
		// TODO Auto-generated method stub
		 productRepository.deleteById(id);
	}

}
