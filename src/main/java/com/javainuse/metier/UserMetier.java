package com.javainuse.metier;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;

import com.javainuse.entities.User;

public interface UserMetier {
	public UserDetails loadUserByUsername(String username);
	public List<User> listUsers();
	public User saveUser(User user);
	public User updateUser(Long id, User user);
	public void deleteUser(Long id);
}
