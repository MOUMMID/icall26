package com.javainuse.metier;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.javainuse.dao.UserRepository;
import com.javainuse.entities.User;

@Service
public class UserMetierImpl implements UserMetier,UserDetailsService{
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder bcryptEncoder;

	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				new ArrayList<>());
	}
	@Override
	public List<User> listUsers() {
		// TODO Auto-generated method stub
		return userRepository.findAll();
	}

	@Override
	public User saveUser(User user) {
		// TODO Auto-generated method stub
	
		//user.setUsername(user.getUsername());
		user.setPassword(bcryptEncoder.encode(user.getPassword()));
		user.setCreated_at(new Date());
		user.setUpdated_at(new Date());
		return userRepository.save(user);
	}

	@Override
	public User updateUser(Long id, User user) {
		// TODO Auto-generated method stub
		Optional<User> userOptional = userRepository.findById(id);

		if (!userOptional.isPresent())
			throw new RuntimeException("id-not found" + id);

		user.setId(id);
		user.setPassword(bcryptEncoder.encode(user.getPassword()));
		user.setUpdated_at(new Date());
		userRepository.save(user);
		return user;
	}

	@Override
	public void deleteUser(Long id) {
		// TODO Auto-generated method stub
		Optional<User> userOptional=userRepository.findById(id);
		if(!userOptional.isPresent()) throw new RuntimeException("id-not found"+ id);
		userRepository.deleteById(id);
	}
	
}
