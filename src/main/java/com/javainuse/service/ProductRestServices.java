package com.javainuse.service;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RestController;

import com.javainuse.entities.Product;
import com.javainuse.metier.ProductMetier;

@RestController
@CrossOrigin("*")
public class ProductRestServices {

	@Autowired
	private ProductMetier productMetier;

	@PutMapping("/products/{id}")
	public Product updateProduct(@RequestBody Product p,@PathVariable int id) {
		return productMetier.updateProduct(p, id);
	}

	@GetMapping("/products")
	public List<Product> listProduct() {
		return productMetier.listProduct();
	}

	@GetMapping("/products/{id}")
	public Product getProductById(@PathVariable int id) {
		return productMetier.getProductById(id);
	}
	@PostMapping("/products")
	public Product saveProduct(@RequestBody Product p) {
		return productMetier.saveProduct(p);
	}

//	@PostMapping("/students")
//	public ResponseEntity<Object> createStudent(@RequestBody Student student) {
//		Student savedStudent = studentRepository.save(student);
//
//		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
//				.buildAndExpand(savedStudent.getId()).toUri();
//
//		return ResponseEntity.created(location).build();
//
//	}
	

	@DeleteMapping("/products/{id}")
	public void deleteProduct(@PathVariable int id) {
		productMetier.deleteProduct(id);
	}
}
