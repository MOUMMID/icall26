package com.javainuse.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.javainuse.entities.User;
import com.javainuse.metier.UserMetier;

@RestController
@CrossOrigin
public class UserServices {
	
	@Autowired
	private UserMetier userMetier;

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<?> saveUser(@RequestBody User user) throws Exception {
		return ResponseEntity.ok(userMetier.saveUser(user));
	}
	
	public UserDetails loadUserByUsername(String username) {
		return userMetier.loadUserByUsername(username);
	}
	@GetMapping("/users")
	public List<User> listUsers() {
		return userMetier.listUsers();
	}

	@PutMapping("/users/{id}")
	public User updateUser(@PathVariable Long id,@RequestBody User user) {
		return userMetier.updateUser(id, user);
	}
	@DeleteMapping("/users/{id}")
	public void deleteUser(@PathVariable Long id) {
		userMetier.deleteUser(id);
	}
	

}
